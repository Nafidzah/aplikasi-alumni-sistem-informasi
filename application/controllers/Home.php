<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    function __construct(){
        parent::__construct();

        $this->data = array();
    }

    function index(){
        redirect('auth/login');
    }

    function error_404(){
        $this->load->view('templates/v_error_404', $this->data);
    }
}

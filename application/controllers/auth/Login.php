<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct(){
        parent::__construct();

        $this->load->model('M_users');
        $this->data = array();
    }

    function index(){
        if($this->session->userdata('id')){
            redirect('backoffice/dashboard');
        }

        if($this->input->post('param1') && $this->input->post('param2')){
            $this->form_validation->set_rules('param1', 'Username', 'required|callback__validate');
        } else {
            $this->form_validation->set_rules('param1', 'Username', 'required');            
        }

        $this->form_validation->set_rules('param2', 'Password', 'required');
        

        $this->form_validation->set_message('required', '%s is required');

        if($this->form_validation->run() == FALSE){
            $this->load->view('auth/login/v_login', $this->data);
        } else {
            redirect('backoffice/dashboard');
        }
    }

    function _validate(){
        $username = htmlspecialchars($this->input->post('param1', TRUE), ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('param2', TRUE), ENT_QUOTES);

        $password = sha1($password);

        $data = $this->M_users->get_login($username, $password);

        if ($data) {
            $data['logged_in'] = TRUE;

            $this->session->set_userdata($data);

            return true;
        } else {
            $this->form_validation->set_message('_validate', 'Username/Password Incorrect!');
            return false;
        }
    }

    function logout(){
        $this->session->sess_destroy();

        redirect('auth/login');
    }
}

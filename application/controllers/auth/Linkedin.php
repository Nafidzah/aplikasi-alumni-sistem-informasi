<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linkedin extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->data = array();
    }

    function index(){
        redirect('http://linkedin.com');
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->data = array();
    }

    function index(){
        redirect('http://mail.google.com');
    }
}
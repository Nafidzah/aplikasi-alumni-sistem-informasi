<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
    function __construct(){
        parent::__construct();

        $this->load->model('M_users');

        $this->data = array();
    }

    function index(){

        if($this->input->post('email') && $this->input->post('username')){
            $this->form_validation->set_rules('email','Email', 'required|callback__validate');
            $this->form_validation->set_rules('username','Username', 'required|min_length[3]|callback__validate');
        } else {
            $this->form_validation->set_rules('email','Email', 'required');
            $this->form_validation->set_rules('username','Username', 'required|min_length[3]');
        }

        $this->form_validation->set_rules('jenis_kelamin','Jenis kelamin', 'required');
        $this->form_validation->set_rules('tahun_angkatan','Tahun angkatan', 'required');
        $this->form_validation->set_rules('nomor_hp','Nomor hp', 'required');
        $this->form_validation->set_rules('fullname','Nama lengkap', 'required|min_length[3]');
        $this->form_validation->set_rules('password','Password', 'required|min_length[3]');
        $this->form_validation->set_rules('konfirmasi_password','Password', 'required|min_length[3]|matches[password]');
        
        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('is_unique', '%s sudah tersedia!');

        if($this->form_validation->run() == FALSE){
            $this->load->view('auth/register/v_index', $this->data);
        } else {
            
            $this->data['role_id']          = 2;
            $this->data['username']         = $this->input->post('username');
            $this->data['password']         = sha1($this->input->post('password'));
            $this->data['fullname']         = $this->input->post('fullname');
            $this->data['email']            = $this->input->post('email');
            $this->data['jenis_kelamin']    = $this->input->post('jenis_kelamin');
            $this->data['tahun_angkatan']   = $this->input->post('tahun_angkatan');
            $this->data['nomor_hp']         = $this->input->post('nomor_hp');
            $this->data['created_at']       = date('Y-m-d H:i:s');

            $simpan = $this->M_users->add($this->data);

            if($simpan){
                $this->session->set_flashdata('success', 'Pendaftaran Telah Berhasil! Silahkan Login!');
                redirect('auth/login/index');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
                redirect('auth/register/index');
            }
        }
    }

    function _validate(){
        $email      = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
        $username   = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES);

        $cek_email      = $this->M_users->check(array('email' => $email));
        $cek_username   = $this->M_users->check(array('username' => $username));

        if($cek_email->num_rows() > 0){
            $this->form_validation->set_message('_validate', 'Email sudah terdaftar!');
            return false;
        } else if($cek_username->num_rows() > 0){
            $this->form_validation->set_message('_validate', 'Username sudah terdaftar!');
            return false;
        } else {
            return true;
        }

    }
}
function _validate(){
        $email      = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
        $username   = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES);

        $cek_email      = $this->M_users->check(array('email' => $email));
        $cek_username   = $this->M_users->check(array('username' => $username));

        if($cek_email->num_rows() > 0){
            $this->form_validation->set_message('_validate', 'Email sudah terdaftar!');
            return false;
        } else if($cek_username->num_rows() > 0){
            $this->form_validation->set_message('_validate', 'Username sudah terdaftar!');
            return false;
        } else {
            return true;
        }

    }
}
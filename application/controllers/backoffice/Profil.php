<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    function __construct(){
        parent::__construct();

        ($this->session->userdata('id') ? '' : redirect('login'));

<<<<<<< HEAD
        $this->load->model('M_users');
        $this->load->model('M_skill');
        $this->load->model('M_achievement');
        $this->load->model('M_contribution');
        $this->load->model('M_experience');
<<<<<<< HEAD
        
=======
>>>>>>> remotes/origin/Agung
=======
        $this->id       = $this->session->userdata('id');
        $this->role_id  = $this->session->userdata('role_id');
        $this->fullname = $this->session->userdata('fullname');
        $this->foto     = $this->session->userdata('foto');

        $this->load->model(array('M_forum_like', 'M_forum', 'M_users', 'M_skill', 'M_achievement', 'M_contribution', 'M_experience'));
>>>>>>> Agung

        $this->template         = 'templates/v_backoffice';
        $this->contents         = 'profil/';
        $this->ajax_contents    = 'backoffice/profil/';

        $this->data = array();
    }

	function index(){
<<<<<<< HEAD
        $id = $this->session->userdata('id');

        $this->data['edit']     = $this->M_users->get_users_by_id($id);
<<<<<<< HEAD
        $this->data['title']    = 'Profil';
=======
        $this->data['title']    = 'Profil Saya';
>>>>>>> remotes/origin/Agung
        $this->data['contents'] = $this->contents . 'v_profil';

        $this->load->view($this->template, $this->data);
=======
        $data['role_id']  = $this->role_id;
        $data['fullname'] = $this->fullname;
        $data['foto']     = $this->foto;
        $data['edit']     = $this->M_users->get_users_by_id($this->id);
        $data['title']    = 'My Profile';
        $data['contents'] = $this->contents . 'v_profil';

        $this->load->view($this->template, $data);
>>>>>>> Agung
	}

    function ubah_password(){
        $id = $this->session->userdata('id');

        $this->form_validation->set_rules('old_password','Old Password', 'required|min_length[5]|callback__password_lama');
        $this->form_validation->set_rules('new_password','New Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password','Confirmation Password', 'required|min_length[5]|matches[new_password]');

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){
            $this->data['title']    = 'Change Password';
            $this->data['contents'] = $this->contents . 'v_ubah_password';

            $this->load->view($this->template, $this->data);
        } else {
            $password = sha1($this->input->post('new_password'));

            $data['password'] = $password;

            $simpan = $this->M_users->update_users_by_id($id, $data);

            if($simpan){
                $this->session->set_userdata('password', $password);
                $this->session->set_flashdata('success', 'Password has been changed successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/profil');
        }
    }

<<<<<<< HEAD
=======
    function ubah_profil(){
        $ses_users_id = $this->session->userdata('id');

        $this->form_validation->set_rules('fullname','Nama lengkap', 'required|min_length[3]');
        $this->form_validation->set_rules('email','Email', 'required');
        $this->form_validation->set_rules('jenis_kelamin','Gender', 'required');
        $this->form_validation->set_rules('nomor_hp','Phone Number', 'required');
        $this->form_validation->set_rules('tahun_angkatan','Tahun angkatan', 'required|max_length[4]');

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){
            $this->data['edit']         = $this->M_users->get_users_by_id($ses_users_id);
            $this->data['experience']   = $this->M_experience->get_all_experience_by_users_id($ses_users_id);

            $this->data['title']        = 'Edit Profile';
            $this->data['contents']     = $this->contents . 'v_ubah_profil';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA FOTO YANG DIUPLOAD     
            $path = './uploads/profil/';

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('foto')){
                $upload = $this->upload->data();
                $data['foto'] = $upload['file_name'];

                // HAPUS FOTO YANG LAMA, JIKA EKSTENSI BERBEDA
                $foto_old = $this->input->post('foto_old');
                if($foto_old && file_exists($path . $foto_old)){
                    @unlink($path . $foto_old);
                }

            } else {
                $data['foto'] = $this->input->post('foto_old');
            }

            $data['fullname']               = $this->input->post('fullname');
            $data['email']                  = $this->input->post('email');
            $data['jenis_kelamin']          = $this->input->post('jenis_kelamin');
            $data['tahun_angkatan']         = $this->input->post('tahun_angkatan');
            $data['tempat_lahir']           = $this->input->post('tempat_lahir');
            $data['tanggal_lahir']          = dateSql($this->input->post('tanggal_lahir'));
            $data['nomor_hp']               = $this->input->post('nomor_hp');
            $data['linkedin']               = $this->input->post('linkedin');
            $data['facebook']               = $this->input->post('facebook');
            $data['twitter']                = $this->input->post('twitter');
            $data['instagram']              = $this->input->post('instagram');
            $data['website']                = $this->input->post('website');
            $data['description']            = $this->input->post('description');
            $data['experience_id_default']  = $this->input->post('experience_id_default');
            $data['updated_by']             = $this->session->userdata('id');
            $data['updated_at']             = date('Y-m-d H:i:s');
            
            $simpan = $this->M_users->update_users_by_id($ses_users_id, $data);

            if($simpan){

                // UPDATE DATA SESSION
                foreach($data as $key => $value){
                    $this->session->set_userdata($key, $value);
                }

                $this->session->set_flashdata('success', 'Profile has been changed successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/profil');
        }
    }

    function _password_lama(){
        $old_password = sha1($this->input->post('old_password'));
        $ses_password = $this->session->userdata('password');

        if($old_password == $ses_password){
            return true;
        } else {
            $this->form_validation->set_message('_password_lama', 'Check your old password again!');
            return false;
        }
    }

>>>>>>> remotes/origin/Agung
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->data['skill']    = $this->M_skill->get_all_skill_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_skill', $this->data);
    }

<<<<<<< HEAD

 /*
=======
    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_tambah($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->load->view($this->ajax_contents . 'v_skill_tambah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UBAH DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_ubah($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['edit'] = $this->M_skill->get_skill_by_id($id);
        $this->load->view($this->ajax_contents . 'v_skill_ubah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_skill->delete_by_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_simpan(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        if($this->input->post('skill_aksi') == 'tambah'){
            $this->data['skill']        = $this->input->post('skill');
            $this->data['users_id']     = $this->input->post('users_id');

            $cek = $this->M_skill->check($this->data);
            if($cek->num_rows() > 0){
                echo "2";
            } else {
                $this->data['skill_level']  = $this->input->post('skill_level');
                $this->data['created_by']   = $this->session->userdata('id');
                $this->data['created_at']   = date('Y-m-d H:i:s');
                
                $simpan = $this->M_skill->add($this->data);

                if($simpan){
                    echo "1";
                } else {
                    echo "0";
                }
            }
        } else {
            $this->data['skill']        = $this->input->post('skill');
            $this->data['skill_level']  = $this->input->post('skill_level');
            $this->data['updated_by']   = $this->session->userdata('id');
            $this->data['updated_at']   = date('Y-m-d H:i:s');

            $id = $this->input->post('id');
            
            $simpan = $this->M_skill->update_skill_by_id($id, $this->data);

            if($simpan){
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    /*
>>>>>>> remotes/origin/Agung
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id']     = $users_id;
        $this->data['achievement']  = $this->M_achievement->get_all_achievement_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_achievement', $this->data);
    }
<<<<<<< HEAD
=======

    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_tambah($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->load->view($this->ajax_contents . 'v_achievement_tambah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UBAH DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_ubah($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['edit'] = $this->M_achievement->get_achievement_by_id($id);
        $this->load->view($this->ajax_contents . 'v_achievement_ubah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_achievement->delete_by_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_simpan(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        if($this->input->post('achievement_aksi') == 'tambah'){
            $this->data['achievement']  = $this->input->post('achievement');
            $this->data['users_id']     = $this->input->post('users_id');

            $cek = $this->M_achievement->check($this->data);
            if($cek->num_rows() > 0){
                echo "2";
            } else {
                $this->data['achievement_year'] = $this->input->post('achievement_year');
                $this->data['created_by']       = $this->session->userdata('id');
                $this->data['created_at']       = date('Y-m-d H:i:s');
                
                $simpan = $this->M_achievement->add($this->data);

                if($simpan){
                    echo "1";
                } else {
                    echo "0";
                }
            }
        } else {
            $this->data['achievement']      = $this->input->post('achievement');
            $this->data['achievement_year'] = $this->input->post('achievement_year');
            $this->data['updated_by']       = $this->session->userdata('id');
            $this->data['updated_at']       = date('Y-m-d H:i:s');

            $id = $this->input->post('id');
            
            $simpan = $this->M_achievement->update_achievement_by_id($id, $this->data);

            if($simpan){
                echo "1";
            } else {
                echo "0";
            }
        }
    }

>>>>>>> remotes/origin/Agung
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['role_id']     = $this->role_id;
        $this->data['users_id']     = $users_id;
        $this->data['contribution'] = $this->M_contribution->get_all_contribution_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_contribution', $this->data);
    }

<<<<<<< HEAD
     /*
=======
    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_tambah($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->load->view($this->ajax_contents . 'v_contribution_tambah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_ubah($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['edit'] = $this->M_contribution->get_contribution_by_id($id);
        $this->load->view($this->ajax_contents . 'v_contribution_ubah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_contribution->delete_by_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_simpan(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        if($this->input->post('contribution_aksi') == 'tambah'){
            $this->data['contribution']  = $this->input->post('contribution');
            $this->data['users_id']      = $this->input->post('users_id');

            $cek = $this->M_contribution->check($this->data);
            if($cek->num_rows() > 0){
                echo "2";
            } else {
                $this->data['contribution_year']    = $this->input->post('contribution_year');
                $this->data['contribution_position']= $this->input->post('contribution_position');
                $this->data['created_by']           = $this->session->userdata('id');
                $this->data['created_at']           = date('Y-m-d H:i:s');
                
                $simpan = $this->M_contribution->add($this->data);

                if($simpan){
                    echo "1";
                } else {
                    echo "0";
                }
            }
        } else {
            $this->data['contribution']             = $this->input->post('contribution');
            $this->data['contribution_year']        = $this->input->post('contribution_year');
            $this->data['contribution_position']    = $this->input->post('contribution_position');
            $this->data['updated_by']               = $this->session->userdata('id');
            $this->data['updated_at']               = date('Y-m-d H:i:s');

            $id = $this->input->post('id');
            
            $simpan = $this->M_contribution->update_contribution_by_id($id, $this->data);

            if($simpan){
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    /*
>>>>>>> remotes/origin/Agung
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id']     = $users_id;
        $this->data['experience']   = $this->M_experience->get_all_experience_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_experience', $this->data);
    }

<<<<<<< HEAD
}

   

   
    
=======
    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_tambah($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->load->view($this->ajax_contents . 'v_experience_tambah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UBAH DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_ubah($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['edit'] = $this->M_experience->get_experience_by_id($id);
        $this->load->view($this->ajax_contents . 'v_experience_ubah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_experience->delete_by_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_simpan(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        if($this->input->post('experience_aksi') == 'tambah'){
            $this->data['experience']   = $this->input->post('experience');
            $this->data['users_id']      = $this->input->post('users_id');

            $cek = $this->M_experience->check($this->data);
            if($cek->num_rows() > 0){
                echo "2";
            } else {
                $this->data['experience_year']      = $this->input->post('experience_year');
                $this->data['experience_position']  = $this->input->post('experience_position');
                $this->data['created_by']           = $this->session->userdata('id');
                $this->data['created_at']           = date('Y-m-d H:i:s');
                
                $simpan = $this->M_experience->add($this->data);

                if($simpan){
                    echo "1";
                } else {
                    echo "0";
                }
            }
        } else {
            $this->data['experience']           = $this->input->post('experience');
            $this->data['experience_year']      = $this->input->post('experience_year');
            $this->data['experience_position']  = $this->input->post('experience_position');
            $this->data['updated_by']           = $this->session->userdata('id');
            $this->data['updated_at']           = date('Y-m-d H:i:s');

            $id = $this->input->post('id');
            
            $simpan = $this->M_experience->update_experience_by_id($id, $this->data);

            if($simpan){
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN STATUS
    | -------------------------------------------------------------------
    */
    function ajax_post_status($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        // JIKA ADA SAMPUL 
        $path           = './uploads/forum/';
        $path_thumbs    = './uploads/forum/thumbs/';

        // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
        if(!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        
        if(!file_exists($path_thumbs)) {
            mkdir($path_thumbs, 0777, true);
        }

        $config['upload_path']      = $path;
        $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
        $config['overwrite']        = true;
        $config['encrypt_name']     = true;
        $config['remove_spaces']    = true;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('forum_cover')){
            
            $upload = $this->upload->data();

            $insertForum['forum_cover'] = $upload['file_name'];

            $this->resize_image($path . $insertForum['forum_cover'], $path_thumbs . $insertForum['forum_cover'], '300','169');
        }

        $insertForum['forum_id_parent']      = 0;
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);

        if($saveForum){
            echo json_encode(array('status' => TRUE));
        } else {
            echo json_encode(array('status' => FALSE));
        }
    }

    function ajax_automore_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $limit      = 25;
        $offset     = ($this->input->post('offset') ? $this->input->post('offset') : 0 );
        $response   = array();
        $forum      = $this->M_forum->get_all_more_post_profil($limit, $offset, $this->id);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = $offset + 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }


    function ajax_latest_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id   = $this->input->post('forum_id');
        $limit      = 25;
        $response   = array();
        $forum      = $this->M_forum->get_all_latest_post_profil($limit, $forum_id, $this->id);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA STATUS
    | -------------------------------------------------------------------
    */
    function ajax_status_hapus($id = null, $forum_id_parent = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit       = $this->M_forum->get_forum_by_id($id);
        $path       = './uploads/forum/';
        $path_thumbs= './uploads/forum/thumbs/';

        if($edit){            
            if(file_exists($path . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path . $edit['forum_cover']);
            }

            if(file_exists($path_thumbs . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path_thumbs . $edit['forum_cover']);
            }
        }

        $this->M_forum->delete_by_forum_id_parent($id);
        $this->M_forum->delete_by_id($id);
        
        $total_comment = 0;

        if($forum_id_parent){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($forum_id_parent);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($forum_id_parent, $updateForum);
        }

        echo $total_comment;
    }

    /*
    | -------------------------------------------------------------------
    | LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_like_status(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $data['forum_id'] = $this->input->get('forum_id');
        $data['users_id'] = $this->id;

        $checkData = $this->M_forum_like->check($data);

        if($checkData->num_rows() > 0){

            $this->M_forum_like->delete_by_forum_id_users_id($data['forum_id'], $data['users_id']);
            
            $status = 0;
        } else {
            $data['created_by']     = $this->id;
            $data['created_at']     = date('Y-m-d H:i:s');

            $this->M_forum_like->add($data);
            
            $status = 1;
        }

        $updateForum['total_like'] = $this->M_forum_like->get_total_like_by_forum_id($data['forum_id']);
        $updateForum['updated_by'] = $this->id;
        $updateForum['updated_at'] = date('Y-m-d H:i:s');
        
        $this->M_forum->update_forum_by_id($data['forum_id'], $updateForum);

        echo $status;
    }

    /*
    | -------------------------------------------------------------------
    | TOTAL LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_total_like(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');

        $forum = $this->M_forum->get_forum_by_id($forum_id);

        echo $forum['total_like'];
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_post_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $insertForum['forum_id_parent']      = $this->input->post('forum_id');
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);
        $total_comment = 0;

        if($saveForum){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($insertForum['forum_id_parent']);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($insertForum['forum_id_parent'], $updateForum);
        }

        echo $total_comment;
    }

     /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_list_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');
        $data['komentar']   = $this->M_forum->get_forum_by_forum_id_parent($forum_id);
        $data['role_id']    = $this->role_id;
        $data['users_id']   = $this->id;

        $this->load->view($this->ajax_contents . 'v_list_comment',  $data);
    }

    /*
    | -------------------------------------------------------------------
    | MODULE RESIZE IMAGE
    | -------------------------------------------------------------------
    */
    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }
}
>>>>>>> remotes/origin/Agung

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori_donasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        ($this->session->userdata('id') ? '' : redirect('login'));
        ($this->session->userdata('role_id') == 1 ? '' : redirect('backoffice/dashboard'));

        $this->load->model('M_kategori_donasi');

        $this->template = 'templates/v_backoffice';
        $this->contents = 'master/kategori_donasi/';

        $this->data = array();
    }

    function index()
    {
        $this->data['title']    = 'Daftar Kategori Donasi';
        $this->data['contents'] = $this->contents . 'v_index';

        $this->load->view($this->template, $this->data);
    }

    function ajax_list()
    {
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
        $datatables['table']    = 'tbl_kategori_donasi as t1';
        $datatables['id-table'] = 't1.id';

        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id', 't1.kategori_donasi');

        /* Jika menggunakan table join */
        $datatables['join']    = ' ';

        $this->M_kategori_donasi->get_datatables($datatables);
    }

    function tambah()
    {
        $this->form_validation->set_rules('kategori_donasi', 'Kategori donasi', 'required|min_length[3]|is_unique[tbl_kategori_donasi.kategori_donasi]');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('is_unique', '%s sudah tersedia!');

        if ($this->form_validation->run() == FALSE) {

            $this->data['title']    = 'Tambah Kategori Donasi';
            $this->data['contents'] = $this->contents . 'v_tambah';

            $this->load->view($this->template, $this->data);
        } else {
            $data['kategori_donasi'] = $this->input->post('kategori_donasi');
            $data['created_by']     = $this->session->userdata('id');
            $data['created_at']     = date('Y-m-d H:i:s');

            $simpan = $this->M_kategori_donasi->add($data);

            if ($simpan) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/master/kategori-donasi');
        }
    }

    function ubah($id = null)
    {
        if (!$id) {
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/master/kategori-donasi');
        }

        $this->form_validation->set_rules('kategori_donasi', 'Kategori donasi', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');

        if ($this->form_validation->run() == FALSE) {

            $edit = $this->M_kategori_donasi->get_kategori_donasi_by_id($id);

            if (!$edit) {
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/master/kategori-donasi');
            }

            $this->data['edit']     = $edit;
            $this->data['title']    = 'Ubah Kategori Donasi';
            $this->data['contents'] = $this->contents . 'v_ubah';

            $this->load->view($this->template, $this->data);
        } else {
            $data['kategori_donasi']    = $this->input->post('kategori_donasi');
            $data['updated_by']         = $this->session->userdata('id');
            $data['updated_at']         = date('Y-m-d H:i:s');

            $simpan = $this->M_kategori_donasi->update_kategori_donasi_by_id($id, $data);

            if ($simpan) {
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/master/kategori-donasi');
        }
    }

    function hapus($id = null)
    {
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_kategori_donasi->delete_by_id($id);

        echo json_encode(array('status' => true));
    }
}

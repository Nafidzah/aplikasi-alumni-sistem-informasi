<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

    function __construct(){
        parent::__construct();
        ($this->session->userdata('id') ? '' : redirect('login'));
		($this->session->userdata('role_id') == 1 ? '' : redirect('backoffice/dashboard'));

        $this->load->model('M_users');
        $this->load->model('M_role');

        $this->template = 'templates/v_backoffice';
        $this->contents = 'master/pengguna/';

        $this->data = array();
<<<<<<< HEAD

    }

    function index(){
        $this->data['title']    = 'Daftar Pengguna';
=======
    }

    function index(){
        $this->data['title']    = 'List of Users';
>>>>>>> Agung
        $this->data['contents'] = $this->contents. 'v_index';

        $this->load->view($this->template, $this->data);
    }

    function ajax_list(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
        $datatables['table']    = 'tbl_users as t1';
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t1.fullname', 't1.username', 't2.role');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' JOIN tbl_role as t2 ON t1.role_id = t2.id ';

        $this->M_users->get_datatables($datatables);
    }

    function tambah(){
        $this->form_validation->set_rules('email','Email', 'required');
<<<<<<< HEAD
        $this->form_validation->set_rules('fullname','Nama lengkap', 'required');
        $this->form_validation->set_rules('username','Username', 'required|min_length[3]|is_unique[tbl_user.username]');
        $this->form_validation->set_rules('password','Password', 'required|min_length[5]');
        $this->form_validation->set_rules('role_id','Role', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('is_unique', '%s sudah tersedia!');
=======
        $this->form_validation->set_rules('fullname','Fullname', 'required');
        $this->form_validation->set_rules('username','Username', 'required|min_length[3]|is_unique[tbl_users.username]');
        $this->form_validation->set_rules('password','Password', 'required|min_length[5]');
        $this->form_validation->set_rules('role_id','Role', 'required');

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');
>>>>>>> Agung

        if($this->form_validation->run() == FALSE){

            $this->data['role']     = $this->M_role->get_all_role();

<<<<<<< HEAD
            $this->data['title']    = 'Tambah Pengguna';
=======
            $this->data['title']    = 'Add Users';
>>>>>>> Agung
            $this->data['contents'] = $this->contents.'v_tambah';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA FOTO YANG DIUPLOAD     
            $path = './uploads/profil/';

<<<<<<< HEAD
=======
            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

>>>>>>> Agung
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('foto')){
                $upload = $this->upload->data();
                $data['foto'] = $upload['file_name'];
            }

            $data['email']          = $this->input->post('email');
            $data['fullname']       = $this->input->post('fullname');
            $data['username']       = $this->input->post('username');
            $data['password']       = sha1($this->input->post('password'));
            $data['role_id']        = $this->input->post('role_id');

            $data['tahun_angkatan'] = $this->input->post('tahun_angkatan');
            $data['jenis_kelamin']  = $this->input->post('jenis_kelamin');
            $data['website']        = $this->input->post('website');
            $data['linkedin']       = $this->input->post('linkedin');
            $data['facebook']       = $this->input->post('facebook');
            $data['twitter']        = $this->input->post('twitter');
            $data['instagram']      = $this->input->post('instagram');

            $data['created_by']     = $this->session->userdata('id');
            $data['created_at']     = date('Y-m-d H:i:s');

            $simpan = $this->M_users->add($data);

            if($simpan){
<<<<<<< HEAD
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
=======
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
>>>>>>> Agung
            }

            redirect('backoffice/master/pengguna');
        }        
    }

    function ubah($id = null){        
        if(!$id){
<<<<<<< HEAD
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
=======
            $this->session->set_flashdata('warning', 'Data not available!');
>>>>>>> Agung
            redirect('backoffice/master/pengguna');
        }

        $this->form_validation->set_rules('fullname','Nama lengkap', 'required');
        $this->form_validation->set_rules('role_id','Role', 'required');

        if($this->input->post('password')){
            $this->form_validation->set_rules('password','Password', 'required|min_length[3]');
        }

<<<<<<< HEAD
        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');
=======
        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');
>>>>>>> Agung

        if($this->form_validation->run() == FALSE){
            $edit = $this->M_users->get_users_by_id($id);
            
            if(!$edit){
<<<<<<< HEAD
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
=======
                $this->session->set_flashdata('warning', 'Data not available!');
>>>>>>> Agung
                redirect('backoffice/master/pengguna');
            }

            $this->data['role']     = $this->M_role->get_all_role();
            $this->data['edit']     = $edit;

<<<<<<< HEAD
            $this->data['title']    = 'Ubah Pengguna';
=======
            $this->data['title']    = 'Edit Users';
>>>>>>> Agung
            $this->data['contents'] = $this->contents.'v_ubah';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA FOTO YANG DIUPLOAD     
            $path = './uploads/profil/';

<<<<<<< HEAD
=======
            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

>>>>>>> Agung
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('foto')){
                $upload = $this->upload->data();
                $data['foto'] = $upload['file_name'];

                // HAPUS FOTO YANG LAMA
                $foto_old = $this->input->post('foto_old');
                if($foto_old && file_exists($path . $foto_old)){
                    @unlink($path . $foto_old);
                }

            } else {
                $data['foto'] = $this->input->post('foto_old');
            }

            $data['email']          = $this->input->post('email');
            $data['fullname']       = $this->input->post('fullname');
            $data['role_id']        = $this->input->post('role_id');

            $data['tahun_angkatan'] = $this->input->post('tahun_angkatan');
            $data['jenis_kelamin']  = $this->input->post('jenis_kelamin');
            $data['website']        = $this->input->post('website');
            $data['linkedin']       = $this->input->post('linkedin');
            $data['facebook']       = $this->input->post('facebook');
            $data['twitter']        = $this->input->post('twitter');
            $data['instagram']      = $this->input->post('instagram');

            $data['updated_by']     = $this->session->userdata('id');
            $data['updated_at']     = date('Y-m-d H:i:s');

            if($this->input->post('password')){
                $data['password']   = sha1($this->input->post('password'));
            }

            $simpan = $this->M_users->update_users_by_id($id, $data);

            if($simpan){
<<<<<<< HEAD
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
=======
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
>>>>>>> Agung
            }

            redirect('backoffice/master/pengguna');
        }        
    }

    function hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit = $this->M_users->get_users_by_id($id);
        $path = './uploads/profil/';
            
        if($edit && file_exists($path . $edit['foto']) && $edit['foto']){
            @unlink($path . $edit['foto']);
        }

        $this->M_users->delete_by_id($id);

        echo json_encode(array('status' => true));
    }
<<<<<<< HEAD
    
=======
>>>>>>> Agung
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('login'));
		($this->session->userdata('role_id') == 1 ? '' : redirect('backoffice/dashboard'));

        $this->load->model('M_role');

        $this->template = 'templates/v_backoffice';
        $this->contents = 'master/role/';

        $this->data = array();
    }

    function index(){
        $this->data['title']    = 'Role Pengguna';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
    }

    function ajax_list(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
        $datatables['table']    = 'tbl_role as t1';
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t1.role');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' ';

        $this->M_role->get_datatables($datatables);
    }

    function ubah($id){
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/master/role');
        }

        $this->form_validation->set_rules('role','Role pengguna', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');

        if($this->form_validation->run() == FALSE){
            $edit = $this->M_role->get_role_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/master/role');
            }

            $this->data['edit']     = $edit;
            $this->data['title']    = 'Ubah Role Pengguna';
            $this->data['contents'] = $this->contents.'v_ubah';

            $this->load->view($this->template, $this->data);
        } else {
            $data['role'] = $this->input->post('role');

            $simpan = $this->M_role->update_role_by_id($id, $data);
            
            if($simpan){
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/master/role');
        }        
    }
}
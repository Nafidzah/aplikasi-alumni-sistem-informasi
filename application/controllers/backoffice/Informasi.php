<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));

<<<<<<< HEAD
<<<<<<< HEAD
        $this->load->model(array('M_forum_like', 'M_forum', 'M_users', 'M_skill', 'M_achievement', 'M_contribution', 'M_experience'));

        $this->id       = $this->session->userdata('id');
        $this->role_id  = $this->session->userdata('role_id');
        $this->fullname = $this->session->userdata('fullname');
        $this->foto     = $this->session->userdata('foto');

        $this->template         = 'templates/v_backoffice';
        $this->contents         = 'informasi/';
        $this->ajax_contents    = 'backoffice/informasi/';
=======
        $this->load->model('M_users');
        $this->load->model('M_achievement');
        $this->load->model('M_contribution');
        $this->load->model('M_experience');
        $this->load->model('M_skill');
=======
        $this->load->model(array('M_forum_like', 'M_forum', 'M_users', 'M_skill', 'M_achievement', 'M_contribution', 'M_experience'));
>>>>>>> Agung

        $this->id       = $this->session->userdata('id');
        $this->role_id  = $this->session->userdata('role_id');
        $this->fullname = $this->session->userdata('fullname');
        $this->foto     = $this->session->userdata('foto');

<<<<<<< HEAD
        $this->template = 'templates/v_backoffice';
        $this->contents = 'informasi/';
>>>>>>> remotes/origin/Agung
=======
        $this->template         = 'templates/v_backoffice';
        $this->contents         = 'informasi/';
        $this->ajax_contents    = 'backoffice/informasi/';
>>>>>>> Agung
    }

    function index(){
        $this->data['role_id']  = $this->role_id;
        $this->data['title']    = 'Information Systems Alumni';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
    }

    function ajax_list(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
        $datatables['table']    = 'tbl_users as t1';
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t1.fullname', 't1.tahun_angkatan', 't3.experience', 't1.nomor_hp', 't1.email');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' JOIN tbl_role as t2 ON t1.role_id = t2.id 
                                    LEFT JOIN tbl_experience as t3 ON t1.experience_id_default = t3.id
<<<<<<< HEAD
<<<<<<< HEAD
=======

>>>>>>> remotes/origin/Agung
=======
>>>>>>> Agung
                                ';

        $this->M_users->get_datatables_alumni($datatables);
    }

    function tambah(){
<<<<<<< HEAD
<<<<<<< HEAD
=======

>>>>>>> remotes/origin/Agung
=======
>>>>>>> Agung
        // SELAIN ADMIN DILARANG
        if($this->role_id != 1){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/informasi');
        }

        $this->form_validation->set_rules('email','Email', 'required');
        $this->form_validation->set_rules('fullname','Nama lengkap', 'required');
        $this->form_validation->set_rules('username','Username', 'required|min_length[3]|is_unique[tbl_users.username]');
        $this->form_validation->set_rules('password','Password', 'required|min_length[5]');

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){
            $this->data['title']    = 'Add Alumni Information';
            $this->data['contents'] = $this->contents.'v_tambah';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA FOTO YANG DIUPLOAD     
            $path = './uploads/profil/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('foto')){
                
                $upload = $this->upload->data();

                $data['foto'] = $upload['file_name'];
            }

            $data['email']          = $this->input->post('email');
            $data['fullname']       = $this->input->post('fullname');
            $data['username']       = $this->input->post('username');
            $data['password']       = sha1($this->input->post('password'));
            $data['role_id']        = 2; // SET ALUMNI

            $data['tempat_lahir']   = $this->input->post('tempat_lahir');
            $data['tanggal_lahir']  = dateSql($this->input->post('tanggal_lahir'));
            $data['tahun_angkatan'] = $this->input->post('tahun_angkatan');
            $data['description']    = $this->input->post('description');
            $data['nomor_hp']       = $this->input->post('nomor_hp');
            $data['jenis_kelamin']  = $this->input->post('jenis_kelamin');
            $data['website']        = $this->input->post('website');
            $data['linkedin']       = $this->input->post('linkedin');
            $data['facebook']       = $this->input->post('facebook');
            $data['twitter']        = $this->input->post('twitter');
            $data['instagram']      = $this->input->post('instagram');

            $data['created_by']     = $this->session->userdata('id');
            $data['created_at']     = date('Y-m-d H:i:s');

            $simpan = $this->M_users->add($data);

            if($simpan){
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/informasi');
        }        
    }

    function ubah($id = null){        
        
        // SELAIN ADMIN DILARANG
        if($this->role_id != 1){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/informasi');
        }

        if(!$id){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/informasi');
        }

        $this->form_validation->set_rules('fullname','Fullname', 'required');
        $this->form_validation->set_rules('email','Email', 'required');

        if($this->input->post('password')){
            $this->form_validation->set_rules('password','Password', 'required|min_length[3]');
        }

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){
            $edit = $this->M_users->get_users_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data not available!');
                redirect('backoffice/master/pengguna');
            }

            $this->data['edit']     = $edit;

            $this->data['title']    = 'Edit Alumni Information';
            $this->data['contents'] = $this->contents.'v_ubah';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA FOTO YANG DIUPLOAD     
            $path = './uploads/profil/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('foto')){
                
                $upload = $this->upload->data();

                $data['foto'] = $upload['file_name'];

                // HAPUS FOTO YANG LAMA
                $foto_old = $this->input->post('foto_old');
                if($foto_old && file_exists($path . $foto_old)){
                    @unlink($path . $foto_old);
                }

            } else {
                $data['foto'] = $this->input->post('foto_old');
            }

            $data['email']          = $this->input->post('email');
            $data['fullname']       = $this->input->post('fullname');
            $data['tempat_lahir']   = $this->input->post('tempat_lahir');
            $data['tanggal_lahir']  = dateSql($this->input->post('tanggal_lahir'));

            $data['tahun_angkatan'] = $this->input->post('tahun_angkatan');
            $data['description']    = $this->input->post('description');
            $data['nomor_hp']       = $this->input->post('nomor_hp');
            $data['jenis_kelamin']  = $this->input->post('jenis_kelamin');
            $data['website']        = $this->input->post('website');
            $data['linkedin']       = $this->input->post('linkedin');
            $data['facebook']       = $this->input->post('facebook');
            $data['twitter']        = $this->input->post('twitter');
            $data['instagram']      = $this->input->post('instagram');

            $data['updated_by']     = $this->session->userdata('id');
            $data['updated_at']     = date('Y-m-d H:i:s');

            if($this->input->post('password')){
                $data['password']   = sha1($this->input->post('password'));
            }

            $simpan = $this->M_users->update_users_by_id($id, $data);

            if($simpan){
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/informasi');
        }        
    }

    function hapus($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit = $this->M_users->get_users_by_id($users_id);
        $path = './uploads/profil/';
            
        if($edit && file_exists($path . $edit['foto']) && $edit['foto']){
            @unlink($path . $edit['foto']);
        }

        $this->M_users->delete_by_id($users_id);
        $this->M_achievement->delete_by_users_id($users_id);
        $this->M_contribution->delete_by_users_id($users_id);
        $this->M_experience->delete_by_users_id($users_id);
        $this->M_skill->delete_by_users_id($users_id);

        echo json_encode(array('status' => true));
    }
<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> Agung

    function detail($users_id = null, $fullname = null){
        if(!$users_id){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/informasi');
        }

        $data['users_id'] = $this->id;
        $data['role_id']  = $this->role_id;
        $data['fullname'] = $this->fullname;
        $data['foto']     = $this->foto;
        $data['edit']     = $this->M_users->get_users_by_id($users_id);
        $data['contents'] = $this->contents.'v_detail';

        $this->load->view($this->template, $data);
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->data['skill']    = $this->M_skill->get_all_skill_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_skill', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id']     = $users_id;
        $this->data['achievement']  = $this->M_achievement->get_all_achievement_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_achievement', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

<<<<<<< HEAD
=======
        $this->data['role_id']      = $this->role_id;
>>>>>>> Agung
        $this->data['users_id']     = $users_id;
        $this->data['contribution'] = $this->M_contribution->get_all_contribution_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_contribution', $this->data);
    }

    /*
    | -------------------------------------------------------------------
<<<<<<< HEAD
=======
    | TAMBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_tambah($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id'] = $users_id;
        $this->load->view($this->ajax_contents . 'v_contribution_tambah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | UBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_ubah($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['edit'] = $this->M_contribution->get_contribution_by_id($id);
        $this->load->view($this->ajax_contents . 'v_contribution_ubah', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_contribution->delete_by_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution_simpan(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        if($this->input->post('contribution_aksi') == 'tambah'){
            $this->data['contribution']  = $this->input->post('contribution');
            $this->data['users_id']      = $this->input->post('users_id');

            $cek = $this->M_contribution->check($this->data);
            if($cek->num_rows() > 0){
                echo "2";
            } else {
                $this->data['contribution_year']    = $this->input->post('contribution_year');
                $this->data['contribution_position']= $this->input->post('contribution_position');
                $this->data['created_by']           = $this->input->post('users_id');
                $this->data['created_at']           = date('Y-m-d H:i:s');
                
                $simpan = $this->M_contribution->add($this->data);

                if($simpan){
                    echo "1";
                } else {
                    echo "0";
                }
            }
        } else {
            $this->data['contribution']             = $this->input->post('contribution');
            $this->data['contribution_year']        = $this->input->post('contribution_year');
            $this->data['contribution_position']    = $this->input->post('contribution_position');
            $this->data['updated_by']               = $this->input->post('users_id');
            $this->data['updated_at']               = date('Y-m-d H:i:s');

            $id = $this->input->post('id');
            
            $simpan = $this->M_contribution->update_contribution_by_id($id, $this->data);

            if($simpan){
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    /*
    | -------------------------------------------------------------------
>>>>>>> Agung
    | UNTUK MENAMPILKAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience_list($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->data['users_id']     = $users_id;
        $this->data['experience']   = $this->M_experience->get_all_experience_by_users_id($users_id);
        $this->load->view($this->ajax_contents . 'v_experience', $this->data);
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN STATUS
    | -------------------------------------------------------------------
    */
    function ajax_post_status($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        // JIKA ADA SAMPUL 
        $path           = './uploads/forum/';
        $path_thumbs    = './uploads/forum/thumbs/';

        // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
        if(!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        
        if(!file_exists($path_thumbs)) {
            mkdir($path_thumbs, 0777, true);
        }

        $config['upload_path']      = $path;
        $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
        $config['overwrite']        = true;
        $config['encrypt_name']     = true;
        $config['remove_spaces']    = true;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('forum_cover')){
            
            $upload = $this->upload->data();

            $insertForum['forum_cover'] = $upload['file_name'];

            $this->resize_image($path . $insertForum['forum_cover'], $path_thumbs . $insertForum['forum_cover'], '300','169');
        }

        $insertForum['forum_id_parent']      = 0;
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);

        if($saveForum){
            echo json_encode(array('status' => TRUE));
        } else {
            echo json_encode(array('status' => FALSE));
        }
    }

    function ajax_automore_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $users_id   = $this->input->post('users_id');
        $limit      = 25;
        $offset     = ($this->input->post('offset') ? $this->input->post('offset') : 0 );
        $response   = array();
        $forum      = $this->M_forum->get_all_more_post_profil($limit, $offset, $users_id);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = $offset + 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }


    function ajax_latest_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $users_id   = $this->input->post('users_id');
        $forum_id   = $this->input->post('forum_id');
        $limit      = 25;
        $response   = array();
        $forum      = $this->M_forum->get_all_latest_post_profil($limit, $forum_id, $users_id);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA STATUS
    | -------------------------------------------------------------------
    */
    function ajax_status_hapus($id = null, $forum_id_parent = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit       = $this->M_forum->get_forum_by_id($id);
        $path       = './uploads/forum/';
        $path_thumbs= './uploads/forum/thumbs/';

        if($edit){            
            if(file_exists($path . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path . $edit['forum_cover']);
            }

            if(file_exists($path_thumbs . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path_thumbs . $edit['forum_cover']);
            }
        }

        $this->M_forum->delete_by_forum_id_parent($id);
        $this->M_forum->delete_by_id($id);
        
        $total_comment = 0;

        if($forum_id_parent){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($forum_id_parent);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($forum_id_parent, $updateForum);
        }

        echo $total_comment;
    }

    /*
    | -------------------------------------------------------------------
    | LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_like_status(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $data['forum_id'] = $this->input->get('forum_id');
        $data['users_id'] = $this->id;

        $checkData = $this->M_forum_like->check($data);

        if($checkData->num_rows() > 0){

            $this->M_forum_like->delete_by_forum_id_users_id($data['forum_id'], $data['users_id']);
            
            $status = 0;
        } else {
            $data['created_by']     = $this->id;
            $data['created_at']     = date('Y-m-d H:i:s');

            $this->M_forum_like->add($data);
            
            $status = 1;
        }

        $updateForum['total_like'] = $this->M_forum_like->get_total_like_by_forum_id($data['forum_id']);
        $updateForum['updated_by'] = $this->id;
        $updateForum['updated_at'] = date('Y-m-d H:i:s');
        
        $this->M_forum->update_forum_by_id($data['forum_id'], $updateForum);

        echo $status;
    }

    /*
    | -------------------------------------------------------------------
    | TOTAL LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_total_like(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');

        $forum = $this->M_forum->get_forum_by_id($forum_id);

        echo $forum['total_like'];
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_post_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $insertForum['forum_id_parent']      = $this->input->post('forum_id');
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);
        $total_comment = 0;

        if($saveForum){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($insertForum['forum_id_parent']);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($insertForum['forum_id_parent'], $updateForum);
        }

        echo $total_comment;
    }

     /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_list_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');
        $data['komentar']   = $this->M_forum->get_forum_by_forum_id_parent($forum_id);
        $data['role_id']    = $this->role_id;
        $data['users_id']   = $this->id;

        $this->load->view($this->ajax_contents . 'v_list_comment',  $data);
    }

    /*
    | -------------------------------------------------------------------
    | MODULE RESIZE IMAGE
    | -------------------------------------------------------------------
    */
    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }
<<<<<<< HEAD
=======
>>>>>>> remotes/origin/Agung
=======
>>>>>>> Agung
}

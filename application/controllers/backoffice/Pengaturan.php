<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));

        $this->template = 'templates/v_backoffice';
        $this->contents = 'pengaturan/';
    }

    function index(){
        $this->data['title']    = 'Settings';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
    }
}

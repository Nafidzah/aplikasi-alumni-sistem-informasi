<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));

        $this->id           = $this->session->userdata('id');
        $this->role_id      = $this->session->userdata('role_id');
        $this->fullname     = $this->session->userdata('fullname');
        $this->foto         = $this->session->userdata('foto');

        $this->load->model(array('M_forum', 'M_forum_like', 'M_donor', 'M_donation', 'M_donation_category'));

        $this->template         = 'templates/v_backoffice';
        $this->contents         = 'forum/';
        $this->ajax_contents    = 'backoffice/forum/';
    }

    function index(){
        $data['role_id']  = $this->role_id;
        $data['fullname'] = $this->fullname;
        $data['foto']     = $this->foto;
        $data['title']    = 'Forum';
        $data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $data);
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN STATUS
    | -------------------------------------------------------------------
    */
    function ajax_post_status($users_id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        // JIKA ADA SAMPUL 
        $path           = './uploads/forum/';
        $path_thumbs    = './uploads/forum/thumbs/';

        // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
        if(!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        
        if(!file_exists($path_thumbs)) {
            mkdir($path_thumbs, 0777, true);
        }

        $config['upload_path']      = $path;
        $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
        $config['overwrite']        = true;
        $config['encrypt_name']     = true;
        $config['remove_spaces']    = true;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('forum_cover')){
            
            $upload = $this->upload->data();

            $insertForum['forum_cover'] = $upload['file_name'];

            $this->resize_image($path . $insertForum['forum_cover'], $path_thumbs . $insertForum['forum_cover'], '300','169');
        }

        $insertForum['forum_id_parent']      = 0;
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);

        if($saveForum){
            echo json_encode(array('status' => TRUE));
        } else {
            echo json_encode(array('status' => FALSE));
        }
    }

    function ajax_automore_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $limit      = 25;
        $offset     = ($this->input->post('offset') ? $this->input->post('offset') : 0 );
        $response   = array();
        $forum      = $this->M_forum->get_all_more_post($limit, $offset);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = $offset + 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }


    function ajax_latest_post(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id   = $this->input->post('forum_id');
        $limit      = 25;
        $response   = array();
        $forum      = $this->M_forum->get_all_latest_post($limit, $forum_id);

        if($forum){
            $data['users_id'] = $this->id;
            $data['role_id']  = $this->role_id;
            $data['fullname'] = $this->fullname;
            $data['foto']     = $this->foto;
            $data['offset']   = 1;
            $data['forum']    = $forum;

            $response['content']    = $this->load->view($this->ajax_contents . 'v_more_status',  $data, TRUE);
            $response['status']     = TRUE;
        } else {
            $response['status']     = FALSE;
        }  
        
        echo json_encode($response);   
    }

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA STATUS
    | -------------------------------------------------------------------
    */
    function ajax_status_hapus($id = null, $forum_id_parent = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit       = $this->M_forum->get_forum_by_id($id);
        $path       = './uploads/forum/';
        $path_thumbs= './uploads/forum/thumbs/';

        if($edit){            
            if(file_exists($path . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path . $edit['forum_cover']);
            }

            if(file_exists($path_thumbs . $edit['forum_cover']) && $edit['forum_cover']){
                @unlink($path_thumbs . $edit['forum_cover']);
            }
        }

        $this->M_forum->delete_by_forum_id_parent($id);
        $this->M_forum->delete_by_id($id);
        
        $total_comment = 0;

        if($forum_id_parent){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($forum_id_parent);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($forum_id_parent, $updateForum);
        }

        echo $total_comment;
    }

    /*
    | -------------------------------------------------------------------
    | LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_like_status(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $data['forum_id'] = $this->input->get('forum_id');
        $data['users_id'] = $this->id;

        $checkData = $this->M_forum_like->check($data);

        if($checkData->num_rows() > 0){

            $this->M_forum_like->delete_by_forum_id_users_id($data['forum_id'], $data['users_id']);
            
            $status = 0;
        } else {
            $data['created_by']     = $this->id;
            $data['created_at']     = date('Y-m-d H:i:s');

            $this->M_forum_like->add($data);
            
            $status = 1;
        }

        $updateForum['total_like'] = $this->M_forum_like->get_total_like_by_forum_id($data['forum_id']);
        $updateForum['updated_by'] = $this->id;
        $updateForum['updated_at'] = date('Y-m-d H:i:s');
        
        $this->M_forum->update_forum_by_id($data['forum_id'], $updateForum);

        echo $status;
    }

    /*
    | -------------------------------------------------------------------
    | TOTAL LIKE STATUS
    | -------------------------------------------------------------------
    */
    function ajax_total_like(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');

        $forum = $this->M_forum->get_forum_by_id($forum_id);

        echo $forum['total_like'];
    }

    /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_post_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $insertForum['forum_id_parent']      = $this->input->post('forum_id');
        $insertForum['forum_description']    = $this->input->post('forum_description');
        $insertForum['created_by']           = $this->session->userdata('id');
        $insertForum['created_at']           = date('Y-m-d H:i:s');

        $saveForum = $this->M_forum->add($insertForum);
        $total_comment = 0;

        if($saveForum){
            $total_comment = $this->M_forum->get_total_forum_by_forum_id($insertForum['forum_id_parent']);

            $updateForum['total_comment']   = $total_comment;
            $updateForum['updated_by']      = $this->id;
            $updateForum['updated_at']      = date('Y-m-d H:i:s');
        
            $this->M_forum->update_forum_by_id($insertForum['forum_id_parent'], $updateForum);
        }

        echo $total_comment;
    }

     /*
    | -------------------------------------------------------------------
    | SIMPAN COMMENT
    | -------------------------------------------------------------------
    */
    function ajax_list_comment(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $forum_id = $this->input->get('forum_id');
        $data['komentar']   = $this->M_forum->get_forum_by_forum_id_parent($forum_id);
        $data['role_id']    = $this->role_id;
        $data['users_id']   = $this->id;

        $this->load->view($this->ajax_contents . 'v_list_comment',  $data);
    }

    /*
    | -------------------------------------------------------------------
    | MODULE RESIZE IMAGE
    | -------------------------------------------------------------------
    */
    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }

    /*
    | -------------------------------------------------------------------
    | MODULE DONASI 
    | -------------------------------------------------------------------
    */

    function donasi($id = null){
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/forum');
        }

        $this->form_validation->set_rules('donation_category_id','Donation category', 'required');
        $this->form_validation->set_rules('donor_name','Donor name', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');

        if($this->form_validation->run() == FALSE){

            $edit = $this->M_donation->get_donation_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/forum');
            }

            $this->data['donation_category']    = $this->M_donation_category->get_all_donation_category();
            $this->data['edit']                 = $edit;
            $this->data['title']                = 'Donate Form';
            $this->data['contents']             = $this->contents.'v_donasi';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA DONASI EVIDENCE 
            $path           = './uploads/donasi_evidence/';
            $path_thumbs    = './uploads/donasi_evidence/thumbs/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('donor_evidence')){
                $upload = $this->upload->data();
                $data['donor_evidence'] = $upload['file_name'];

                # RESIZE FOTO
                $this->resize_image($path . $upload['file_name'], $path_thumbs . $upload['file_name'], '300','169');

            } 

            $data['donation_id']          = $id;
            $data['donation_category_id'] = $this->input->post('donation_category_id');
            $data['donor_name']           = $this->input->post('donor_name');
            $data['donor_description']    = $this->input->post('donor_description');
            $data['created_by']           = $this->session->userdata('id');
            $data['created_at']           = date('Y-m-d H:i:s');

            $simpan = $this->M_donor->add($data);

            if($simpan){
                $this->session->set_flashdata('success', 'Thank you for your donation!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/forum');
        }        
    }

    function donasi_alumni(){
        $this->form_validation->set_rules('donation_category_id','Donation category', 'required');
        $this->form_validation->set_rules('donor_name','Donor name', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');

        if($this->form_validation->run() == FALSE){

            $this->data['donation_category']    = $this->M_donation_category->get_all_donation_category();
            $this->data['title']                = 'Donation For Alumni';
            $this->data['contents']             = $this->contents.'v_donasi_alumni';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA DONASI EVIDENCE 
            $path           = './uploads/donasi_evidence/';
            $path_thumbs    = './uploads/donasi_evidence/thumbs/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('donor_evidence')){
                $upload = $this->upload->data();
                $data['donor_evidence'] = $upload['file_name'];

                # RESIZE FOTO
                $this->resize_image($path . $upload['file_name'], $path_thumbs . $upload['file_name'], '300','169');

            } 

            $data['donation_id']          = 0;
            $data['donation_category_id'] = $this->input->post('donation_category_id');
            $data['donor_name']           = $this->input->post('donor_name');
            $data['donor_description']    = $this->input->post('donor_description');
            $data['created_by']           = $this->session->userdata('id');
            $data['created_at']           = date('Y-m-d H:i:s');

            $simpan = $this->M_donor->add($data);

            if($simpan){
                $this->session->set_flashdata('success', 'Thank you for your donation!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/forum');
        }        
    }
}
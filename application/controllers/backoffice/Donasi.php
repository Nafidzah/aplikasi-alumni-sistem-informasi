<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donasi extends CI_Controller {

    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('login'));

<<<<<<< HEAD
<<<<<<< HEAD
        $this->load->model(array('M_donor', 'M_donation_category', 'M_donation'));
=======
        $this->load->model('M_donation');
>>>>>>> remotes/origin/Agung
=======
        $this->load->model(array('M_donor', 'M_donation_category', 'M_donation'));
>>>>>>> Agung

        $this->template = 'templates/v_backoffice';
        $this->contents = 'donasi/';

        $this->data = array();
    }

    function index(){
<<<<<<< HEAD
<<<<<<< HEAD
        $this->data['title']    = 'Donation Results';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
=======
        $role_id = $this->session->userdata('role_id');

        if($role_id == 1){
            $this->data['title']    = 'Daftar Donasi';
            $this->data['contents'] = $this->contents.'v_index';

            $this->load->view($this->template, $this->data);
        } else {
            echo 'HALAMAN DALAM PENGEMBANGAN';
        }
>>>>>>> remotes/origin/Agung
=======
        $this->data['title']    = 'Donation Results';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
>>>>>>> Agung
    }

    function ajax_list(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
<<<<<<< HEAD
<<<<<<< HEAD
        $datatables['table']    = 'tbl_donor as t1';
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t2.donation_title', 't1.donor_name', 't3.donation_category', 't1.donor_description');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' INNER JOIN tbl_donation as t2 ON t1.donation_id = t2.id
                                    INNER JOIN tbl_donation_category as t3 ON t1.donation_category_id = t3.id
                                ';

        $this->M_donor->get_datatables($datatables);
    }

    function ubah($id = null){
=======
        $datatables['table']    = 'tbl_donation as t1';
=======
        $datatables['table']    = 'tbl_donor as t1';
>>>>>>> Agung
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t2.donation_title', 't1.donor_name', 't3.donation_category', 't1.donor_description');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' INNER JOIN tbl_donation as t2 ON t1.donation_id = t2.id
                                    INNER JOIN tbl_donation_category as t3 ON t1.donation_category_id = t3.id
                                ';

        $this->M_donor->get_datatables($datatables);
    }

<<<<<<< HEAD
    function tambah(){
        $this->form_validation->set_rules('donation_description', 'Deskripsi donasi', 'required');
        $this->form_validation->set_rules('donation_title','Judul donasi', 'required|min_length[3]|is_unique[tbl_donation.donation_title]');
        
        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('is_unique', '%s sudah tersedia!');

        if($this->form_validation->run() == FALSE){

            $this->data['title']    = 'Tambah Donasi';
            $this->data['contents'] = $this->contents.'v_tambah';

            $this->load->view($this->template, $this->data);
        } else {

            // JIKA ADA COVER DONASI 
            $path           = './uploads/donasi/';
            $path_thumbs    = './uploads/donasi/thumbs/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            
            if (!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('donation_cover')){
                $upload = $this->upload->data();
                $this->data['donation_cover'] = $upload['file_name'];

                $source_path    = './uploads/donasi/' . $upload['file_name'];
                $path           = './uploads/donasi/thumbs/';

                # RESIZE FOTO
                $this->resize_image($source_path, $path . $upload['file_name'], '300','169');
            }

            $this->data['donation_title']       = $this->input->post('donation_title');
            $this->data['donation_description'] = $this->input->post('donation_description');
            $this->data['publish']              = $this->input->post('publish');
            $this->data['created_by']           = $this->session->userdata('id');
            $this->data['created_at']           = date('Y-m-d H:i:s');

            $simpan = $this->M_donation->add($this->data);

            if($simpan){
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/donasi');
        }        
    }

    function ubah($id = null){        
>>>>>>> remotes/origin/Agung
=======
    function ubah($id = null){
>>>>>>> Agung
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/donasi');
        }

<<<<<<< HEAD
<<<<<<< HEAD
        $this->form_validation->set_rules('donation_id','Donation name', 'required');
        $this->form_validation->set_rules('donation_category_id','Donation category', 'required');
        $this->form_validation->set_rules('donor_name','Donor name', 'required');
=======
        $this->form_validation->set_rules('donation_description', 'Deskripsi donasi', 'required');
        $this->form_validation->set_rules('donation_title','Judul donasi', 'required');
>>>>>>> remotes/origin/Agung
=======
        $this->form_validation->set_rules('donation_id','Donation name', 'required');
        $this->form_validation->set_rules('donation_category_id','Donation category', 'required');
        $this->form_validation->set_rules('donor_name','Donor name', 'required');
>>>>>>> Agung

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');

        if($this->form_validation->run() == FALSE){

<<<<<<< HEAD
<<<<<<< HEAD
            $edit = $this->M_donor->get_donor_by_id($id);
=======
            $edit = $this->M_donation->get_donation_by_id($id);
>>>>>>> remotes/origin/Agung
=======
            $edit = $this->M_donor->get_donor_by_id($id);
>>>>>>> Agung
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/donasi');
            }

<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> Agung
            $this->data['donation']             = $this->M_donation->get_all_donation();
            $this->data['donation_category']    = $this->M_donation_category->get_all_donation_category();
            $this->data['edit']                 = $edit;
            $this->data['title']                = 'Edit Donation Result';
            $this->data['contents']             = $this->contents.'v_ubah';
<<<<<<< HEAD
=======
            $this->data['edit']     = $edit;
            $this->data['title']    = 'Ubah Donasi';
            $this->data['contents'] = $this->contents.'v_ubah';
>>>>>>> remotes/origin/Agung
=======
>>>>>>> Agung

            $this->load->view($this->template, $this->data);
        } else {

<<<<<<< HEAD
<<<<<<< HEAD
            // JIKA ADA DONASI EVIDENCE 
            $path           = './uploads/donasi_evidence/';
            $path_thumbs    = './uploads/donasi_evidence/thumbs/';
=======
            // JIKA ADA COVER DONASI 
            $path           = './uploads/donasi/';
            $path_thumbs    = './uploads/donasi/thumbs/';
>>>>>>> remotes/origin/Agung
=======
            // JIKA ADA DONASI EVIDENCE 
            $path           = './uploads/donasi_evidence/';
            $path_thumbs    = './uploads/donasi_evidence/thumbs/';
>>>>>>> Agung

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if (!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

<<<<<<< HEAD
<<<<<<< HEAD
            if($this->upload->do_upload('donor_evidence')){
                $upload = $this->upload->data();
                $this->data['donor_evidence'] = $upload['file_name'];

                $source_path    = './uploads/donasi_evidence/' . $upload['file_name'];
=======
            if($this->upload->do_upload('donation_cover')){
=======
            if($this->upload->do_upload('donor_evidence')){
>>>>>>> Agung
                $upload = $this->upload->data();
                $this->data['donor_evidence'] = $upload['file_name'];

<<<<<<< HEAD
                $source_path    = './uploads/donasi/' . $upload['file_name'];
                $path           = './uploads/donasi/thumbs/';
>>>>>>> remotes/origin/Agung
=======
                $source_path    = './uploads/donasi_evidence/' . $upload['file_name'];
>>>>>>> Agung

                # RESIZE FOTO
                $this->resize_image($source_path, $path . $upload['file_name'], '300','169');

                // HAPUS COVER YANG LAMA
<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> Agung
                $donor_evidence_old = $this->input->post('donor_evidence_old');
                if($donor_evidence_old && file_exists($path . $donor_evidence_old)){
                    @unlink($path . $donor_evidence_old);
                }
                
                if($donor_evidence_old && file_exists($path_thumbs . $donor_evidence_old)){
                    @unlink($path_thumbs . $donor_evidence_old);
<<<<<<< HEAD
                }

            } else {
                $this->data['donor_evidence'] = $this->input->post('donor_evidence_old');
            }

            $this->data['donation_id']          = $this->input->post('donation_id');
            $this->data['donation_category_id'] = $this->input->post('donation_category_id');
            $this->data['donor_name']           = $this->input->post('donor_name');
            $this->data['donor_description']    = $this->input->post('donor_description');
            $this->data['updated_by']           = $this->session->userdata('id');
            $this->data['updated_at']           = date('Y-m-d H:i:s');

            $simpan = $this->M_donor->update_donor_by_id($id, $this->data);
=======
                $donation_cover_old = $this->input->post('donation_cover_old');
                if($donation_cover_old && file_exists($path . $donation_cover_old)){
                    @unlink($path . $donation_cover_old);
=======
>>>>>>> Agung
                }

            } else {
                $this->data['donor_evidence'] = $this->input->post('donor_evidence_old');
            }

            $this->data['donation_id']          = $this->input->post('donation_id');
            $this->data['donation_category_id'] = $this->input->post('donation_category_id');
            $this->data['donor_name']           = $this->input->post('donor_name');
            $this->data['donor_description']    = $this->input->post('donor_description');
            $this->data['updated_by']           = $this->session->userdata('id');
            $this->data['updated_at']           = date('Y-m-d H:i:s');

<<<<<<< HEAD
            $simpan = $this->M_donation->update_donation_by_id($id, $this->data);
>>>>>>> remotes/origin/Agung
=======
            $simpan = $this->M_donor->update_donor_by_id($id, $this->data);
>>>>>>> Agung

            if($simpan){
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/donasi');
        }        
    }

<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> Agung
    function detail($id = null){
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/donasi');
        }
<<<<<<< HEAD

        $edit = $this->M_donor->get_donor_by_id($id);
        
        if(!$edit){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/donasi');
        }

        $this->data['donation']             = $this->M_donation->get_all_donation();
        $this->data['donation_category']    = $this->M_donation_category->get_all_donation_category();
        $this->data['edit']                 = $edit;
        $this->data['title']                = 'Detail Donation Result';
        $this->data['contents']             = $this->contents.'v_detail';

        $this->load->view($this->template, $this->data);     
    }

    function hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit          = $this->M_donor->get_donor_by_id($id);
        $path_source    = './uploads/donor_evidence/';
        $path_thumbs    = './uploads/donor_evidence/thumbs/';

        if($edit){
            
            if(file_exists($path_source . $edit['donor_evidence']) && $edit['donor_evidence']){
                @unlink($path_source . $edit['donor_evidence']);
            }

            if(file_exists($path_thumbs . $edit['donor_evidence']) && $edit['donor_evidence']){
                @unlink($path_thumbs . $edit['donor_evidence']);
            }
        }

        $this->M_donor->delete_by_id($id);
=======
    function hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');
=======
>>>>>>> Agung

        $edit = $this->M_donor->get_donor_by_id($id);
        
        if(!$edit){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/donasi');
        }

        $this->data['donation']             = $this->M_donation->get_all_donation();
        $this->data['donation_category']    = $this->M_donation_category->get_all_donation_category();
        $this->data['edit']                 = $edit;
        $this->data['title']                = 'Detail Donation Result';
        $this->data['contents']             = $this->contents.'v_detail';

        $this->load->view($this->template, $this->data);     
    }

    function hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit          = $this->M_donor->get_donor_by_id($id);
        $path_source    = './uploads/donor_evidence/';
        $path_thumbs    = './uploads/donor_evidence/thumbs/';

        if($edit){
            
            if(file_exists($path_source . $edit['donor_evidence']) && $edit['donor_evidence']){
                @unlink($path_source . $edit['donor_evidence']);
            }

            if(file_exists($path_thumbs . $edit['donor_evidence']) && $edit['donor_evidence']){
                @unlink($path_thumbs . $edit['donor_evidence']);
            }
        }

<<<<<<< HEAD
        $this->M_donation->delete_by_id($id);
>>>>>>> remotes/origin/Agung
=======
        $this->M_donor->delete_by_id($id);
>>>>>>> Agung

        echo json_encode(array('status' => true));
    }

<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> Agung
    /*
    | -------------------------------------------------------------------
    | MODULE RESIZE IMAGE
    | -------------------------------------------------------------------
    */
<<<<<<< HEAD
=======
>>>>>>> remotes/origin/Agung
=======
>>>>>>> Agung
    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }
}
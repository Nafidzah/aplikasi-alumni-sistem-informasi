<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));
        ($this->session->userdata('role_id') == 2 ? redirect('backoffice/forum') : '');
        ($this->session->userdata('role_id') == 3 ? redirect('backoffice/survei') : '');

        $this->load->model(array('M_donation', 'M_forum', 'M_survey', 'M_survey_question', 'M_survey_result'));

        $this->role_id  = $this->session->userdata('role_id');

        $this->template = 'templates/v_backoffice';
        $this->contents = 'dashboard/';
    }

    function index(){
        $data['role_id']  = $this->role_id;
        $data['title']    = 'Home';
        $data['contents'] = $this->contents.'v_dashboard';

        $this->load->view($this->template, $data);
    }

    /*
    | -------------------------------------------------------------------
    | MODULE DONASI
    | -------------------------------------------------------------------
    */
    function ajax_list_donasi(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables             = $_POST;
        $datatables['table']    = 'tbl_donation as t1';
        $datatables['id-table'] = 't1.id';
        
        $datatables['col-display'] = array('t1.id','t1.donation_cover', 't1.donation_title', 't1.donation_description', 't1.publish');
        
        $datatables['join']    = ' ';

        $this->M_donation->get_datatables($datatables);
    }

    function tambah_donasi(){
        $this->form_validation->set_rules('donation_description', 'Description', 'required');
        $this->form_validation->set_rules('donation_title','Name/Title', 'required|min_length[3]|is_unique[tbl_donation.donation_title]');
        
        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){

            $data['title']    = 'Add Donation';
            $data['contents'] = $this->contents.'v_tambah_donasi';

            $this->load->view($this->template, $data);
        } else {

            // JIKA ADA COVER DONASI 
            $path           = './uploads/donasi/';
            $path_thumbs    = './uploads/donasi/thumbs/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            
            if (!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('donation_cover')){
                
                $upload = $this->upload->data();

                $insertDonation['donation_cover'] = $upload['file_name'];

                $this->resize_image($path . $insertDonation['donation_cover'], $path_thumbs . $insertDonation['donation_cover'], '300','169');
            }

            $insertDonation['donation_title']       = $this->input->post('donation_title');
            $insertDonation['donation_description'] = $this->input->post('donation_description');
            $insertDonation['publish']              = $this->input->post('publish');
            $insertDonation['created_by']           = $this->session->userdata('id');
            $insertDonation['created_at']           = date('Y-m-d H:i:s');

            $donationId = $this->M_donation->add($insertDonation);

            if($donationId){
                if($insertDonation['publish'] == 'Y'){
                    $insertForum['donation_id'] = $donationId;
                    $insertForum['created_by']  = $this->session->userdata('id');
                    $insertForum['created_at']  = date('Y-m-d H:i:s');

                    $saveForum = $this->M_forum->add($insertForum);
                }

                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/dashboard');
        }        
    }

    function ubah_donasi($id = null){        
        if(!$id){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/dashboard');
        }

        $this->form_validation->set_rules('donation_description', 'Description', 'required');
        $this->form_validation->set_rules('donation_title','Name/Title', 'required');

        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){

            $edit = $this->M_donation->get_donation_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data not available!');
                redirect('backoffice/dashboard');
            }

            $data['edit']     = $edit;
            $data['title']    = 'Edit Donation';
            $data['contents'] = $this->contents.'v_ubah_donasi';

            $this->load->view($this->template, $data);
        } else {

            // JIKA ADA SAMPUL 
            $path           = './uploads/donasi/';
            $path_thumbs    = './uploads/donasi/thumbs/';

            // JIKA BELUM ADA FOLDERNYA, MAKA DIBUAT
            if(!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if(!file_exists($path_thumbs)) {
                mkdir($path_thumbs, 0777, true);
            }

            $config['upload_path']      = $path;
            $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
            $config['overwrite']        = true;
            $config['encrypt_name']     = true;
            $config['remove_spaces']    = true;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('donation_cover')){
                
                $upload = $this->upload->data();

                $updateDonation['donation_cover'] = $upload['file_name'];

                $this->resize_image($path . $updateDonation['donation_cover'], $path_thumbs . $updateDonation['donation_cover'], '300','169');

                $donation_cover_old = $this->input->post('donation_cover_old');
                if($donation_cover_old && file_exists($path . $donation_cover_old)){
                    @unlink($path . $donation_cover_old);
                }

            } else {
                $updateDonation['donation_cover'] = $this->input->post('donation_cover_old');
            }

            $updateDonation['donation_title']       = $this->input->post('donation_title');
            $updateDonation['donation_description'] = $this->input->post('donation_description');
            $updateDonation['publish']              = $this->input->post('publish');
            $updateDonation['updated_by']           = $this->session->userdata('id');
            $updateDonation['updated_at']           = date('Y-m-d H:i:s');

            $saveDonation = $this->M_donation->update_donation_by_id($id, $updateDonation);

            if($saveDonation){
                
                $checkDonation['donation_id'] = $id;
                $check = $this->M_forum->check($checkDonation);

                if($check->num_rows() == 0 && $updateDonation['publish'] == 'Y'){
                    $insertForum['donation_id'] = $id;
                    $insertForum['created_by']  = $this->session->userdata('id');
                    $insertForum['created_at']  = date('Y-m-d H:i:s');

                    $saveForum = $this->M_forum->add($insertForum);
                }

                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/dashboard');
        }        
    }

    function hapus_donasi($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $edit           = $this->M_donation->get_donation_by_id($id);
        $path           = './upload/donasi/';
        $path_thumbs    = './upload/donasi/thumbs/';
        
        if($edit['donation_cover'] && file_exists($path . $edit['donation_cover'])){
            @unlink($path . $edit['donation_cover']);
        }
        
        if($edit['donation_cover'] && file_exists($path_thumbs . $edit['donation_cover'])){
            @unlink($path_thumbs . $edit['donation_cover']);
        }

        $this->M_donation->delete_by_id($id);
        $this->M_forum->delete_by_forum_donation_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | MODULE SURVEI
    | -------------------------------------------------------------------
    */
    function ajax_list_survei(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables             = $_POST;
        $datatables['table']    = 'tbl_survey as t1';
        $datatables['id-table'] = 't1.id';
        
        $datatables['col-display'] = array('t1.id','t1.survey_name', 't1.start_date', 't1.end_date', 't1.respondent', 't1.publish');
        
        $datatables['join']    = ' ';

        $this->M_survey->get_datatables($datatables);
    }

    function tambah_survei(){
        $this->form_validation->set_rules('survey_description', 'Description', 'required');
        $this->form_validation->set_rules('survey_name','Name', 'required|min_length[3]|is_unique[tbl_survey.survey_name]');
        
        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){

            $data['title']    = 'Add Survey';
            $data['contents'] = $this->contents.'v_tambah_survei';

            $this->load->view($this->template, $data);
        } else {
            $insertSurvey['respondent']         = join(',', $this->input->post('respondent'));
            $insertSurvey['survey_name']        = $this->input->post('survey_name');
            $insertSurvey['survey_description'] = $this->input->post('survey_description');
            $insertSurvey['start_date']         = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
            $insertSurvey['start_time']         = $this->input->post('start_time');
            $insertSurvey['end_date']           = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
            $insertSurvey['end_time']           = $this->input->post('end_time');
            $insertSurvey['publish']            = $this->input->post('publish');
            $insertSurvey['created_by']         = $this->session->userdata('id');
            $insertSurvey['created_at']         = date('Y-m-d H:i:s');

            $survey_id  = $this->M_survey->add($insertSurvey);
            $question   = $this->input->post('question');

            if($question){
                foreach($question as $key => $value){
                    $answerInput    = $this->input->post('answer_' . $value);
                    $answer         = array();

                    foreach($answerInput as $key2 => $value2){
                        $val_post = $this->input->post('answer_' . $value2);
                        $val_post = str_replace(',', ';', $val_post);
                        $answer[] = $val_post;
                    }
                    $answer = join(',', $answer);

                    $insertSurveyQuestion['survey_id']      = $survey_id;
                    $insertSurveyQuestion['question']       = $this->input->post('question_' . $value);
                    $insertSurveyQuestion['answer_type']    = $this->input->post('answer_type_' . $value);
                    $insertSurveyQuestion['answer']         = $answer;
                    $insertSurveyQuestion['created_by']     = $this->session->userdata('id');
                    $insertSurveyQuestion['created_at']     = date('Y-m-d H:i:s');

                    $this->M_survey_question->add($insertSurveyQuestion);
                }
            }

            if($survey_id){
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/dashboard');

        }

    }
    
    function ubah_survei($id = null){
        if(!$id){
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/dashboard');
        }

        $this->form_validation->set_rules('survey_description', 'Description', 'required');
        $this->form_validation->set_rules('survey_name','Name', 'required|min_length[3]');
        
        $this->form_validation->set_message('required', '%s is required!');
        $this->form_validation->set_message('min_length', 'Minimum %s must %s characters!');
        $this->form_validation->set_message('is_unique', '%s already available!');
        $this->form_validation->set_message('matches', '%s not same %s !');

        if($this->form_validation->run() == FALSE){
            $edit = $this->M_survey->get_survey_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data not available!');
                redirect('backoffice/dashboard');
            }

            $data['edit']     = $edit;
            $data['question'] = $this->M_survey_question->get_all_survei_question_by_survey_id($id);
            $data['title']    = 'Edit Survey';
            $data['contents'] = $this->contents.'v_ubah_survei';

            $this->load->view($this->template, $data);
        } else {
            $updateSurvey['respondent']         = join(',', $this->input->post('respondent'));
            $updateSurvey['survey_name']        = $this->input->post('survey_name');
            $updateSurvey['survey_description'] = $this->input->post('survey_description');
            $updateSurvey['start_date']         = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date'))));
            $updateSurvey['start_time']         = $this->input->post('start_time');
            $updateSurvey['end_date']           = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date'))));
            $updateSurvey['end_time']           = $this->input->post('end_time');
            $updateSurvey['publish']            = $this->input->post('publish');
            $updateSurvey['updated_by']         = $this->session->userdata('id');
            $updateSurvey['updated_at']         = date('Y-m-d H:i:s');

            $survey_id      = $this->M_survey->update_survey_by_id($id, $updateSurvey);
            $question       = $this->input->post('question');
            $question_db    = $this->M_survey_question->get_array_survei_question_by_survey_id($id);
            $question_post  = array();

            if($question){
                foreach($question as $key => $value){
                    $survey_question_id = $this->input->post('survey_question_id_' . $value);                    
                    $answerInput        = $this->input->post('answer_' . $value);
                    $answerType         = $this->input->post('answer_type_' . $value);
                    $question           = $this->input->post('question_' . $value);
                    $answer             = array();
                    $data_answer        = '';

                    if($answerType == 'radio' || $answerType == 'checkbox'){
                        foreach($answerInput as $key2 => $value2){
                            $val_post = $this->input->post('answer_' . $value2);
                            $val_post = str_replace(',', ';', $val_post);
                            $answer[] = $val_post;
                        }

                        $data_answer = join(',', $answer);
                    }

                    $insertSurveyQuestion = array();
                    $insertSurveyQuestion['survey_id']      = $id;
                    $insertSurveyQuestion['question']       = $question;
                    $insertSurveyQuestion['answer_type']    = $answerType;
                    $insertSurveyQuestion['answer']         = $data_answer;

                    if($survey_question_id){
                        $question_post[] = $survey_question_id;
                        $insertSurveyQuestion['updated_by']     = $this->session->userdata('id');
                        $insertSurveyQuestion['updated_at']     = date('Y-m-d H:i:s');

                        $this->M_survey_question->update_survey_question_by_id($survey_question_id, $insertSurveyQuestion);
                    } else {
                        $insertSurveyQuestion['created_by']     = $this->session->userdata('id');
                        $insertSurveyQuestion['created_at']     = date('Y-m-d H:i:s');

                        $this->M_survey_question->add($insertSurveyQuestion);
                    }
                }
            }

            $array_diff = array_diff($question_db, $question_post);

            if($array_diff){
                foreach($array_diff as $id){
                    $this->M_survey_question->delete_by_id($id);
                }
            }

            if($survey_id){
                $this->session->set_flashdata('success', 'Data saved successfully!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/dashboard');

        }

    }

    function hapus_survei($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $this->M_survey->delete_by_id($id);
        $this->M_survey_question->delete_by_survey_id($id);
        $this->M_survey_result->delete_by_survey_id($id);

        echo json_encode(array('status' => true));
    }

    /*
    | -------------------------------------------------------------------
    | MODULE RESIZE IMAGE
    | -------------------------------------------------------------------
    */
    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }
}

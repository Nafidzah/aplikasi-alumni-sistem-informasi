<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Survei extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        ($this->session->userdata('id') ? '' : redirect('auth/login'));

        $this->role_id  = $this->session->userdata('role_id');
        $this->id       = $this->session->userdata('id');

        $this->load->model(array('M_survey', 'M_survey_question', 'M_survey_result'));

        $this->template = 'templates/v_backoffice';
        $this->contents = 'survei/';
    }

    function index()
    {
        if ($this->role_id == 1) {
            $data['survei']     = $this->M_survey->get_all_survey();
            $data['title']      = 'Survey Result';
            $data['contents']   = $this->contents . 'v_index_admin';

            $this->load->view($this->template, $data);
        } else {
            $data['survei']     = $this->M_survey->get_all_survey();
            $data['title']      = 'List of Survey';
            $data['contents']   = $this->contents . 'v_index';

            $this->load->view($this->template, $data);
        }
    }

    function ajax_list_survei_result()
    {
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables             = $_POST;
        $datatables['table']    = 'tbl_survey as t1';
        $datatables['id-table'] = 't1.id';

        $datatables['col-display'] = array('t1.id', 't1.survey_name', 't1.start_date', 't1.end_date', 't1.respondent', 't1.publish');

        $datatables['join']    = ' ';

        $this->M_survey->get_datatables_survey_result($datatables);
    }

    function form($id = null)
    {
        if (!$id) {
            $this->session->set_flashdata('warning', 'Data not found!');
            redirect('backoffice/survei');
        }

        // CEK APAKAH SUDAH ISI SURVEY?
        $check['created_by']    = $this->id;
        $check['survey_id']     = $id;

        $total = $this->M_survey_result->check($check);
        if ($total->num_rows() > 0 && $this->role_id == 2) {
            $this->session->set_flashdata('warning', 'You have completed the survey!');
            redirect('backoffice/survei');
        }

        if ($this->input->post()) {
            $survey_question = $this->input->post('survey_question');
            if ($survey_question) {
                foreach ($survey_question as $survey_question_id) {
                    $answer_type    = $this->input->post('answer_type_' . $survey_question_id);
                    $answer_result  = $this->input->post('answer_result_' . $survey_question_id);
                    $answer         = array();

                    if ($answer_type == 'checkbox' && $answer_result) {
                        foreach ($answer_result as $key => $value) {
                            $value = str_replace(',', ';', $value);
                            $answer[] = $value;
                        }

                        $answer_result = join(',', $answer);
                    } else if ($answer_type == 'radio' && $answer_result) {
                        $answer_result = str_replace(',', ';', $answer_result);
                    }

                    $data = array();
                    $data['survey_id']              = $id;
                    $data['survey_question_id']     = $survey_question_id;
                    $data['answer_result']          = $answer_result;
                    $data['created_at']             = date('Y-m-d H:i:s');
                    $data['created_by']             = $this->session->userdata('id');

                    $this->M_survey_result->add($data);
                }

                $this->session->set_flashdata('success', 'Survey has been submitted!');
            } else {
                $this->session->set_flashdata('error', 'Data could not be saved!');
            }

            redirect('backoffice/survei');
        }

        $edit = $this->M_survey->get_survey_by_id($id);

        if (!$edit) {
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/survei');
        }

        $data['edit']           = $edit;
        $data['question']       = $this->M_survey_question->get_all_survei_question_by_survey_id($id);
        $data['title']          = 'Form Survei';
        $data['contents']       = $this->contents . 'v_form';

        $this->load->view($this->template, $data);
    }

    function detail($id = null)
    {
        if (!$id) {
            $this->session->set_flashdata('warning', 'Data not found!');
            redirect('backoffice/survei');
        }

        $edit = $this->M_survey->get_survey_by_id($id);
        if (!$edit) {
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/survei');
        }

        $group_by_tanggal   = $this->M_survey_result->get_group_by_created_at_by_survei_id($id);
        $chart_label        = '';
        $chart_data         = '';

        foreach ($group_by_tanggal as $row) {
            $chart_label .= "'" . date('d M Y', strtotime($row['tanggal'])) . "', ";
            $chart_data .= $row['total'] . ",";
        }

        $data['chart_label'] = $chart_label;
        $data['chart_data'] = $chart_data;
        $data['edit']       = $edit;
        $data['question']   = $this->M_survey_question->get_all_survei_question_by_survey_id($id);
        $data['respondent'] = $this->M_survey_result->get_respondent_by_survei_id($id);
        $data['title']      = 'Summary Respondent Survey';
        $data['contents']   = $this->contents . 'v_detail';

        $this->load->view($this->template, $data);
    }

    function ajax_result($survey_id, $created_by)
    {
        (!$this->input->is_ajax_request() ? show_404() : '');

        $data['survey_id']      = $survey_id;
        $data['created_by']     = $created_by;
        $data['edit']           = $this->M_survey->get_survey_by_id($survey_id);
        $data['question']       = $this->M_survey_question->get_all_survei_question_by_survey_id($survey_id);

        $this->load->view('backoffice/survei/v_result', $data);
    }

    function export($id = null)
    {
        if (!$id) {
            $this->session->set_flashdata('warning', 'Data not found!');
            redirect('backoffice/survei');
        }

        $edit = $this->M_survey->get_survey_by_id($id);
        if (!$edit) {
            $this->session->set_flashdata('warning', 'Data not available!');
            redirect('backoffice/survei');
        }

        $data['edit']       = $edit;
        $data['question']   = $this->M_survey_question->get_all_survei_question_by_survey_id($id);
        $data['respondent'] = $this->M_survey_result->get_respondent_by_survei_id($id);

        $this->load->view('backoffice/survei/v_export', $data);
    }
}

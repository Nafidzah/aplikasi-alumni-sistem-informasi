<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumentasi extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));

        $this->load->model('M_album');
        $this->load->model('M_album_photo');
        $this->load->model('M_photo');

        $this->load->library('pagination');

        $this->template         = 'templates/v_backoffice';
        $this->contents         = 'dokumentasi/';
        $this->ajax_contents    = 'backoffice/dokumentasi/';
    }

    function index(){
        $role_id = $this->session->userdata('role_id');

        if($role_id == 1){

            $this->data['title']    = 'Documentation';
            $this->data['contents'] = $this->contents.'v_index_admin';
            
        } else {
            $config['base_url']         = base_url('backoffice/dokumentasi/index');
            $config['total_rows']       = $this->M_album->get_total_all_album_pagination();
            $config['per_page']         = 10;
            $config["uri_segment"]      = 4;
            $config["num_links"]        = floor($config["total_rows"] / $config["per_page"]);
            $config['first_link']       = 'First';
            $config['last_link']        = 'Last';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';

            $this->pagination->initialize($config);
            $this->data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $this->data['album']        = $this->M_album->get_all_album_pagination($config["per_page"], $this->data['page']); 
            $this->data['pagination']   = $this->pagination->create_links();
            $this->data['title']        = 'Documentation';
            $this->data['contents']     = $this->contents.'v_index_alumni';
        }

        $this->load->view($this->template, $this->data);
    }

    function ajax_list(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $datatables  = $_POST;
        $datatables['table']    = 'tbl_album as t1';
        $datatables['id-table'] = 't1.id';
        
        /* Kolom yang ditampilkan */
        $datatables['col-display'] = array('t1.id','t1.album_cover', 't1.album_name', 't1.album_description', 't1.publish');
        
        /* Jika menggunakan table join */
        $datatables['join']    = ' ';

        $this->M_album->get_datatables($datatables);
    }

    function tambah(){
        $this->form_validation->set_rules('album_name','Judul dokumentasi', 'required|min_length[3]|is_unique[tbl_album.album_name]');
        
        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('is_unique', '%s sudah tersedia!');

        if($this->form_validation->run() == FALSE){

            $this->data['title']    = 'Add Documentation';
            $this->data['contents'] = $this->contents.'v_tambah';

            $this->load->view($this->template, $this->data);
        } else {
            $this->data['album_name']         = $this->input->post('album_name');
            $this->data['album_description']  = $this->input->post('album_description');
            $this->data['publish']            = $this->input->post('publish');
            $this->data['created_by']         = $this->session->userdata('id');
            $this->data['created_at']         = date('Y-m-d H:i:s');

            $simpan = $this->M_album->add($this->data);

            if($simpan){
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/dokumentasi');
        }        
    }

    function ubah($id = null){
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/dokumentasi');
        }

        $this->form_validation->set_rules('album_name','Judul dokumentasi', 'required');

        $this->form_validation->set_message('required', '%s wajib diisi!');
        $this->form_validation->set_message('min_length', 'Minimal %s harus %s karakter!');
        $this->form_validation->set_message('matches', '%s tidak sama %s !');

        if($this->form_validation->run() == FALSE){

            $edit = $this->M_album->get_album_by_id($id);
            
            if(!$edit){
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/dokumentasi');
            }

            $this->data['edit']     = $edit;
            $this->data['title']    = 'Edit Documentation';
            $this->data['contents'] = $this->contents.'v_ubah';

            $this->load->view($this->template, $this->data);
        } else {
            $this->data['album_name']         = $this->input->post('album_name');
            $this->data['album_description']  = $this->input->post('album_description');
            $this->data['publish']            = $this->input->post('publish');
            $this->data['updated_by']         = $this->session->userdata('id');
            $this->data['updated_at']         = date('Y-m-d H:i:s');

            $simpan = $this->M_album->update_album_by_id($id, $this->data);

            if($simpan){
                $this->session->set_flashdata('success', 'Data berhasil disimpan!');
            } else {
                $this->session->set_flashdata('error', 'Data gagal disimpan!');
            }

            redirect('backoffice/dokumentasi');
        }        
    }

    function foto($id = null){        
        if(!$id){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/dokumentasi');
        }

        $edit = $this->M_album->get_album_by_id($id);
        
        if(!$edit){
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/dokumentasi');
        }

        $this->data['edit']     = $edit;
        $this->data['title']    = 'Photo of Documentation';
        $this->data['contents'] = $this->contents.'v_foto';

        $this->load->view($this->template, $this->data);
    }

    function hapus($id = null){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $photo          = $this->M_album_photo->get_photo_by_album_id($id);
        $path_source    = './uploads/dokumentasi/';
        $path_thumbs    = './uploads/dokumentasi/thumbs/';

        foreach($photo as $row){
            if(file_exists($path_source . $row['photo_name']) && $row['photo_name']){
                @unlink($path_source . $row['photo_name']);
            }

            if(file_exists($path_thumbs . $row['photo_name']) && $row['photo_name']){
                @unlink($path_thumbs . $row['photo_name']);
            }

            $this->M_photo->delete_by_id($row['id']);

        }

        $this->M_album->delete_by_id($id);
        $this->M_album_photo->delete_by_album_id($album_id);

        echo json_encode(array('status' => true));
    }

    function detail($album_id = null, $album_name = null){

        if($album_id && $album_name){

            $edit = $this->M_album->get_album_by_id($album_id);

            if(!$edit){
                $this->session->set_flashdata('warning', 'Data tidak tersedia!');
                redirect('backoffice/dokumentasi');
            }

            $this->data['photo']    = $this->M_album_photo->get_photo_by_album_id($album_id);
            $this->data['album']    = $edit;
            $this->data['title']    = 'Detail Dokumentasi';
            $this->data['contents'] = $this->contents.'v_detail';

            $this->load->view($this->template, $this->data);
        } else {
            $this->session->set_flashdata('warning', 'Data tidak tersedia!');
            redirect('backoffice/dokumentasi');
        }
    }

    function upload_foto(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $config['upload_path']   = './uploads/dokumentasi/';
        $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
        $config['encrypt_name']  = TRUE;
        //$config['overwrite']   = TRUE;

        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){

            $file_ext   = $this->upload->data('file_ext');
            $xfilename  = $this->upload->data('file_name');
            $album_id   = $this->input->post('album_id');
            $token      = $this->input->post('token_foto');
            $photo_name = $xfilename;

            $data['photo_name'] = $photo_name;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata('id');
            $data['token']      = $token;

            $photo_id = $this->M_photo->add($data);

            $data2['photo_id'] = $photo_id;
            $data2['album_id'] = $album_id;

            $album_photo_id = $this->M_album_photo->add($data2);

            $source_path    = './uploads/dokumentasi/' . $xfilename;
            $path           = './uploads/dokumentasi/thumbs/';

            # RESIZE FOTO
            $this->resize_image($source_path, $path . $xfilename, '300','169');

            # UPDATE SAMPUL ALBUM BERDASARKAN WAKTU FOTO PERTAMA DI UPLOAD
            $data3['album_cover']   = $this->M_album_photo->get_album_cover_by_album_id($album_id);

            $this->M_album->update_album_by_id($album_id, $data3);
        }

        echo json_encode(array('status' => true));
    }

    function hapus_foto(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $token = $this->input->post('token');

        $edit = $this->M_photo->get_photo_by_token($token);

        if($edit){
            $path           = './uploads/dokumentasi/';
            $path_thumbs    = './uploads/dokumentasi/thumbs/';

            $photo_id       = $edit['id'];
            $album_id       = $edit['album_id'];

            if(file_exists($path . $edit['photo_name'])){
                @unlink($path . $edit['photo_name']);
            }
            
            if(file_exists($path_thumbs . $edit['photo_name'])){
                @unlink($path_thumbs . $edit['photo_name']);
            }

            $this->M_photo->delete_by_id($photo_id);
            $this->M_album_photo->get_album_photo_by_photo_id($photo_id);

            $data['album_cover'] = $this->M_album_photo->get_album_cover_by_album_id($album_id);

            $this->M_album->update_album_by_id($album_id, $data);
        }

        echo json_encode(array('status' => true));
    }

    function caption(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $id = $this->input->post('id');
        
        $this->data['edit'] = $this->M_photo->get_photo_by_id($id);
        
        $this->load->view($this->ajax_contents . 'v_caption', $this->data);
    }

    function simpan_caption(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $id = $this->input->post('id');
        
        $this->data['photo_caption'] = $this->input->post('photo_caption');

        $simpan = $this->M_photo->update_photo_by_id($id, $this->data);

        echo json_encode(array('status' => true));
    }

    function daftar_foto(){
        (!$this->input->is_ajax_request() ? show_404() : '');

        $album_id = $this->input->post('id');
        
        $this->data['album'] = $this->M_album_photo->get_photo_by_album_id($album_id);
        
        $this->load->view($this->ajax_contents . 'v_daftar_foto', $this->data);
    }

    function resize_image($sourcePath, $desPath, $width = '300', $height = '169'){
        $this->load->library('image_lib');
        $this->image_lib->clear();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $sourcePath;
        $config['new_image']        = $desPath;
        $config['quality']          = '100%';
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['width']            = $width;
        $config['height']           = $height;

        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize()){
            return true;
        } else{
            return false;
        }
    }
}

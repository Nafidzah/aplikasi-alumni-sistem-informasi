<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        ($this->session->userdata('id') ? '' : redirect('auth/login'));

        $this->template = 'templates/v_backoffice';
        $this->contents = 'pesan/';
    }

    function index(){
        $this->data['title']    = 'Message';
        $this->data['contents'] = $this->contents.'v_index';

        $this->load->view($this->template, $this->data);
    }

    function ajax_send_chat(){
        $chat_message       = $this->input->post('chat_message');
        $id_user_terima     = $this->input->post('id_user_terima');
        $id_user_kirim      = $this->session->userdata('id');
        $waktu_kirim        = date('Y-m-d H:i:s');

        $sql = "INSERT INTO tbl_chat
                    SET chat='".$chat_message."'
                        , id_user_kirim='".$id_user_kirim."'
                        , id_user_terima='".$id_user_terima."'
                        , waktu_kirim='".$waktu_kirim."'
                ";
        $result = $this->db->query($sql);
    }

    function ajax_notifikasi_chat(){
        $id_user_terima = $this->session->userdata('id');
        $sql = 'SELECT t1.id_chat
                FROM tbl_chat as t1
                WHERE t1.id_user_terima="'.$id_user_terima.'"
                    AND t1.waktu_baca="0000-00-00 00:00:00"
                ';
        $result = $this->db->query($sql);
        $total  = $result->num_rows();
        $response = array();

        if($total > 0){
            $response['status'] = TRUE;
            $response['total']  = $total;
        } else {
            $response['status'] = FALSE;
        }

        echo json_encode($response);
    }

    function ajax_latest_chat(){
        $id_user_kirim      = $this->session->userdata('id');
        $id_user_terima     = $this->input->post('id_user_terima');
        $terakhir_kirim     = $this->input->post('waktu_kirim');

        // SELECT CHAT
        $sql = "SELECT t1.*
                FROM tbl_chat as t1
                WHERE
                    ( 
                        (t1.id_user_kirim = '".$id_user_kirim."' AND t1.id_user_terima = '".$id_user_terima."')
                        OR
                        (t1.id_user_kirim = '".$id_user_terima."' AND t1.id_user_terima = '".$id_user_kirim."')
                    ) AND t1.waktu_kirim > '".$terakhir_kirim."'
                ORDER BY t1.waktu_kirim DESC
                ";
        $result_chat    = $this->db->query($sql);
        $total_chat     = $result_chat->num_rows();
        $data_chat      = array();
        $chat_data      = array();

        if($total_chat > 0){
            foreach($result_chat->result() as $row){
                $chat_data['id_chat']       = $row->id_chat;
                $chat_data['chat']          = $row->chat;
                $chat_data['id_user_kirim'] = $row->id_user_kirim;
                $chat_data['id_user_terima']= $row->id_user_terima;
                $chat_data['waktu_kirim']   = $row->waktu_kirim;
                $chat_data['waktu_baca']    = $row->waktu_baca;

                $data_chat[] = $chat_data;
            }
        }

        array_sort_by_column($data_chat, 'waktu_kirim');

        $data['id_user_kirim']  = $id_user_kirim;
        $data['id_user_terima'] = $id_user_terima;
        $data['terakhir_kirim'] = $terakhir_kirim;
        $data['data_chat']      = $data_chat;

        $this->load->view('backoffice/pesan/v_list_latest', $data);
    }

    function ajax_chat(){
        $id_user_kirim      = $this->session->userdata('id');
        $id_user_terima     = $this->input->post('id_user_terima');
        $offset             = ($this->input->post('offset') ? $this->input->post('offset') : 0);
        $more               = ($this->input->post('more') ? $this->input->post('more') : 0);

        $data['id_user_terima'] = $id_user_terima;
        $data['id_user_kirim'] = $id_user_kirim;
        
        // SELECT TOTAL
        $sql = "SELECT t1.*
                FROM tbl_chat as t1
                WHERE 
                    (t1.id_user_kirim = '".$id_user_kirim."' AND t1.id_user_terima = '".$id_user_terima."')
                    OR
                    (t1.id_user_kirim = '".$id_user_terima."' AND t1.id_user_terima = '".$id_user_kirim."')
                ";
        $result_total   = $this->db->query($sql);
        $data['total_all']      = $result_total->num_rows();


        // SELECT CHAT
        $sql = "SELECT t1.*
                FROM tbl_chat as t1
                WHERE 
                    (t1.id_user_kirim = '".$id_user_kirim."' AND t1.id_user_terima = '".$id_user_terima."')
                    OR
                    (t1.id_user_kirim = '".$id_user_terima."' AND t1.id_user_terima = '".$id_user_kirim."')
                ORDER BY t1.waktu_kirim DESC
                LIMIT ".$offset.", 25
                ";
        $result_chat    = $this->db->query($sql);
        $total_chat     = $result_chat->num_rows();
        $data_chat      = array();
        $chat_data      = array();

        if($total_chat > 0){
            foreach($result_chat->result() as $row){
                $chat_data['id_chat']       = $row->id_chat;
                $chat_data['chat']          = $row->chat;
                $chat_data['id_user_kirim'] = $row->id_user_kirim;
                $chat_data['id_user_terima']= $row->id_user_terima;
                $chat_data['waktu_kirim']   = $row->waktu_kirim;
                $chat_data['waktu_baca']    = $row->waktu_baca;

                $data_chat[] = $chat_data;
            }
        }

        array_sort_by_column($data_chat, 'waktu_kirim');

        $data['total_chat'] = $total_chat;
        $data['data_chat'] = $data_chat;

        if($more){
            $data['offset'] = $offset + $total_chat;
            $this->load->view('backoffice/pesan/v_list_more', $data);
        } else {
            $this->load->view('backoffice/pesan/v_list_chat', $data);
        }
    }

    function ajax_user(){
        $ses_id_user    = $this->session->userdata('id');
        $where_level    = '';
        $where_search   = ' AND t1.role_id != 3 ';
        $search         = $this->input->post('search');
        $active         = ($this->input->post('active') ? $this->input->post('active') : 0);

        if(trim($search) != ''){
            $where_search .= "  AND t1.fullname LIKE '%".$search."%' ";
        }


        $sql = "SELECT 
                    t1.id
                    , t1.fullname
                    , t1.foto
                    , t1.role_id
                FROM tbl_users as t1
                WHERE t1.id != '".$ses_id_user."'
                    ".$where_search."
                    ".$where_level."
                ";
        $result_user            = $this->db->query($sql);
        $data['active']         = $active;
        $data['ses_id_user']    = $ses_id_user;
        $data['result_user']    = $result_user;
        $data['total_user']     = $result_user->num_rows();

        $this->load->view('backoffice/pesan/v_list_contact', $data);
    }
}

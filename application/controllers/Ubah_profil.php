<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="alert alert-secondary">
                                            <strong><i class="fas fa-user"></i> PERSONAL INFORMATION</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="control-label col-md-3 col-sm-6">
                                        <strong>Email</strong>  <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="email" id="email" name="email" value="<?php echo $edit['email'];?>" class="form-control" placeholder="......" maxlength="50">
                                        <?php echo form_error('email', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fullname" class="control-label col-md-3 col-sm-6">
                                        <strong>Fullname</strong>  <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="fullname" name="fullname" value="<?php echo $edit['fullname'];?>" class="form-control" placeholder="......" maxlength="50">
                                        <?php echo form_error('fullname', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="jenis_kelamin" class="control-label col-md-3 col-sm-6">
                                        <strong>Gender</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="jenis_kelamin" value="L" <?php echo ($edit['jenis_kelamin'] == 'L' ? 'checked' : '');?>> Male
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="jenis_kelamin" value="P" <?php echo ($edit['jenis_kelamin'] == 'P' ? 'checked' : '');?>> Female
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('jenis_kelamin', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tempat_lahir" class="control-label col-md-3 col-sm-6">
                                        <strong>Place of Birth</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?php echo $edit['tempat_lahir'];?>" class="form-control" placeholder="......">
                                        <?php echo form_error('tempat_lahir', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tanggal_lahir" class="control-label col-md-3 col-sm-6">
                                        <strong>Date of Birth</strong>
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control tanggal" name="tanggal_lahir" id="tanggal_lahir" value="<?php echo ( ($edit['tanggal_lahir'] == null || $edit['tanggal_lahir'] == '0000-00-00') ? '' : dateSlash($edit['tanggal_lahir']) );?>" autocomplete="off" onkeydown="event.preventDefault()">
                                            <div class="input-group-prepend input-group-prepend-tanggal">
                                                <small class="input-group-text">
                                                    <i class="fas fa-calendar"></i>
                                                </small>
                                            </div>                                        
                                        </div>
                                        <?php echo form_error('tanggal_lahir', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div> 
                                <div class="form-group row">
                                    <label for="tahun_angkatan" class="control-label col-md-3 col-sm-6">
                                        <strong>Tahun Angkatan</strong>   <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="text" id="tahun_angkatan" name="tahun_angkatan" value="<?php echo $edit['tahun_angkatan'];?>" class="form-control angka" placeholder="Misalnya: 2019" maxlength="4" autocomplete="off">
                                        <?php echo form_error('tahun_angkatan', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nomor_hp" class="control-label col-md-3 col-sm-6">
                                        <strong>Phone Number</strong>  <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="text" id="nomor_hp" name="nomor_hp" value="<?php echo $edit['nomor_hp'];?>" class="form-control" placeholder="......" maxlength="15">
                                        <?php echo form_error('nomor_hp', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="control-label col-md-3 col-sm-6">
                                        <strong>Bio</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <textarea id="description" name="description" class="form-control" placeholder="......" rows="3"><?php echo $edit['description'];?></textarea>
                                        <?php echo form_error('description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nomor_hp" class="control-label col-md-3 col-sm-6">
                                        <strong>Experience</strong>
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <select name="experience_id_default" class="form-control" id="experience_id_default">
                                            <option value="">- Choose Experience -</option>
                                            <?php
                                                foreach($experience as $row){
                                                    $selected = ($edit['experience_id_default'] == $row['id'] ? 'selected' : '');
                                                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['experience_position'].' - '.$row['experience'].'</option>';
                                                }
                                            ?>
                                        </select>       
                                        <?php echo form_error('experience_id_default', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="foto" class="control-label col-md-3 col-sm-6">
                                        <strong>Photo</strong>
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="file" id="foto" name="foto" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" name="foto_old" value="<?php echo $edit['foto'];?>">
                                        <?php echo form_error('foto', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="alert alert-secondary">
                                            <strong><i class="fas fa-user"></i> OTHER INFORMATION</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="website" class="control-label col-md-3 col-sm-6">
                                        <strong>Website</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="website" name="website" value="<?php echo $edit['website'];?>" class="form-control" placeholder="......" maxlength="255">
                                        <?php echo form_error('website', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="linkedin" class="control-label col-md-3 col-sm-6">
                                        <strong>Linkedin</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="linkedin" name="linkedin" value="<?php echo $edit['linkedin'];?>" class="form-control" placeholder="......" maxlength="255">
                                        <?php echo form_error('linkedin', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="facebook" class="control-label col-md-3 col-sm-6">
                                        <strong>Facebook</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="facebook" name="facebook" value="<?php echo $edit['facebook'];?>" class="form-control" placeholder="......" maxlength="255">
                                        <?php echo form_error('facebook', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="twitter" class="control-label col-md-3 col-sm-6">
                                        <strong>Twitter</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="twitter" name="twitter" value="<?php echo $edit['twitter'];?>" class="form-control" placeholder="......" maxlength="255">
                                        <?php echo form_error('twitter', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="instagram" class="control-label col-md-3 col-sm-6">
                                        <strong>Instagram</strong>
                                    </label>
                                    <div class="col-md-7 col-sm-6">
                                        <input type="text" id="instagram" name="instagram" value="<?php echo $edit['instagram'];?>" class="form-control" placeholder="......" maxlength="255">
                                        <?php echo form_error('instagram', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="offset-md-3 col-md-7 offset-sm-6 offset-sm-6">
                                            <button type="submit" class="btn btn-dark">
                                                <i class="fas fa-save"></i> Save
                                            </button>
                                            <a href="<?php echo base_url('backoffice/profil');?>" class="btn btn-outline-secondary">
                                                 <i class="fas fa-reply"></i> Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

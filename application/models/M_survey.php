<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_survey extends CI_Model{
    function __construct(){
        parent::__construct();

        $this->load->library('session');

        $this->id               = $this->session->userdata('id');
        $this->tahun_angkatan   = $this->session->userdata('tahun_angkatan');
        $this->role_id          = $this->session->userdata('role_id');
    }

    function update_survey_by_id($id, $data){
        return $this->db->update('tbl_survey', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_survey', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_survey', array('id' => $id));
    }

    function check($data){
        return $this->db->get_where('tbl_survey', $data);
    }

    function get_all_survey_belum_diisi(){
        $where = "";
        if($this->role_id == 2){
            $where = " AND (t1.respondent LIKE '%".$this->tahun_angkatan."%' OR t1.respondent LIKE '%all%' )";
        }

        if($this->role_id == 3){
            $where = " AND (t1.respondent LIKE '%survey%' )";
        }

        $sql = "SELECT t1.*
                FROM tbl_survey as t1
                LEFT JOIN (
                    SELECT COUNT(*) as total_isi
                            ,xa.survey_id
                    FROM tbl_survey_result as xa
                    WHERE xa.created_by = '".$this->id."'
                    GROUP BY xa.survey_id
                ) as t2 ON t1.id = t2.survey_id
                WHERE t1.publish = 'Y'
                    AND (
                        CONCAT(t1.start_date, '', t1.start_time) BETWEEN '".date('Y-m-d H:i:s')."' AND '".date('Y-m-d H:i:s')."' OR 
                        '".date('Y-m-d H:i:s')."' BETWEEN CONCAT(t1.start_date, '', t1.start_time) AND CONCAT(t1.end_date, '', t1.end_time)
                    )
                    AND (t2.total_isi = 0 OR t2.total_isi IS NULL)
                    ".$where."
                ORDER BY t1.created_at ASC
                LIMIT 5
                ";
        $result = $this->db->query($sql)->result_array();

        if($this->role_id == 1){
            $result = array();
        }

        return $result;
    }

    function get_all_survey(){
        $where = "";
        if($this->role_id == 2){
            $where = " AND (t1.respondent LIKE '%".$this->tahun_angkatan."%' OR t1.respondent LIKE '%all%' )";
        }

        if($this->role_id == 3){
            $where = " AND (t1.respondent LIKE '%survey%' )";
        }

        $sql = "SELECT t1.*
                FROM tbl_survey as t1
                WHERE t1.publish = 'Y'
                    AND (
                        CONCAT(t1.start_date, '', t1.start_time) BETWEEN '".date('Y-m-d H:i:s')."' AND '".date('Y-m-d H:i:s')."' OR 
                        '".date('Y-m-d H:i:s')."' BETWEEN CONCAT(t1.start_date, '', t1.start_time) AND CONCAT(t1.end_date, '', t1.end_time)
                    )
                    ".$where."
                ORDER BY t1.end_date ASC
                ";
        return $this->db->query($sql)->result_array();

    }

    function get_survey_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_survey as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns} ,t1.start_time, t1.end_time FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();

        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_edit       = base_url('backoffice/dashboard/ubah-survei/' . $row['id'] . '/' . url_title(strtolower($row['survey_name'])));
            $url_delete     = base_url('backoffice/dashboard/hapus-survei/' . $row['id'] . '/' . url_title(strtolower($row['survey_name'])));

            $aksi = '
                    <a title="Ubah Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Hapus Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';

            if($row['publish'] == 'Y'){
                $publish = 'Yes';
            } else {
                $publish = 'No';
            }

            $option['data'][] = array(
                                    $no,
                                    $row['survey_name'],
                                    $row['start_date'] . '<br>' . $row['start_time'],
                                    $row['end_date'] . '<br>' . $row['end_time'],
                                    str_replace('all', 'All Alumni', $row['respondent']),
                                    $publish,
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }

    function get_datatables_survey_result($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns} ,t1.start_time, t1.end_time FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();

        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_edit       = base_url('backoffice/survei/detail/' . $row['id'] . '/' . url_title(strtolower($row['survey_name'])));
            $aksi = '
                    <a title="View Summary" class="btn btn-info btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-chart-bar"></i> View
                    </a>
                    ';

            if($row['publish'] == 'Y'){
                $publish = 'Yes';
            } else {
                $publish = 'No';
            }

            $status = '';
            if(strtotime(date('Y-m-d')) < strtotime($row['start_date'])){
                $status = '<div class="text-center">
                            <div class="progress">
                              <div class="progress-bar-animated progress-bar progress-bar-striped active bg-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                            </div>
                            <strong class="text-warning">Unopened</strong>
                        </div>';
            } else if(strtotime(date('Y-m-d')) >= strtotime($row['start_date']) && strtotime(date('Y-m-d')) <= strtotime($row['end_date']) ){
                $status = '<div class="text-center">
                            <div class="progress">
                              <div class="progress-bar-animated progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                            </div>
                            <strong class="text-primary">In the period</strong>
                        </div>';
            } else if(strtotime(date('Y-m-d')) > strtotime($row['end_date'])){
                $status = '<div class="text-center">
                            <div class="progress">
                              <div class="progress-bar-animated progress-bar progress-bar-striped active bg-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                            </div>
                            <strong class="text-danger">Closed</strong>
                        </div>';
            }

            $option['data'][] = array(
                                    $no,
                                    $row['survey_name'],
                                    $row['start_date'] . '<br>' . $row['start_time'],
                                    $row['end_date'] . '<br>' . $row['end_time'],
                                    str_replace('all', 'All Alumni', $row['respondent']),
                                    $publish,
                                    $status,
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
}
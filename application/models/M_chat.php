<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_chat extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_chat_by_id($id, $data){
        return $this->db->update('tbl_chat', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_chat', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_chat', array('id' => $id));
    }

    function delete_by_users_id($users_id){
        return $this->db->delete('tbl_chat', array('users_id' => $users_id));
    }

    function check($data){
        return $this->db->get_where('tbl_chat', $data);
    }

    function get_chat_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_chat as t1');

        $query = $this->db->get();
        return $query->row_array();
    }
}
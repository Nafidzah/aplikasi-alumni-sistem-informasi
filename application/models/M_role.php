<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_role extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_role_by_id($id, $data){
        return $this->db->update('tbl_role', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_role', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_role', array('id' => $id));
    }

    function get_role_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_role as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_role(){
        $this->db->select('t1.*');
        $this->db->from('tbl_role as t1');
        $this->db->order_by('t1.role','ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns}  FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $aksi = '<a title="Ubah Data" class="btn btn-warning btn-sm" href="'.base_url('backoffice/master/role/ubah/' . $row['id'] . '/' . url_title(strtolower($row['role']))).'">
                        <i class="fa fa-edit"></i>
                    </a>
                    ';

            $option['data'][] = array(
                                    $no,
                                    $row['role'],
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
}
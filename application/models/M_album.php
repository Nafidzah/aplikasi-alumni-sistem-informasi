<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_album extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_album_by_id($id, $data){
        return $this->db->update('tbl_album', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_album', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_album', array('id' => $id));
    }

    function check($data){
        return $this->db->get_where('tbl_album', $data);
    }

    function get_total_all_album_pagination(){
        $this->db->select('t1.*');
        $this->db->where('t1.publish', 'Y');
        $this->db->from('tbl_album as t1');

        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_album_pagination($limit, $start){
        $this->db->select('t1.*');
        $this->db->where('t1.publish', 'Y');
        $this->db->from('tbl_album as t1');
        $this->db->order_by('t1.created_at', 'DESC');
        $this->db->limit($limit, $start);

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_album_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_album as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns}  FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $path = './uploads/dokumentasi/thumbs/';
        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_photo      = base_url('backoffice/dokumentasi/foto/' . $row['id'] . '/' . url_title(strtolower($row['album_name'])));
            $url_edit       = base_url('backoffice/dokumentasi/ubah/' . $row['id'] . '/' . url_title(strtolower($row['album_name'])));
            $url_delete     = base_url('backoffice/dokumentasi/hapus/' . $row['id'] . '/' . url_title(strtolower($row['album_name'])));

            $aksi = '
                    <a title="Lihat Foto" class="btn btn-info btn-sm" href="'.$url_photo.'">
                        <i class="fas fa-photo-video"></i>
                    </a>
                    <a title="Ubah Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Hapus Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';

            if(file_exists($path . $row['album_cover']) && $row['album_cover']){
                $album_cover = '<img src="'.base_url('uploads/dokumentasi/thumbs/' . $row['album_cover']).'" style="width:122px;">';
            } else {
                $album_cover = '<img src="'.base_url('uploads/dokumentasi/thumbs_no_image.jpg').'">';
            }

            if($row['publish'] == 'Y'){
                $publish = 'Ya';
            } else {
                $publish = 'Tidak';
            }

            $option['data'][] = array(
                                    $no,
                                    $album_cover,
                                    $row['album_name'],
                                    $row['album_description'],
                                    $publish,
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
}
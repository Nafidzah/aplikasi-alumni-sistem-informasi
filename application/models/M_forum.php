<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_forum extends CI_Model{
    function __construct(){
        parent::__construct();

        $this->id = $this->session->userdata('id');
    }

    function update_forum_by_id($id, $data){
        return $this->db->update('tbl_forum', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_forum', $data);
        return $this->db->insert_id();
    }

    function delete_by_forum_id_parent($forum_id_parent){
        return $this->db->delete('tbl_forum', array('forum_id_parent' => $forum_id_parent));
    }

    function delete_by_forum_donation_id($donation_id){
        return $this->db->delete('tbl_forum', array('donation_id' => $donation_id));
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_forum', array('id' => $id));
    }

    function delete_by_users_id($users_id){
        return $this->db->delete('tbl_forum', array('users_id' => $users_id));
    }

    function check($data){
        return $this->db->get_where('tbl_forum', $data);
    }

    function get_forum_by_forum_id_parent($forum_id_parent){
        $sql = "SELECT t1.id
                        , t1.forum_id_parent
                        , t1.forum_description
                        , t1.forum_cover
                        , t1.donation_id
                        , t1.total_like
                        , t1.total_comment
                        , t1.created_at
                        , t1.created_by as created_by_id
                        , t2.fullname as created_by                            
                        , t2.foto
                FROM tbl_forum as t1
                INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                WHERE t1.forum_id_parent = '".$forum_id_parent."'
                ";
        return $this->db->query($sql)->result_array();
    }

    function get_forum_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_forum as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_more_post($limit, $offset){
        $sql = "SELECT t.*
                FROM (
                    SELECT t1.id
                            , t1.forum_id_parent
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN CONCAT('<strong>',t3.donation_title, '</strong><br><br>', t3.donation_description)
                                ELSE t1.forum_description
                            END as forum_description
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN t3.donation_cover
                                ELSE t1.forum_cover
                            END as forum_cover
                            , t1.donation_id
                            , t1.total_like
                            , t1.total_comment
                            , t1.created_at
                            , t1.created_by as created_by_id
                            , t2.fullname as created_by                            
                            , t2.foto
                            , t4.sudah_like
<<<<<<< HEAD
=======
                            , t3.publish
>>>>>>> remotes/origin/Agung
                    FROM tbl_forum as t1
                    INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                    LEFT JOIN tbl_donation as t3 ON t1.donation_id = t3.id
                    LEFT JOIN (
                        SELECT COUNT(x.id) as sudah_like
                            , x.forum_id
                        FROM tbl_forum_like as x
                        WHERE x.users_id = ".$this->id."
                        GROUP BY x.forum_id
                    ) as t4 ON t1.id = t4.forum_id
                    WHERE t1.forum_id_parent = '0'
                ) as t
<<<<<<< HEAD
                WHERE (t.forum_description IS NOT NULL OR t.forum_description = '')
=======
                WHERE ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish IS NULL )
                    OR 
                    ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish ='Y')
>>>>>>> remotes/origin/Agung
                ORDER BY t.created_at DESC
                LIMIT ".$offset.", ".$limit."
                ";

        return $this->db->query($sql)->result_array();
    }

    function get_all_latest_post($limit, $forum_id){
        $sql = "SELECT t.*
                FROM (
                    SELECT t1.id
                            , t1.forum_id_parent
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN CONCAT('<strong>',t3.donation_title, '</strong><br><br>', t3.donation_description)
                                ELSE t1.forum_description
                            END as forum_description
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN t3.donation_cover
                                ELSE t1.forum_cover
                            END as forum_cover
                            , t1.donation_id
                            , t1.total_like
                            , t1.total_comment
                            , t1.created_at
                            , t1.created_by as created_by_id
                            , t2.fullname as created_by                            
                            , t2.foto
                            , t4.sudah_like
<<<<<<< HEAD
=======
                            , t3.publish
>>>>>>> remotes/origin/Agung
                    FROM tbl_forum as t1
                    INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                    LEFT JOIN tbl_donation as t3 ON t1.donation_id = t3.id
                    LEFT JOIN (
                        SELECT COUNT(x.id) as sudah_like
                            , x.forum_id
                        FROM tbl_forum_like as x
                        WHERE x.users_id = ".$this->id."
                        GROUP BY x.forum_id
                    ) as t4 ON t1.id = t4.forum_id
                    WHERE t1.forum_id_parent = '0'
                        AND t1.id > ".$forum_id."
                ) as t
<<<<<<< HEAD
                WHERE (t.forum_description IS NOT NULL OR t.forum_description = '')
=======
                WHERE ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish IS NULL )
                    OR 
                    ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish ='Y')
>>>>>>> remotes/origin/Agung
                ORDER BY t.created_at DESC
                LIMIT  ".$limit."
                ";

        return $this->db->query($sql)->result_array();
    }

    function get_total_forum_by_forum_id($forum_id){
        $this->db->select('COUNT(id) as total');
        $this->db->where('forum_id_parent', $forum_id);
        $this->db->from('tbl_forum');

        $data = $this->db->get()->row_array();
        $total= ($data && $data['total'] ? $data['total'] : 0);
        return $total;
    }

    function get_all_more_post_profil($limit, $offset, $users_id){
        $sql = "SELECT t.*
                FROM (
                    SELECT t1.id
                            , t1.forum_id_parent
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN CONCAT('<strong>',t3.donation_title, '</strong><br><br>', t3.donation_description)
                                ELSE t1.forum_description
                            END as forum_description
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN t3.donation_cover
                                ELSE t1.forum_cover
                            END as forum_cover
                            , t1.donation_id
                            , t1.total_like
                            , t1.total_comment
                            , t1.created_at
                            , t1.created_by as created_by_id
                            , t2.fullname as created_by                            
                            , t2.foto
                            , t4.sudah_like
<<<<<<< HEAD
=======
                            , t3.publish
>>>>>>> remotes/origin/Agung
                    FROM tbl_forum as t1
                    INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                    LEFT JOIN tbl_donation as t3 ON t1.donation_id = t3.id
                    LEFT JOIN (
                        SELECT COUNT(x.id) as sudah_like
                            , x.forum_id
                        FROM tbl_forum_like as x
                        WHERE x.users_id = ".$this->id."
                        GROUP BY x.forum_id
                    ) as t4 ON t1.id = t4.forum_id
                    WHERE t1.forum_id_parent = '0'
                    AND t1.created_by = '".$users_id."'
                ) as t
<<<<<<< HEAD
                WHERE (t.forum_description IS NOT NULL OR t.forum_description = '')
=======
                WHERE ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish IS NULL )
                    OR 
                    ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish ='Y')
>>>>>>> remotes/origin/Agung
                ORDER BY t.created_at DESC
                LIMIT ".$offset.", ".$limit."
                ";

        return $this->db->query($sql)->result_array();
    }

    function get_all_latest_post_profil($limit, $forum_id, $users_id){
        $sql = "SELECT t.*
                FROM (
                    SELECT t1.id
                            , t1.forum_id_parent
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN CONCAT('<strong>',t3.donation_title, '</strong><br><br>', t3.donation_description)
                                ELSE t1.forum_description
                            END as forum_description
                            , CASE WHEN (t1.donation_id != 0 OR t1.donation_id IS NOT NULL) AND t3.donation_title IS NOT NULL
                                    THEN t3.donation_cover
                                ELSE t1.forum_cover
                            END as forum_cover
                            , t1.donation_id
                            , t1.total_like
                            , t1.total_comment
                            , t1.created_at
                            , t1.created_by as created_by_id
                            , t2.fullname as created_by                            
                            , t2.foto
                            , t4.sudah_like
<<<<<<< HEAD
=======
                            , t3.publish
>>>>>>> remotes/origin/Agung
                    FROM tbl_forum as t1
                    INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                    LEFT JOIN tbl_donation as t3 ON t1.donation_id = t3.id
                    LEFT JOIN (
                        SELECT COUNT(x.id) as sudah_like
                            , x.forum_id
                        FROM tbl_forum_like as x
                        WHERE x.users_id = ".$this->id."
                        GROUP BY x.forum_id
                    ) as t4 ON t1.id = t4.forum_id
                    WHERE t1.forum_id_parent = '0'
                        AND t1.id > ".$forum_id."
                        AND t1.created_by = '".$users_id."'
                ) as t
<<<<<<< HEAD
                WHERE (t.forum_description IS NOT NULL OR t.forum_description = '')
=======
                WHERE ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish IS NULL )
                    OR 
                    ((t.forum_description IS NOT NULL OR t.forum_description = '') AND t.publish ='Y')
>>>>>>> remotes/origin/Agung
                ORDER BY t.created_at DESC
                LIMIT  ".$limit."
                ";

        return $this->db->query($sql)->result_array();
    }
}
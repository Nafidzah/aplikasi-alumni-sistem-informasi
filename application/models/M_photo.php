<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_photo extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_photo_by_id($id, $data){
        return $this->db->update('tbl_photo', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_photo', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_photo', array('id' => $id));
    }

    function check($data){
        return $this->db->get_where('tbl_photo', $data);
    }

    function get_photo_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_photo as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_photo_by_token($token){
        $this->db->select('t1.*');
        $this->db->where('t1.token', $token);
        $this->db->from('tbl_photo as t1');

        $query = $this->db->get();
        return $query->row_array();
    }
}
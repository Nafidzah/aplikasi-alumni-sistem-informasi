<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_achievement extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_achievement_by_id($id, $data){
        return $this->db->update('tbl_achievement', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_achievement', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_achievement', array('id' => $id));
    }

<<<<<<< HEAD
=======
    function delete_by_users_id($users_id){
        return $this->db->delete('tbl_achievement', array('users_id' => $users_id));
    }

>>>>>>> remotes/origin/Agung
    function check($data){
        return $this->db->get_where('tbl_achievement', $data);
    }

    function get_achievement_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_achievement as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_achievement_by_users_id($users_id){
        $this->db->select('t1.*');
        $this->db->where('users_id', $users_id);
        $this->db->from('tbl_achievement as t1');
        $this->db->order_by('t1.id','ASC');

        $query = $this->db->get();
        return $query->result_array();
    }
}
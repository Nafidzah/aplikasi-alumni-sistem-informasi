<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengaturan extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function update_pengaturan_by_id($id, $data)
    {
        return $this->db->update('tbl_pengaturan', $data, array('id' => $id));
    }

    function add($data)
    {
        $this->db->insert('tbl_pengaturan', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id)
    {
        return $this->db->delete('tbl_pengaturan', array('id' => $id));
    }

    function get_pengaturan_by_id($id)
    {
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_pengaturan as t1');

        $query = $this->db->get();
        $data  = "";

        if($query->num_rows() > 0){
            $row = $query->row();

            $data = $row->pengaturan;
        }
        
        return $data;
    }
}
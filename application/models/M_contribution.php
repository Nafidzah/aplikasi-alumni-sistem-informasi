<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_contribution extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_contribution_by_id($id, $data){
        return $this->db->update('tbl_contribution', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_contribution', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_contribution', array('id' => $id));
    }

<<<<<<< HEAD
=======
    function delete_by_users_id($users_id){
        return $this->db->delete('tbl_contribution', array('users_id' => $users_id));
    }

>>>>>>> remotes/origin/Agung
    function check($data){
        return $this->db->get_where('tbl_contribution', $data);
    }

    function get_contribution_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_contribution as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_contribution_by_users_id($users_id){
        $this->db->select('t1.*');
        $this->db->where('users_id', $users_id);
        $this->db->from('tbl_contribution as t1');
        $this->db->order_by('t1.id','ASC');

        $query = $this->db->get();
        return $query->result_array();
    }
}
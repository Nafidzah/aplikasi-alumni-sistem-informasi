<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_users extends CI_Model{

    function __construct(){
        parent::__construct();
<<<<<<< HEAD
=======

        $this->fullname = $this->session->userdata('fullname');
        $this->id       = $this->session->userdata('id');
        $this->role_id  = $this->session->userdata('role_id');
>>>>>>> Agung
    }

    function get_login($username, $password){
        $this->db->select('t1.*');
        $this->db->where('t1.username', $username);
        $this->db->where('t1.password', $password);
        $this->db->from('tbl_users as t1');

        $query = $this->db->get();
        return $query->row_array();
	}

    function get_users_by_id($id){
        $this->db->select('t1.*, t2.role');
        $this->db->join('tbl_role as t2', 't1.role_id = t2.id');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_users as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function update_users_by_id($id, $data){
        return $this->db->update('tbl_users', $data, array('id' => $id));
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_users', array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_users', $data);
        return $this->db->insert_id();
    }

<<<<<<< HEAD
=======
    function check($data){
        return $this->db->get_where('tbl_users', $data);
    }

>>>>>>> Agung
    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns}  FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_edit   = base_url('backoffice/master/pengguna/ubah/' . $row['id'] . '/' . url_title(strtolower($row['role'])));
            $url_delete = base_url('backoffice/master/pengguna/hapus/' . $row['id'] . '/' . url_title(strtolower($row['role'])));

            $aksi = '<a title="Ubah Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Hapus Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';

            $option['data'][] = array(
                                    $no,
                                    $row['fullname'],
                                    $row['username'],
                                    $row['role'],
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
<<<<<<< HEAD
=======

    function get_datatables_alumni($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns} ,t3.experience_position  FROM {$dt['table']} {$join}";
        $role_id = 2;

        $where_filter = '';
        if($role_id){
            $where_filter .= " AND  t1.role_id = '".$role_id."'";
        }

        // total all
        $sql2 = $sql . " WHERE 1=1 " . $where_filter;
        $rowCountAll = $this->db->query($sql2)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '1=1' . $where_filter;

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {

                if($i == 0){
                    $where .= ' AND ( ';
                } else {
                    $where .= ' ( ';
                }
                

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"' . $where_filter;

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                if($i == 0){
                    $where .= ' AND ( ';
                } else {
                    $where .= ' ( ';
                }

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ' . $where_filter;
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $no = $start + 1;
        foreach ($list->result_array() as $row) {

            if($this->id == $row['id']){
                $url_view = base_url('backoffice/profil');
            } else {
                $url_view   = base_url('backoffice/informasi/detail/' . $row['id'] . '/' . url_title(strtolower($row['fullname'])));    
            }

            $url_chat = base_url('backoffice/pesan/?chat_to=' . $row['id'] . '&chat_from=' . $this->fullname);
            
            $url_edit   = base_url('backoffice/informasi/ubah/' . $row['id'] . '/' . url_title(strtolower($row['fullname'])));
            $url_delete = base_url('backoffice/informasi/hapus/' . $row['id'] . '/' . url_title(strtolower($row['fullname'])));

            $aksi = '<a title="View Profile" class="btn btn-info btn-sm" href="'.$url_view.'">
                        <i class="fas fa-search"></i>
                    </a>
                    <a title="Edit Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Delete Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';

            $experience = '';
            if($row['experience_position']){
                $experience .= $row['experience_position'];
            }
            if($row['experience']){
                $experience .= ($experience ? ' - ' : '');
                $experience .= $row['experience'];
            }

            $fullname = '<a href="javascript:;"  role="button" data-toggle="popover" data-placement="left" data-trigger="focus" title="'.$row['fullname'].'" data-content="<a  class=\'ml-2 mr-2 text-decoration-none text-danger\' href=\''.$url_view.'\'><i class=\'fa fa-user-circle\'></i> View Profile</a>&nbsp;<a class=\'ml-2 mr-2 text-decoration-none text-success\' href=\''.$url_chat.'\'><i class=\'fa fa-comment\'></i> Message</a>">
                            <strong>'.$row['fullname'].'</strong>
                        </a>';

            if($this->role_id == 1){
                $array_tr = array(
                                    $no,
                                    $fullname,
                                    $row['tahun_angkatan'],
                                    $experience,
                                    $row['nomor_hp'],
                                    $row['email'],
                                    $aksi
                                );
            } else {
                $array_tr = array(
                                    $no,
                                    $fullname,
                                    $row['tahun_angkatan'],
                                    $experience,
                                    $row['nomor_hp'],
                                    $row['email']
                                );
            }

            $option['data'][] = $array_tr;
            
            $no++;
        }
        echo json_encode($option);
    }
>>>>>>> Agung
}



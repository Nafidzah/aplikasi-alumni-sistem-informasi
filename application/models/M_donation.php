<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_donation extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_donation_by_id($id, $data){
        return $this->db->update('tbl_donation', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_donation', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_donation', array('id' => $id));
    }

    function get_donation_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_donation as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_donation(){
        $this->db->select('t1.*');
        $this->db->from('tbl_donation as t1');
        $this->db->order_by('t1.donation_title','ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns}  FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $path = './uploads/donasi/thumbs/';

        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_edit       = base_url('backoffice/dashboard/ubah-donasi/' . $row['id'] . '/' . url_title(strtolower($row['donation_title'])));
            $url_delete     = base_url('backoffice/dashboard/hapus-donasi/' . $row['id'] . '/' . url_title(strtolower($row['donation_title'])));

            $aksi = '
                    <a title="Ubah Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Hapus Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';

            if(file_exists($path . $row['donation_cover']) && $row['donation_cover']){
                $donation_cover = '<img src="'.base_url('uploads/donasi/thumbs/' . $row['donation_cover']).'" style="width:150px;">';
            } else {
                $donation_cover = '<img src="'.base_url('uploads/donasi/thumbs_no_image.jpg').'">';
            }

            if($row['publish'] == 'Y'){
                $publish = 'Yes';
            } else {
                $publish = 'No';
            }

            $option['data'][] = array(
                                    $no,
                                    $donation_cover,
                                    $row['donation_title'],
                                    character_limiter($row['donation_description'], 180),
                                    $publish,
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
}
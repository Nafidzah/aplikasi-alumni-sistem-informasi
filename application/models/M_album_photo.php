<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_album_photo extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_album_photo_by_id($id, $data){
        return $this->db->update('tbl_album_photo', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_album_photo', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_album_photo', array('id' => $id));
    }

    function delete_by_album_id($album_id){
        return $this->db->delete('tbl_album_photo', array('album_id' => $album_id));
    }

    function check($data){
        return $this->db->get_where('tbl_album_photo', $data);
    }

    function get_album_photo_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_album_photo as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_album_photo_by_photo_id($photo_id){
        $this->db->select('t1.*');
        $this->db->where('t1.photo_id', $photo_id);
        $this->db->from('tbl_album_photo as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_album_cover_by_album_id($album_id){
        $this->db->select('t2.photo_name');
        $this->db->join('tbl_photo as t2', 't1.photo_id = t2.id');
        $this->db->where('t1.album_id', $album_id);
        $this->db->from('tbl_album_photo as t1');
        $this->db->order_by('t1.photo_id', 'ASC');
        $this->db->limit(1);

        $query  = $this->db->get();
        $data   = null;

        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data = $row['photo_name'];
        }

        return $data;
    }

    function get_photo_by_album_id($album_id){
        $this->db->select('t1.album_id, t1.photo_id, t2.photo_name, t2.photo_caption, t2.token');
        $this->db->join('tbl_photo as t2', 't1.photo_id = t2.id');
        $this->db->where('t1.album_id', $album_id);
        $this->db->from('tbl_album_photo as t1');
        $this->db->order_by('t1.photo_id', 'ASC');

        $query  = $this->db->get();
        return $query->result_array();
    }
}
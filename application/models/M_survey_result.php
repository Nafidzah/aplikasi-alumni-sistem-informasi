<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_survey_result extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_survey_result_by_id($id, $data){
        return $this->db->update('tbl_survey_result', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_survey_result', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_survey_result', array('id' => $id));
    }

    function delete_by_survey_id($survey_id){
        return $this->db->delete('tbl_survey_result', array('survey_id' => $survey_id));
    }

    function check($data){
        return $this->db->get_where('tbl_survey_result', $data);
    }

    function get_survey_result_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_survey_result as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_respondent_by_survei_id($survey_id){
        $sql = "SELECT t1.survey_id
                        , t1.created_by
                        , t1.created_at
                        , t2.fullname
                FROM tbl_survey_result as t1
                INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                WHERE t1.survey_id = '".$survey_id."'
                GROUP BY t1.survey_id, t1.created_by
                ORDER BY t1.created_at ASC
                ";
        return $this->db->query($sql)->result_array();
    }

    function get_group_by_created_at_by_survei_id($survey_id){
        $sql = "SELECT DATE(t1.created_at) as tanggal
                        , COUNT(DISTINCT t1.survey_id, t1.created_by) as total
                FROM tbl_survey_result as t1
                INNER JOIN tbl_users as t2 ON t1.created_by = t2.id
                WHERE t1.survey_id = '".$survey_id."'
                GROUP BY t1.survey_id, DATE(t1.created_at)
                ORDER BY DATE(t1.created_at) ASC
                ";
        return $this->db->query($sql)->result_array();
    }

    function get_answer_result_by($survey_id, $created_by, $survey_question_id){
        $this->db->select('t1.answer_result');
        $this->db->where('t1.survey_id', $survey_id);
        $this->db->where('t1.created_by', $created_by);
        $this->db->where('t1.survey_question_id', $survey_question_id);
        $this->db->from('tbl_survey_result as t1');

        $row  = $this->db->get()->row_array();
        $data = ($row && $row['answer_result'] ? $row['answer_result']  : '');

        return $data;
    }
}
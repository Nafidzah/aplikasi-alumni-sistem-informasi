<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_donor extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_donor_by_id($id, $data){
        return $this->db->update('tbl_donor', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_donor', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_donor', array('id' => $id));
    }

    function check($data){
        return $this->db->get_where('tbl_donor', $data);
    }

    function get_donor_by_id($id){
        $this->db->select('t1.*, t2.fullname as created_by, t3.donation_title, t4.donation_category');
        $this->db->join('tbl_users as t2', 't1.created_by = t2.id', 'LEFT');
        $this->db->join('tbl_donation as t3', 't1.donation_id = t3.id');
        $this->db->join('tbl_donation_category as t4', 't1.donation_category_id = t4.id');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_donor as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_datatables($dt){
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns}  FROM {$dt['table']} {$join}";

        // total all
        $rowCountAll = $this->db->query($sql)->num_rows();

        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        // search
        $search = $dt['search']['value'];

        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';

        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= ' ( ';

                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';

                $where .= ' ) ';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {                       

                $where .= ' ( ';

                $where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                
                $where .= ' ) ';
            }
        }

        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where ." ";
            $rowCount = $this->db->query($sql)->num_rows();
        } else {
            $rowCount = $this->db->query($sql)->num_rows();
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCountAll;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        
        $path = './uploads/donasi/thumbs/';

        $no = $start + 1;
        foreach ($list->result_array() as $row) {
            $url_detail     = base_url('backoffice/donasi/detail/' . $row['id'] . '/' . url_title(strtolower($row['donation_title'])) . '/' . url_title(strtolower($row['donor_name'])));
            $url_edit       = base_url('backoffice/donasi/ubah/' . $row['id'] . '/' . url_title(strtolower($row['donation_title'])) . '/' . url_title(strtolower($row['donor_name'])));
            $url_delete     = base_url('backoffice/donasi/hapus/' . $row['id'] . '/' . url_title(strtolower($row['donation_title'])) . '/' . url_title(strtolower($row['donor_name'])));

            $aksi = '
                    <a title="Evidence Data" class="btn btn-info btn-sm" href="'.$url_detail.'">
                        <i class="fas fa-photo-video"></i>
                    </a>
                    <a title="Edit Data" class="btn btn-warning btn-sm" href="'.$url_edit.'">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a title="Delete Data" class="btn btn-danger btn-sm btn-hapus" href="javascript:;" data-url="'.$url_delete.'">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    ';
            $donor_name = '<a href="'.$url_detail.'" title="Detail"><strong>'.$row['donor_name'].'</strong></a>';

            $option['data'][] = array(
                                    $no,
                                    $row['donation_title'],
                                    $donor_name,
                                    $row['donation_category'],
                                    character_limiter($row['donor_description'], 180),
                                    $aksi
                                );
            $no++;
        }
        echo json_encode($option);
    }
}
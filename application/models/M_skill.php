<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_skill extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_skill_by_id($id, $data){
        return $this->db->update('tbl_skill', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_skill', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_skill', array('id' => $id));
    }

<<<<<<< HEAD
=======
    function delete_by_users_id($users_id){
        return $this->db->delete('tbl_skill', array('users_id' => $iusers_idd));
    }

>>>>>>> remotes/origin/Agung
    function check($data){
        return $this->db->get_where('tbl_skill', $data);
    }

    function get_skill_by_id($id){
        $this->db->select('t1.*');
        $this->db->where('t1.id', $id);
        $this->db->from('tbl_skill as t1');

        $query = $this->db->get();
        return $query->row_array();
    }

    function get_all_skill_by_users_id($users_id){
        $this->db->select('t1.*');
        $this->db->where('users_id', $users_id);
        $this->db->from('tbl_skill as t1');
        $this->db->order_by('t1.id','ASC');

        $query = $this->db->get();
        return $query->result_array();
    }
}
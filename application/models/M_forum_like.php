<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_forum_like extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function update_forum_like_by_id($id, $data){
        return $this->db->update('tbl_forum_like', $data, array('id' => $id));
    }

    function add($data){
        $this->db->insert('tbl_forum_like', $data);
        return $this->db->insert_id();
    }

    function delete_by_id($id){
        return $this->db->delete('tbl_forum_like', array('id' => $id));
    }

    function delete_by_forum_id_users_id($forum_id, $users_id){
        return $this->db->delete('tbl_forum_like', array('forum_id' => $forum_id, 'users_id' => $users_id));
    }

    function check($data){
        return $this->db->get_where('tbl_forum_like', $data);
    }

    function get_total_like_by_forum_id($forum_id){
        $this->db->select('COUNT(id) as total');
        $this->db->where('forum_id', $forum_id);
        $this->db->from('tbl_forum_like');

        $data = $this->db->get()->row_array();
        $total= ($data && $data['total'] ? $data['total'] : 0);
        return $total;
    }
}
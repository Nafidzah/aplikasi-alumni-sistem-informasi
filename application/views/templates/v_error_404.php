<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>404 Page Not Found</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/fonts.min.css');?>" rel="stylesheet" />
        
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url('assets/css/sb-admin-2.min.css');?>" rel="stylesheet" />             
        <link href="<?php echo base_url('assets/css/custom.min.css');?>" rel="stylesheet" />
    </head>

    <body class="login">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <div class="mt-5"></div>
                        </div>
                    </div>                    
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="py-5 px-3 h-100 d-flex justify-content-center align-items-center text-center text-secondary font-weight-bold">
                                        <div class="text-center">
                                            <div class="error mx-auto" data-text="404" style="width: 100%;">404</div>
                                            <p class="lead text-gray-800 mb-5">Page Not Found</p>
                                            <a href="<?php echo base_url();?>">&larr; Back to Home</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?php echo base_url('assets/js/sb-admin-2.min.js');?>"></script>
    </body>
</html>

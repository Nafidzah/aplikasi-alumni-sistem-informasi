<?php
$ses_level_id = $this->session->userdata('ses_level_id');
?>
<!DOCTYPE html>
<html lang="en">
<<<<<<< HEAD

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo (isset($title) ? $title . ' - Alumni Sistem Informasi' : 'Alumni Sistem Informasi'); ?></title>

    <link href="<?php echo base_url('assets/css/fonts.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendor/sweetalert/sweetalert.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendor/datatables/css/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/vendor/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.min.css'); ?>" rel="stylesheet" />

    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/select2/js/select2.min.js'); ?>"></script>
</head>

<body id="page-top">
    <div id="wrapper">

        <?php $this->load->view('partials/v_back_sidebar'); ?>

        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php $this->load->view('partials/v_back_topbar'); ?>

                <?php $this->load->view('backoffice/' . $contents); ?>

            </div><!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Alumni Sistem Informasi 2020</span>
                    </div>
                </div>
            </footer><!-- End of Footer -->

        </div><!-- End of Content Wrapper -->
    </div><!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/datatables/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/datatables/js/dataTables.bootstrap4.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/sb-admin-2.min.js'); ?>"></script>

    <script>
        $(document).ready(function() {
            $(document).on("click", ".btn-logout", function() {
                swal({
                        title: "Konfirmasi Keluar Sistem",
                        text: "Apakah anda yakin ingin keluar dari sistem ini?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-danger',
                        confirmButtonText: 'Ya',
                        cancelButtonText: "Batal",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.href = '<?php echo base_url('auth/login/logout'); ?>';
                        } else {
                            swal("Batal", "Keluar sistem data dibatalkan", "error");
                        }
                    });
=======
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo (isset($title) ? $title .' - Alumni Sistem Informasi' : 'Alumni Sistem Informasi');?></title>

        <link href="<?php echo base_url('assets/css/fonts.min.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/vendor/sweetalert/sweetalert.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/vendor/datatables/css/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/vendor/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/vendor/dropzone/dropzone.min.css');?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/vendor/light-gallery/css/lightgallery.min.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/custom.min.css');?>" rel="stylesheet" />

        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>        
        <script src="<?php echo base_url('assets/vendor/select2/js/select2.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/dropzone/dropzone.min.js');?>"></script>
    </head>
    <body id="page-top">
        <div id="wrapper">

            <?php $this->load->view('partials/v_back_sidebar');?>

            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">
                    <?php $this->load->view('partials/v_back_topbar');?>

                    <?php $this->load->view('backoffice/'.$contents);?>
                    
                </div><!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Alumni Sistem Informasi 2020</span>
                        </div>
                    </div>
                </footer><!-- End of Footer -->

            </div><!-- End of Content Wrapper -->
        </div><!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
        
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/datatables/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/datatables/js/dataTables.bootstrap4.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/light-gallery/js/lightgallery-all.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/sb-admin-2.min.js'); ?>"></script>

        <script>
            $(document).ready(function() {
                $('.tanggal').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy',
                });

                $(document).on("click",".input-group-addon-tanggal",function() {
                    $(this).prev().focus();
                });

                $(document).on("keyup", ".angka", function() {
                    this.value = this.value.replace(/\D/g,'');
                });

                $(document).on("click",".btn-logout",function() {
                    swal({
                            title: "Konfirmasi Keluar Sistem",
                            text: "Apakah anda yakin ingin keluar dari sistem ini?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: 'btn-danger',
                            confirmButtonText: 'Ya',
                            cancelButtonText: "Batal",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm){
                                window.location.href='<?php echo base_url('auth/login/logout');?>';
                            } else {
                                swal("Canceled!", "Logout canceled!", "error");
                            }
                        });
                });

                <?php if($this->session->flashdata('success')):?>
                    swal('Success!', '<?php echo $this->session->flashdata('success');?>', 'success');
                <?php endif; ?>

                <?php if($this->session->flashdata('warning')):?>
                    swal('Sorry!', '<?php echo $this->session->flashdata('warning');?>', 'warning');
                <?php endif; ?>

                <?php if($this->session->flashdata('error')):?>
                    swal('Error!', '<?php echo $this->session->flashdata('error');?>', 'error');
                <?php endif; ?>
                
>>>>>>> Sabila
            });

            <?php if ($this->session->flashdata('success')) : ?>
                swal('Sukses!', '<?php echo $this->session->flashdata('success'); ?>', 'success');
            <?php endif; ?>

            <?php if ($this->session->flashdata('warning')) : ?>
                swal('Mohon maaf!', '<?php echo $this->session->flashdata('warning'); ?>', 'warning');
            <?php endif; ?>

            <?php if ($this->session->flashdata('error')) : ?>
                swal('Error!', '<?php echo $this->session->flashdata('error'); ?>', 'error');
            <?php endif; ?>

        });
    </script>
</body>

</html>
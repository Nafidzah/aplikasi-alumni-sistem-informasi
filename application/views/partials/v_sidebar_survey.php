
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="<?php echo base_url('assets/img/HMSI.png');?>" alt="" style="width: 56px;">
        </div>
        <div class="sidebar-brand-text ml-2">Information System Alumni</div>
    </a>
    <br>
    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'survei' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/survei');?>">
            <i class="fas fa-fw fa-file-signature"></i>
            <span>Survey</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link btn-logout" href="javascript:;">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span>
        </a>
    </li>
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>

<?php
    if($this->session->userdata('role_id') == 1){
        $this->load->view('partials/v_sidebar_admin');
    } else if($this->session->userdata('role_id') == 2) {
        $this->load->view('partials/v_sidebar_alumni');
    } else {
        $this->load->view('partials/v_sidebar_survey');
    }

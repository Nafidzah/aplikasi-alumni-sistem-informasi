
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="<?php echo base_url('assets/img/HMSI.png');?>" alt="" style="width: 56px;">
        </div>
        <div class="sidebar-brand-text ml-2">Information System Alumni</div>
    </a>
    <br>
    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'forum' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/forum');?>">
            <i class="fas fa-fw fa-home"></i>
            <span>Home</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'pesan' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/pesan');?>">
            <i class="fas fa-fw fa-envelope"></i>
            <span>Message</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'informasi' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/informasi');?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Information System Alumni</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'profil' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/profil');?>">
            <i class="fas fa-fw fa-user-circle"></i>
            <span>Profile</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'dokumentasi' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/dokumentasi');?>">
            <i class="fas fa-fw fa-photo-video"></i>
            <span>Documentation</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'survei' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/survei');?>">
            <i class="fas fa-fw fa-file-signature"></i>
            <span>Survey</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(3) == 'donasi-alumni' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/forum/donasi-alumni');?>">
            <i class="fas fa-fw fa-hand-holding-usd"></i>
            <span>Donation For Alumni</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link btn-logout" href="javascript:;">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span>
        </a>
    </li>
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>

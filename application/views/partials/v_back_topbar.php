<?php
$ses_role_id    = $this->session->userdata('role_id');
$ses_fullname   = $this->session->userdata('fullname');
$ses_foto       = $this->session->userdata('foto');

// SET AVATAR FOTO
$path = './uploads/profil/';
if(file_exists($path . $ses_foto) && $ses_foto){
    $url_gambar = base_url('uploads/profil/' . $ses_foto);
} else {
    $url_gambar = base_url('assets/img/default.jpg');
}

?>

<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <h5 class="mr-auto ml-md-3 my-2 my-md-0 mw-100">
        <?php echo (isset($title) ? $title : '');?>
    </h5>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="javascript:;" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $ses_fullname;?></span>
                <img class="img-profile rounded-circle" src="<?php echo $url_gambar;?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo base_url('backoffice/profil');?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    View Profile
                </a>
                <a class="dropdown-item" href="<?php echo base_url('backoffice/profil/ubah-password');?>">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Change Password
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item btn-logout" href="javascript:;">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
<!-- End of Topbar -->


<?php
$ses_role_id = $this->session->userdata('role_id');
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="<?php echo base_url('assets/img/HMSI.png');?>" alt="" style="width: 56px;">
        </div>
        <div class="sidebar-brand-text ml-2">Information System Alumni</div>
    </a>
    <br>
    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'dashboard' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/dashboard');?>">
            <i class="fas fa-fw fa-home"></i>
            <span>Home</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'forum' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/forum');?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Forum</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'pesan' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/pesan');?>">
            <i class="fas fa-fw fa-envelope"></i>
            <span>Message</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'informasi' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/informasi');?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Information System Alumni</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'profil' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/profil');?>">
            <i class="fas fa-fw fa-user-circle"></i>
            <span>Profile</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'donasi' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/donasi');?>">
            <i class="fas fa-fw fa-hand-holding-usd"></i>
            <span>Donation Results</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'dokumentasi' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/dokumentasi');?>">
            <i class="fas fa-fw fa-photo-video"></i>
            <span>Documentation</span>
        </a>
    </li>
    <li class="nav-item <?php echo ($this->uri->segment(2) == 'survei' ? 'active' : '');?>">
        <a class="nav-link" href="<?php echo base_url('backoffice/survei');?>">
            <i class="fas fa-fw fa-file-signature"></i>
            <span>Survey Results</span>
        </a>
    </li>


    <?php if($ses_role_id == 1){?>

    <li class="nav-item <?php echo ($this->uri->segment(2) == 'master' ? 'active' : '');?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapselayanan" aria-expanded="false" aria-controls="collapselayanan">
            <i class="far fa-list-alt"></i>
            <span>Master</span>
        </a>
        <div id="collapselayanan" class="collapse <?php echo ($this->uri->segment(2) == 'master' ? 'show' : '');?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                
                <a class="collapse-item <?php echo (($this->uri->segment(2) == 'master' && $this->uri->segment(3) == 'kategori-donasi') ? 'active' : '');?>" href="<?php echo base_url('backoffice/master/kategori-donasi');?>">
                    Donation Category
                </a>
                <a class="collapse-item <?php echo (($this->uri->segment(2) == 'master' && $this->uri->segment(3) == 'pengguna') ? 'active' : '');?>" href="<?php echo base_url('backoffice/master/pengguna');?>">
                    Users
                </a>
                <a class="collapse-item <?php echo (($this->uri->segment(2) == 'master' && $this->uri->segment(3) == 'role') ? 'active' : '');?>" href="<?php echo base_url('backoffice/master/role');?>">
                    User Role
                </a>
                
            </div>
        </div>
    </li>
    <?php } ?>

    <li class="nav-item">
        <a class="nav-link btn-logout" href="javascript:;">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span>
        </a>
    </li>
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->

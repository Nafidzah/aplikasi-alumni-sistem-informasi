<form id="form_caption" onsubmit="return false;">
    <div class="form-group">
        <?php
            $path = './uploads/dokumentasi/';
            if(file_exists($path . $edit['photo_name']) && $edit['photo_name']){
                echo '<center>
                        <img src="'.base_url('uploads/dokumentasi/thumbs/' . $edit['photo_name']).'" class="img-thumbnail">
                    </center>
                    ';
            }
        ?>
    </div>
    <div class="form-group">
        <label>Caption</label>
        <div>
            <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
            <input type="text" name="photo_caption" class="form-control" value="<?php echo $edit['photo_caption'];?>">
        </div>
    </div>
</form>         
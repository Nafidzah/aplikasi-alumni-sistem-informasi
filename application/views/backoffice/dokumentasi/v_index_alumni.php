<div class="container-fluid">
    <div class="row">
        <div class="col-12">              
            <?php
            if($album){
                $path = './uploads/dokumentasi/';


                echo '<div>';
                
                $no = 1;
                foreach($album as $row){
                    if(file_exists($path . $row['album_cover']) && $row['album_cover']){
                        $album_cover = base_url('uploads/dokumentasi/' . $row['album_cover']);
                    } else {
                        $album_cover = base_url('uploads/dokumentasi/no_image.jpg');
                    }
                    
                    $url_detail = base_url('backoffice/dokumentasi/detail/' . $row['id'] . '/' . strtolower(url_title($row['album_name'])));
                    
                    if($no % 2 == 1){
                        echo '<div class="row">';
                    }

                    echo '<div class="col-md-6 col-12 mb-3">
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="img-fluid" src="'.$album_cover.'">
                                    </div>
                                    <div class="mt-3">
                                        <h5 class="text-secondary"><strong>'.$row['album_name'].'</strong></h5>
                                        <p>'.character_limiter(strip_tags($row['album_description']), 150).'</p>
                                        <a href="'.$url_detail.'" title="See More">
                                            See More <i class="fas fa-fw fa-long-arrow-alt-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>';

                    if($no % 2 == 0){
                        echo '</div>';
                    }

                    $no++;
                }

                echo '</div>

                    <div class="row">
                        <div class="col-12">
                            '.$pagination.'
                        </div>
                    </div>';
            } else {
                echo '
                    <div class="text-center pb-5 py-5 mt-5">
                        <div class="px-3 h-100 d-flex justify-content-center align-items-center text-center text-secondary font-weight-bold">
                            <i class="fas fa-file-alt fa-5x"></i>
                        </div>
                        <br>                    
                        <div>
                            No Data
                        </div>
                    </div>
                    ';
            }
            ?>
        </div>
    </div>
</div>
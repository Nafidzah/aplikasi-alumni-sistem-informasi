<div id="aniimated-thumbnials" class="list-unstyled row clearfix">
    <?php
    $path           = base_url('uploads/dokumentasi');
    $path_thumbs    = base_url('uploads/dokumentasi/thumbs');

    foreach($album as $row){
        echo '<div class="col-lg-2 col-md-2 col-sm-3 col-6">
                <a href="'.$path  .'/'. $row['photo_name'].'" data-sub-html="'. $row['photo_caption'].'" title="'.$row['photo_caption'].'">
                    <img class="img-responsive img-thumbnail" src="'.$path_thumbs .'/'. $row['photo_name'].'">
                </a>
                <div class="mt-2">
                    <button class="btn btn-sm btn-danger float-right" type="button" onClick="hapus_foto(\''.$row['token'].'\')"  title="Delete">
                        <small><i class="fas fa-trash-alt"></i> Delete</small>
                    </button>
                    <button class="btn btn-sm btn-warning float-left" type="button" onClick="caption(\''.$row['photo_id'].'\')" title="Add Caption">
                        <small><i class="fas fa-edit"></i> Caption</small>
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>';
    }
    ?>
</div>
<script>
    $(function () {
        $("#aniimated-thumbnials").lightGallery({
            thumbnail: true,
            selector: 'a',
            loop: true,
            keypress: true,
            getCaptionFromTitleOrAlt: true
        });
    });
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group row">
                                <label for="album_name" class="col-md-3">
                                    <strong>Title</strong>
                                </label>
                                <div class="col-md-9">
                                    <?php echo $album['album_name'];?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="album_description" class="col-md-3">
                                    <strong>Description</strong>
                                </label>
                                <div class="col-md-9">
                                    <?php echo nl2br($album['album_description']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row"  id="aniimated-thumbnials">
                        <?php 
                        $path           = base_url('uploads/dokumentasi');
                        $path_thumbs    = base_url('uploads/dokumentasi/thumbs');

                        foreach($photo as $row){
                            echo '<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-3">
                                    <a href="'.$path  .'/'. $row['photo_name'].'" data-sub-html="'. $row['photo_caption'].'" title="'.$row['photo_caption'].'">
                                        <img class="img-responsive img-thumbnail" src="'.$path_thumbs .'/'. $row['photo_name'].'">
                                    </a>
                                </div>';
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <a href="<?php echo base_url('backoffice/dokumentasi');?>" class="btn btn-outline-secondary">
                                     <i class="fas fa-reply"></i> Close
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#aniimated-thumbnials").lightGallery({
            thumbnail: true,
            selector: 'a',
            loop: true,
            keypress: true,
            getCaptionFromTitleOrAlt: true
        });
    });
</script>
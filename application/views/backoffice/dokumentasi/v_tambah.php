<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="album_name">
                                        <strong>Title</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="album_name" name="album_name" value="<?php echo set_value('album_name');?>" class="form-control" placeholder="......">
                                        <?php echo form_error('album_name', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="album_description">
                                        <strong>Description</strong>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="album_description" id="album_description" class="form-control"><?php echo set_value('album_description');?></textarea>
                                        <?php echo form_error('album_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Publish</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" checked> Yes
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T"  > No
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/dokumentasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
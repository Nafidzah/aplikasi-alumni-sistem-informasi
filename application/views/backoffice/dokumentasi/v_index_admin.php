<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Cover</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Publish</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var myTable
    $(document).ready(function() {

        myTable = $('#myTable').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [[2, 'asc']], 
            "oLanguage": {
               "sSearch": "Pencarian",
            },
            "ajax": {
                "url": "<?php echo base_url('backoffice/dokumentasi/ajax_list')?>",
                "type": "POST"
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ],
                    "orderable": false, 
                    "width": 50,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 1 ],
                    "width": 160,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 2 ],
                    "width": 200,
                },
                { 
                    "targets": [ 4 ],
                    "width": 80,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 5 ],
                    "orderable": false, 
                    "width": 150,
                    "className": 'text-center'
                },
            ],
            drawCallback: function () {
                $('.pagination').addClass('pagination-sm');
            }
        });

        var tambah = '<a href="<?php echo base_url('backoffice/dokumentasi/tambah');?>" class="btn btn-dark"><i class="fas fa-plus"></i> Add Documentation</a>';
        $('.dataTables_length').html(tambah);

        
        });
 
   
</script>
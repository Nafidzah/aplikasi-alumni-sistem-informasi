<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-secondary"><?php echo $edit['album_name'];?></h6>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal">                                
                                <div class="form-group">          
                                    <input type="hidden" name="album_id" id="album_id" value="<?php echo $edit['id'];?>">
                                    <div class="dropzone">
                                        <div class="dz-message">
                                            <h3> CLICK OR DROP PHOTO IN HERE</h3>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <p>&nbsp;</p>

                                    <div class="tampil-foto"></div>
                                </div>                       
                                <div class="form-group">                                    
                                    <a href="<?php echo base_url('backoffice/dokumentasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Close
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="MyModalCaption">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="MyModalCaptionTitle"></h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="MyModalCaptionBody"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-dark" onclick="simpan()">
                    <i class="fa fa-save"></i> Save
                </button>
                <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                    <i class="fa fa-reply"></i> Cancel
                </button>
            </div>
        </div>
    </div>
</div>        
<!-- END MODAL -->  

<script>
    var progress_bar = '<div class="text-center"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div>LOADING DATA</div>';

    /*
    | -------------------------------------------------------------------
    | UNTUK MENGUPLOAD VIA DROPZONE
    | -------------------------------------------------------------------
    */
    Dropzone.autoDiscover = false;

    var foto_upload= new Dropzone(".dropzone",{
        url: "<?php echo base_url('backoffice/dokumentasi/upload_foto');?>",
        maxFilesize: 5,
        method:"post",
        acceptedFiles:"image/*",
        paramName:"userfile",
        dictInvalidFileType:"File type not allowed",
        addRemoveLinks:true,
    });

    /*
    | -------------------------------------------------------------------
    | EVENT KETIKA MENGUPLOAD FILE
    | -------------------------------------------------------------------
    */
    foto_upload.on("sending",function(a,b,c){
        a.token=Math.random();
        c.append("token_foto", a.token);
        c.append("album_id", $("#album_id").val());
    });

    /*
    | -------------------------------------------------------------------
    | JIKA FILE YANG BARU DIUPLOAD, DIHAPUS
    | -------------------------------------------------------------------
    */
    foto_upload.on("removedfile",function(a){
        var token       = a.token;
        var post_data   = {token:token};
        var post_url    = "<?php echo base_url('backoffice/dokumentasi/hapus_foto');?>";

        swal({
            title: "Confirm Delete Data",
            text: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Yes',
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){                
                $.post(post_url, post_data, function(data, status){
                    swal('Success!', 'Data has been deleted!', 'success');
                    daftar_foto();
                });
            } else {
                swal("Canceled!", "Delete canceled!", "error");
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | JIKA FOTO BERHASIL DIUPLOAD, MAKA TAMPILKAN DAFTAR FOTO
    | -------------------------------------------------------------------
    */
    foto_upload.on("complete",function(a){
        daftar_foto();
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK MENGHAPUS FOTO, PADA DAFTAR FOTO
    | -------------------------------------------------------------------
    */
    function hapus_foto(token){
        var post_data   = {token:token};
        var post_url    = "<?php echo base_url('backoffice/dokumentasi/hapus_foto');?>";

        swal({
            title: "Confirm Delete Data",
            text: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Yes',
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm){                
                $.post(post_url, post_data, function(data, status){
                    swal('Success!', 'Data has been deleted!', 'success');
                    daftar_foto();
                });
            } else {
                swal("Canceled!", "Delete canceled!", "error");
            }
        });
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DAFTAR FOTO
    | -------------------------------------------------------------------
    */
    function daftar_foto(){
        var id          = $("#album_id").val();
        var post_data   = {id:id};
        var post_url    = "<?php echo base_url('backoffice/dokumentasi/daftar_foto');?>";

        $.post(post_url, post_data, function(data, status){
            $(".tampil-foto").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENGUBAH CAPTION PADA FOTO
    | -------------------------------------------------------------------
    */
    function caption(id){
        $("#MyModalCaption").modal('show');
        $("#MyModalCaptionBody").html(progress_bar);
        $("#MyModalCaptionTitle").html('Ubah Caption Foto');

        var post_data   = {id:id};
        var post_url    = "<?php echo base_url('backoffice/dokumentasi/caption'); ?>";

        $.post(post_url, post_data, function(data, status){
            $("#MyModalCaptionBody").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENYIMPAN CAPTION FOTO
    | -------------------------------------------------------------------
    */
    function simpan(){            
        var post_data   = $("#form_caption").serialize();
        var post_url    = "<?php echo base_url('backoffice/dokumentasi/simpan_caption'); ?>";
        
        $.post(post_url, post_data, function(data, status){
            $("#MyModalCaption").modal('hide');
            daftar_foto();
        });
    }

    daftar_foto();
    
</script>
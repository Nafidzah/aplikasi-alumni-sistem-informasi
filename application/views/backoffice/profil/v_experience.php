<button type="button" class="btn btn-dark btn-tambah-experience mb-2" data-url="<?php echo base_url('backoffice/profil/ajax_experience_tambah/' . $users_id);?>">
    <i class="fas fa-plus"></i> Add Experience
</button>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Name</th>
                <th style="width:100px;" class="text-center">Year</th>
                <th style="width:200px;" class="text-center">Position</th>
                <th style="width:120px;" class="text-center">Option</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($experience){
                $nomor = 1;
                foreach($experience as $row){
                    
                    $url_edit   = base_url('backoffice/profil/ajax_experience_ubah/' . $row['id']);
                    $url_delete = base_url('backoffice/profil/ajax_experience_hapus/' . $row['id']);
                    
                    echo '<tr>
                            <td class="text-center">
                                '.$nomor.'
                            </td>
                            <td>'.$row['experience'].'</td>
                            <td class="text-center">
                                '.$row['experience_year'].'
                            </td>
                            <td>'.$row['experience_position'].'</td>
                            <td class="text-center">
                                <button type="button" title="Edit Data" class="btn btn-warning btn-sm btn-ubah-experience" data-url="'.$url_edit.'">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" title="Delete Data" class="btn btn-danger btn-sm btn-hapus-experience" data-url="'.$url_delete.'">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="5" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
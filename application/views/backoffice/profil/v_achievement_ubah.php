<form id="form_achievement" onsubmit="return false;">
    <input type="hidden" name="achievement_aksi" value="ubah">
    <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
    <div class="form-group">
        <label>Prestasi?penghargaan <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="achievement" id="achievement" class="form-control" value="<?php echo $edit['achievement'];?>" placeholder="Tuliskan pretasi/penghargaan yang anda miliki...">
        </div>
    </div>
    <div class="form-group">
        <label>Tahun <span class="text-danger">*</span></label>
        <div>
            <select name="achievement_year" id="achievement_year" class="form-control">
                <?php
                for($i = date('Y'); $i > 1950; $i--){
                    $selected = ($i == $edit['tahun'] ? 'selected' : '');
                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
</form>
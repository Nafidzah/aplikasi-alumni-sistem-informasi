<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <a href="<?php echo base_url('backoffice/profil/ubah-profil');?>" class="text-secondary float-right" title="Ubah Profil">
                                <i class="fas fa-edit"></i> 
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="border-right pt-3 pb-3 row">
                                <div class="col-md-4">
                                    <?php
                                    $path = './uploads/profil/';
                                    if(file_exists($path . $edit['foto']) && $edit['foto']){
                                        $url_gambar = base_url('uploads/profil/' . $edit['foto']);
                                    } else {
                                        $url_gambar = base_url('assets/img/default.jpg');
                                    }
                                    ?>
                                    <center>
                                        <img src="<?php echo $url_gambar; ?>" alt="John Doe" class="rounded-circle img-thumbnail" style="max-width:96px;">    
                                    </center>                                    
                                    
                                </div>
                                <div class="col-md-8 justify-content-center ">
                                    <h4><small><?php echo ($edit['fullname'] ? $edit['fullname'] : '-');?></small></h4>
                                    <p><?php echo ($edit['tahun_angkatan'] ? $edit['tahun_angkatan'] : '-');?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="border-right p-3">
                                <div class="mb-2">
                                    <i class="fas fa-envelope mr-2"></i> <?php echo ($edit['email'] ? $edit['email'] : '-');?>
                                </div>
                                <div class="mb-2">
                                    <i class="fab fa-whatsapp mr-2"></i> <?php echo ($edit['nomor_hp'] ? $edit['nomor_hp'] : '-');?>
                                </div>
                                <div class="mb-2">
                                    <i class="fab fa-linkedin-in mr-2"></i> <?php echo ($edit['linkedin'] ? $edit['linkedin'] : '-');?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-3">
                                <?php echo ($edit['description'] ? nl2br($edit['description']) : '-');?>
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-tabs nav-justified mt-5">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_post">
                                <i class="fas fa-newspaper mr-2"></i> Post
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_skill">
                                <i class="fas fa-swatchbook mr-2"></i> Skill
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_achievement">
                                <i class="fas fa-medal mr-2"></i> Achievement
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_contribution">
                                <i class="fas fa-people-carry mr-2"></i> Contribution
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_experience">
                                <i class="fas fa-briefcase mr-2"></i> Experience
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane container active" id="tab_post">
                            Coming Soon..
                        </div>
                        <div class="tab-pane container fade" id="tab_skill">
                            <div class="mt-3 mb-3" id="tampil_skill"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalSkill">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalSkillTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalSkillBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-skill">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->                
                        </div>
                        <div class="tab-pane container fade" id="tab_achievement">
                            <div class="mt-3 mb-3" id="tampil_achievement"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalAchievement">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalAchievementTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalAchievementBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-achievement">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->   
                        </div>
                        <div class="tab-pane container fade" id="tab_contribution">
                            <div class="mt-3 mb-3" id="tampil_contribution"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalContribution">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalContributionTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalContributionBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-contribution">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->   
                        </div>
                        <div class="tab-pane container fade" id="tab_experience">
                            <div class="mt-3 mb-3" id="tampil_experience"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalExperience">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalExperienceTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalExperienceBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-experience">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var progress_bar = '<div class="text-center"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div>LOADING DATA</div>';

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill(){
        var get_url = "<?php echo base_url('backoffice/profil/ajax_skill_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_skill").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA SKILL
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-tambah-skill",function() {
        $("#MyModalSkill").modal('show');
        $("#MyModalSkillBody").html(progress_bar);
        $("#MyModalSkillTitle").html('Add Skill');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalSkillBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | UBAH DATA SKILL
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-ubah-skill",function() {
        $("#MyModalSkill").modal('show');
        $("#MyModalSkillBody").html(progress_bar);
        $("#MyModalSkillTitle").html('Edit Skill');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalSkillBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA SKILL
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-skill",function() {
        var url = $(this).data('url');
        swal({
                title: "Confirm Delete Data",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Data has been deleted!', 'success');
                        ajax_skill();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA SKILL
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-simpan-skill",function() {

        if($.trim($("#skill").val()) == ''){
            document.getElementById("skill").focus();
            swal("Sorry", "Skill is required!", "warning");
            return false;
        }

        if($.trim($("#skill_level").val())== ''){
            document.getElementById("skill_level").focus();
            swal("Sorry", "Skill level is required!", "warning");
            return false;
        }

        var post_url    = "<?php echo base_url('backoffice/profil/ajax_skill_simpan');?>";
        var post_data   = $("#form_skill").serialize();

        $.post(post_url, post_data, function(data, status){
            if(data == 1){ // JIKA SUKSES
                $("#MyModalSkill").modal('hide');
                swal('Success!', 'Data saved successfully!', 'success');
                ajax_skill();                
            } else if(data == 2){ // JIKA DATA-NYA SUDAH ADA
                swal('Sorry', 'Skills already available!', 'warning');
            } else {
                swal("Error!", "Data could not be saved!", "error");
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement(){
        var get_url = "<?php echo base_url('backoffice/profil/ajax_achievement_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_achievement").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-tambah-achievement",function() {
        $("#MyModalAchievement").modal('show');
        $("#MyModalAchievementBody").html(progress_bar);
        $("#MyModalAchievementTitle").html('Add Achievement');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalAchievementBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | UBAH DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-ubah-achievement",function() {
        $("#MyModalAchievement").modal('show');
        $("#MyModalAchievementBody").html(progress_bar);
        $("#MyModalAchievementTitle").html('Edit Achievement');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalAchievementBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-achievement",function() {
        var url = $(this).data('url');
        swal({
                title: "Confirm Delete Data",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Data has been deleted!', 'success');
                        ajax_achievement();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-simpan-achievement",function() {

        if($.trim($("#achievement").val()) == ''){
            document.getElementById("achievement").focus();
            swal("Sorry!", "Name is required!", "warning");
            return false;
        }

        if($.trim($("#achievement_year").val())== ''){
            document.getElementById("achievement_year").focus();
            swal("Sorry!", "Year is required!", "warning");
            return false;
        }

        var post_url    = "<?php echo base_url('backoffice/profil/ajax_achievement_simpan');?>";
        var post_data   = $("#form_achievement").serialize();

        $.post(post_url, post_data, function(data, status){
            if(data == 1){ // JIKA SUKSES
                $("#MyModalAchievement").modal('hide');
                swal('Success!', 'Data saved successfully!', 'success');
                ajax_achievement();                
            } else if(data == 2){ // JIKA DATA-NYA SUDAH ADA
                swal('Sorry!!', 'Experience already available!', 'warning');
            } else {
                swal("Error!", "Data could not be saved!", "error");
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution(){
        var get_url = "<?php echo base_url('backoffice/profil/ajax_contribution_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_contribution").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-tambah-contribution",function() {
        $("#MyModalContribution").modal('show');
        $("#MyModalContributionBody").html(progress_bar);
        $("#MyModalContributionTitle").html('Add Contribution');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalContributionBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | UBAH DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-ubah-contribution",function() {
        $("#MyModalContribution").modal('show');
        $("#MyModalContributionBody").html(progress_bar);
        $("#MyModalContributionTitle").html('Edit Contribution');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalContributionBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-contribution",function() {
        var url = $(this).data('url');
        swal({
                title: "Confirm Delete Data",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Data has been deleted!', 'success');
                        ajax_contribution();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-simpan-contribution",function() {

        if($.trim($("#contribution").val()) == ''){
            document.getElementById("contribution").focus();
            swal("Sorry!", "Name is required!", "warning");
            return false;
        }

        if($.trim($("#contribution_year").val())== ''){
            document.getElementById("contribution_year").focus();
            swal("Sorry!", "Year is required!", "warning");
            return false;
        }

        if($.trim($("#contribution_position").val())== ''){
            document.getElementById("contribution_position").focus();
            swal("Sorry!", "Position is required!", "warning");
            return false;
        }

        var post_url    = "<?php echo base_url('backoffice/profil/ajax_contribution_simpan');?>";
        var post_data   = $("#form_contribution").serialize();

        $.post(post_url, post_data, function(data, status){
            if(data == 1){ // JIKA SUKSES
                $("#MyModalContribution").modal('hide');
                swal('Success!', 'Data saved successfully!', 'success');
                ajax_contribution();                
            } else if(data == 2){ // JIKA DATA-NYA SUDAH ADA
                swal('Sorry!!', 'Contribution already available!', 'warning');
            } else {
                swal("Error!", "Data could not be saved!", "error");
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience(){
        var get_url = "<?php echo base_url('backoffice/profil/ajax_experience_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_experience").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | TAMBAH DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-tambah-experience",function() {
        $("#MyModalExperience").modal('show');
        $("#MyModalExperienceBody").html(progress_bar);
        $("#MyModalExperienceTitle").html('Add Experience');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalExperienceBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | UBAH DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-ubah-experience",function() {
        $("#MyModalExperience").modal('show');
        $("#MyModalExperienceBody").html(progress_bar);
        $("#MyModalExperienceTitle").html('Edit Experience');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalExperienceBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | HAPUS DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-experience",function() {
        var url = $(this).data('url');
        swal({
                title: "Confirm Delete Data",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Data has been deleted!', 'success');
                        ajax_experience();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | SIMPAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-simpan-experience",function() {

        if($.trim($("#experience").val()) == ''){
            document.getElementById("experience").focus();
            swal("Sorry!", "Name is required!", "warning");
            return false;
        }

        if($.trim($("#experience_year").val())== ''){
            document.getElementById("experience_year").focus();
            swal("Sorry!", "Year is required!", "warning");
            return false;
        }

        if($.trim($("#experience_position").val())== ''){
            document.getElementById("experience_position").focus();
            swal("Sorry!", "Position is required!", "warning");
            return false;
        }

        var post_url    = "<?php echo base_url('backoffice/profil/ajax_experience_simpan');?>";
        var post_data   = $("#form_experience").serialize();

        $.post(post_url, post_data, function(data, status){
            if(data == 1){ // JIKA SUKSES
                $("#MyModalExperience").modal('hide');
                swal('Success!', 'Data saved successfully!', 'success');
                ajax_experience();
            } else if(data == 2){ // JIKA DATA-NYA SUDAH ADA
                swal('Sorry!!', 'Experience already available!', 'warning');
            } else {
                swal("Error!", "Data could not be saved!", "error");
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | AUTOLOAD
    | -------------------------------------------------------------------
    */
    ajax_skill();
    ajax_achievement();
    ajax_contribution();
    ajax_experience();
</script>
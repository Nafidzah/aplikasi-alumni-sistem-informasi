<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="alert alert-secondary">
                                            <strong><i class="fas fa-check"></i> PLEASE TICK FOR DATA TO BE DISPLAYED IN YOUR PROFILE</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_description" value="Y" <?php echo ($edit['view_description'] == 'Y' ? 'checked' : '');?>> Show Bio
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_nomor_hp" value="Y" <?php echo ($edit['view_nomor_hp'] == 'Y' ? 'checked' : '');?>> Show Phone Number
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_email" value="Y" <?php echo ($edit['view_email'] == 'Y' ? 'checked' : '');?>> Show Email
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_tempat_lahir" value="Y" <?php echo ($edit['view_tempat_lahir'] == 'Y' ? 'checked' : '');?>> Show Place of Birth
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_tanggal_lahir" value="Y" <?php echo ($edit['view_tanggal_lahir'] == 'Y' ? 'checked' : '');?>> Show Date of Birth
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_jenis_kelamin" value="Y" <?php echo ($edit['view_jenis_kelamin'] == 'Y' ? 'checked' : '');?>> Show Gender
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_website" value="Y" <?php echo ($edit['view_website'] == 'Y' ? 'checked' : '');?>> Show Website
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_instagram" value="Y" <?php echo ($edit['view_instagram'] == 'Y' ? 'checked' : '');?>> Show Instagram
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_twitter" value="Y" <?php echo ($edit['view_twitter'] == 'Y' ? 'checked' : '');?>> Show Twitter
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_facebook" value="Y" <?php echo ($edit['view_facebook'] == 'Y' ? 'checked' : '');?>> Show Facebook
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_linkedin" value="Y" <?php echo ($edit['view_linkedin'] == 'Y' ? 'checked' : '');?>> Show Linkedin
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_skill" value="Y" <?php echo ($edit['view_skill'] == 'Y' ? 'checked' : '');?>> Show Skills
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_achievement" value="Y" <?php echo ($edit['view_achievement'] == 'Y' ? 'checked' : '');?>> Show Achievement
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="view_experience" value="Y" <?php echo ($edit['view_experience'] == 'Y' ? 'checked' : '');?>> Show Experience
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="offset-md-3 col-md-7 offset-sm-6 offset-sm-6">
                                            <button type="submit" class="btn btn-dark" name="submit" value="1">
                                                <i class="fas fa-save"></i> Save
                                            </button>
                                            <a href="<?php echo base_url('backoffice/profil');?>" class="btn btn-outline-secondary">
                                                 <i class="fas fa-reply"></i> Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

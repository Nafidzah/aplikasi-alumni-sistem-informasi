<button type="button" class="btn btn-dark btn-tambah-skill mb-2" data-url="<?php echo base_url('backoffice/profil/ajax_skill_tambah/' . $users_id);?>">
    <i class="fas fa-plus"></i> Tambah
</button>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Skill</th>
                <th style="width:200px;" class="text-center">Level</th>
                <th style="width:120px;" class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($skill){
                $nomor = 1;
                foreach($skill as $row){
                    
                    $url_edit   = base_url('backoffice/profil/ajax_skill_ubah/' . $row['id']);
                    $url_delete = base_url('backoffice/profil/ajax_skill_hapus/' . $row['id']);
                    
                    echo '<tr>
                            <td class="text-center">'.$nomor.'</td>
                            <td>'.$row['skill'].'</td>
                            <td class="text-center">'.$row['skill_level'].'</td>
                            <td class="text-center">
                                <button type="button" title="Ubah Data" class="btn btn-warning btn-sm btn-ubah-skill" data-url="'.$url_edit.'">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" title="Hapus Data" class="btn btn-danger btn-sm btn-hapus-skill" data-url="'.$url_delete.'">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="4" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
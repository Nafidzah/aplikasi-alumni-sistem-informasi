<form id="form_experience" onsubmit="return false;">
    <input type="hidden" name="experience_aksi" value="ubah">
    <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
    <div class="form-group">
        <label>Name <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="experience" id="experience" class="form-control" value="<?php echo $edit['experience'];?>" placeholder="......">
        </div>
    </div>
    <div class="form-group">
        <label>Year <span class="text-danger">*</span></label>
        <div>
            <select name="experience_year" id="experience_year" class="form-control">
                <?php
                for($i = date('Y'); $i > 1950; $i--){
                    $selected = ($i == $edit['experience_year'] ? 'selected' : '');
                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label>Position <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="experience_position" id="experience_position" value="<?php echo $edit['experience_position'];?>" class="form-control" placeholder="......">
        </div>
    </div>
</form>
<form id="form_contribution" onsubmit="return false;">
    <input type="hidden" name="contribution_aksi" value="tambah">
    <input type="hidden" name="users_id" value="<?php echo $users_id;?>">
    <div class="form-group">
        <label>Name <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="contribution" id="contribution" class="form-control" placeholder="..........">
        </div>
    </div>
    <div class="form-group">
        <label>Year <span class="text-danger">*</span></label>
        <div>
            <select name="contribution_year" id="contribution_year" class="form-control">
                <?php
                for($i = date('Y'); $i > 1950; $i--){
                    $selected = ($i == date('Y') ? 'selected' : '');
                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label>Position <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="contribution_position" id="contribution_position" class="form-control" placeholder="..........">
        </div>
    </div>
</form>
<button type="button" class="btn btn-dark btn-tambah-achievement mb-2" data-url="<?php echo base_url('backoffice/profil/ajax_achievement_tambah/' . $users_id);?>">
    <i class="fas fa-plus"></i> Tambah
</button>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Prestasi</th>
                <th style="width:200px;" class="text-center">Tahun</th>
                <th style="width:120px;" class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($achievement){
                $nomor = 1;
                foreach($achievement as $row){
                    
                    $url_edit   = base_url('backoffice/profil/ajax_achievement_ubah/' . $row['id']);
                    $url_delete = base_url('backoffice/profil/ajax_achievement_hapus/' . $row['id']);
                    
                    echo '<tr>
                            <td class="text-center">'.$nomor.'</td>
                            <td>'.$row['achievement'].'</td>
                            <td class="text-center">'.$row['achievement_year'].'</td>
                            <td class="text-center">
                                <button type="button" title="Ubah Data" class="btn btn-warning btn-sm btn-ubah-achievement" data-url="'.$url_edit.'">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" title="Hapus Data" class="btn btn-danger btn-sm btn-hapus-achievement" data-url="'.$url_delete.'">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="4" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
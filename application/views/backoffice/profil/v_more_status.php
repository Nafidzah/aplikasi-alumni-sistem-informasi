<?php 
if($forum){
    $path_foto_profil = './uploads/profil/';
    
    foreach($forum as $row){

        // FOTO UNTUK PROFIL SI STATUS
        if(file_exists($path_foto_profil . $row['foto']) && $row['foto']){
            $url_profil_status = base_url('uploads/profil/' . $row['foto']);
        } else {
            $url_profil_status = base_url('assets/img/default.jpg');
        }

        // FOTO UNTUK PROFIL SI PEMBUAT KOMEN
        if(file_exists($path_foto_profil . $foto) && $foto){
            $url_profil_komentar = base_url('uploads/profil/' . $foto);
        } else {
            $url_profil_komentar = base_url('assets/img/default.jpg');
        }
?>
<div class="card card-forum mb-4 loop-status" data-id="<?php echo $offset;?>" id="status_<?php echo $row['id'];?>" data-last="<?php echo $row['id'];?>">
    <div class="card-header bg-white pb-0" style="border-bottom: none;">
        <div class="media mb-3">
            <div class="col-11">
                <img src="<?php echo $url_profil_status;?>" alt="<?php echo $row['created_by'];?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
                <strong><?php echo $row['created_by'];?></strong>
                <div class="ml-5">
                    <small style="font-size: 10px;"><?php echo date('d M Y, H:i:s', strtotime($row['created_at']));?> WIB</small>
                </div>
            </div>
            <div class="col-1">

                <?php if($role_id == 1 OR $users_id == $row['created_by_id']){?>
                
                <a class="float-right btn-hapus-status" data-id="<?php echo $row['id'];?>" data-url="<?php echo base_url('backoffice/profil/ajax_status_hapus/' . $row['id']);?>" href="javascript:;" title="Delete This Post!">
                    <i class="fas fa-ellipsis-h"></i>
                </a>
                
                <?php } ?>

            </div>
        </div> 
    </div>
    <div class="card-body px-4 pt-0">
        <?php
        // JIKA ADA FOTO PADA FORUM
        $url_forum_cover = '';
        if(file_exists('./uploads/forum/' . $row['forum_cover']) && $row['forum_cover']){
            $url_forum_cover = base_url('uploads/forum/' . $row['forum_cover']);
        }

        if(file_exists('./uploads/donasi/' . $row['forum_cover']) && $row['forum_cover']){
            $url_forum_cover = base_url('uploads/donasi/' . $row['forum_cover']);
        }

        if($url_forum_cover != ''){
            echo '<center>
                <img src="'.$url_forum_cover.'" class="img-fluid mb-3">
            </center>';
        }

        echo nl2br($row['forum_description']);
        ?>
        <div class="row">
            <div class="col-12 col-sm-6">
                <a href="javascript:;" class="btn btn-default btn-sm btn-like" data-id="<?php echo $row['id'];?>">
                    <i class="fas fa-heart is_like_<?php echo $row['id'];?> <?php echo ($row['sudah_like'] > 0 ? 'text-danger' : '');?>"></i>
                </a>
                <a href="javascript:;" class="btn btn-default btn-sm btn-comment" data-id="<?php echo $row['id'];?>">
                    <i class="fas fa-comment"></i>
                </a>
            </div>
            <div class="col-12 col-sm-6">
                <div class="float-sm-right">
                    <small>
                        <label class="mr-3">
                            <span id="total_like_<?php echo $row['id'];?>"><?php echo $row['total_like'];?></span> Likes
                        </label>
                        <label>
                            <span id="total_comment_<?php echo $row['id'];?>"><?php echo $row['total_comment'];?></span> Comments</small>
                        </label>
                    </small>
                </div>                            
            </div>
        </div>
    </div>
    <div class="card-footer bg-white">
        <div id="media_comment_<?php echo $row['id'];?>">
            <?php
            $komentar = $this->M_forum->get_forum_by_forum_id_parent($row['id']);
            foreach($komentar as $row_komentar):
                // FOTO UNTUK PROFIL YANG TELAH KOMEN
                if(file_exists($path_foto_profil . $row_komentar['foto']) && $row_komentar['foto']){
                    $url_profil_telah_komentar = base_url('uploads/profil/' . $row_komentar['foto']);
                } else {
                    $url_profil_telah_komentar = base_url('assets/img/default.jpg');
                }
            ?>
                <div class="media mb-3" id="komentar_<?php echo $row_komentar['id'];?>">
                    <img src="<?php echo $url_profil_telah_komentar;?>" alt="<?php echo $row_komentar['created_by'];?>" title="<?php echo $row_komentar['created_by'];?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
                    <div class="media-body border-bottom pb-4">
                        <div class="row">
                            <div class="col-11">
                                <div>
                                    <strong><?php echo $row_komentar['created_by'];?></strong>
                                    <div>
                                        <small style="font-size: 10px;"><?php echo date('d M Y, H:i:s', strtotime($row_komentar['created_at']));?> WIB</small>
                                    </div>
                                </div>
                                <?php echo nl2br($row_komentar['forum_description']);?>
                            </div>
                            <div class="col-1">
                                <?php if($role_id == 1 OR $users_id == $row_komentar['created_by_id']){?>
                                    
                                    <a class="float-right btn-hapus-comment mr-3" data-id="<?php echo $row_komentar['forum_id_parent'];?>" data-url="<?php echo base_url('backoffice/profil/ajax_status_hapus/' . $row_komentar['id'] . '/' . $row_komentar['forum_id_parent']);?>" href="javascript:;" title="Delete This Post!">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </a>
                                
                                <?php } ?>
                            </div> 
                        </div>
                    </div>
                </div> 
            <?php endforeach; ?>
        </div>
        <div class="media mb-3">
            <img src="<?php echo $url_profil_komentar;?>" alt="<?php echo $fullname;?>" title="<?php echo $fullname;?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
            <div class="media-body">
                <textarea name="forum_description_komentar" id="forum_description_komentar_<?php echo $row['id'];?>" class="form-control" placeholder="Write something here......" rows="3"></textarea>
                <button type="button" class="btn btn-dark btn-sm mt-2 btn-submit-comment" data-id="<?php echo $row['id'];?>">
                    Comment Now <i class="fas fa-fw fa-long-arrow-alt-right"></i>
                </button>
            </div>
        </div> 
    </div>
</div>
<?php 
    $offset++;
    }
}
?>
<form id="form_achievement" onsubmit="return false;">
    <input type="hidden" name="achievement_aksi" value="tambah">
    <input type="hidden" name="users_id" value="<?php echo $users_id;?>">
    <div class="form-group">
        <label>Prestasi/Penghargaan <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="achievement" id="achievement" class="form-control" placeholder="Tuliskan prestasi/penghargaan yang anda miliki...">
        </div>
    </div>
    <div class="form-group">
        <label>Tahun <span class="text-danger">*</span></label>
        <div>
            <select name="achievement_year" id="achievement_year" class="form-control">
                <?php
                for($i = date('Y'); $i > 1950; $i--){
                    $selected = ($i == date('Y') ? 'selected' : '');
                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
</form>
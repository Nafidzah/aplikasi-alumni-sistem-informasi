<form id="form_skill" onsubmit="return false;">
    <input type="hidden" name="skill_aksi" value="ubah">
    <input type="hidden" name="id" value="<?php echo $edit['id'];?>">
    <div class="form-group">
        <label>Skill <span class="text-danger">*</span></label>
        <div>
            <input type="text" name="skill" id="skill" class="form-control" value="<?php echo $edit['skill'];?>" placeholder="Tuliskan skill yang anda miliki...">
        </div>
    </div>
    <div class="form-group">
        <label>Skill Level <span class="text-danger">*</span></label>
        <div>
            <select name="skill_level" id="skill_level" class="form-control">
                <?php
                for($i = 1; $i<=10; $i++){
                    $selected = ($i == $edit['skill_level'] ? 'selected' : '');
                    echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                }
                ?>
            </select>
        </div>
    </div>
</form>
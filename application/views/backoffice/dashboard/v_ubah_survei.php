<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="survey_name" name="survey_name" value="<?php echo $edit['survey_name'];?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('survey_name', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_description">
                                        <strong>Description</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="survey_description" id="survey_description" class="form-control" required="required"><?php echo $edit['survey_description'];?></textarea>
                                        <?php echo form_error('survey_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                <strong>Start Period Date</strong>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control tanggal" name="start_date" id="start_date" value="<?php echo date('d/m/Y', strtotime($edit['start_date']));?>" autocomplete="off" onkeydown="event.preventDefault()">
                                                <div class="input-group-prepend input-group-addon-tanggal">
                                                    <small class="input-group-text">
                                                        <i class="fas fa-calendar"></i>
                                                    </small>
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <strong>Start Period Time</strong>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control clockpicker" name="start_time" id="start_time" value="<?php echo $edit['start_time'];?>" autocomplete="off" onkeydown="event.preventDefault()">
                                                <div class="input-group-prepend input-group-waktu">
                                                    <small class="input-group-text">
                                                        <i class="fas fa-clock"></i>
                                                    </small>
                                                </div>                                        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                <strong>End Period Date</strong>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control tanggal" name="end_date" id="end_date" value="<?php echo date('d/m/Y', strtotime($edit['end_date']));?>" autocomplete="off" onkeydown="event.preventDefault()">
                                                <div class="input-group-prepend input-group-addon-tanggal">
                                                    <small class="input-group-text">
                                                        <i class="fas fa-calendar"></i>
                                                    </small>
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <strong>End Period Time</strong>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control clockpicker" name="end_time" id="end_time" value="<?php echo $edit['end_time'];?>" autocomplete="off" onkeydown="event.preventDefault()">
                                                <div class="input-group-prepend input-group-waktu">
                                                    <small class="input-group-text">
                                                        <i class="fas fa-clock"></i>
                                                    </small>
                                                </div>                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Respondent</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <select class="form-control" name="respondent[]" id="respondent" multiple>
                                            <?php $respondent = explode(',', $edit['respondent']); ?>
                                            <option value="all" <?php echo (in_array('all', $respondent) ? 'selected' : '');?>>All Alumni</option>
                                            <option value="survey" <?php echo (in_array('survey', $respondent) ? 'selected' : '');?>>Survey Pengguna</option>
                                            <?php
                                            for($i = date('Y'); $i >= 2000; $i--){
                                                $selected = (in_array($i, $respondent) ? 'selected' : '');
                                                echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                                            }
                                            ?>
                                        </select>
                                        <?php echo form_error('respondent', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Publish</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" <?php echo ($edit['publish'] == 'Y' ? 'checked' : '');?>> Yes
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T" <?php echo ($edit['publish'] == 'T' ? 'checked' : '');?>> No
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-dark btn-sm" onclick="tambah_question()">
                                                    <i class="fa fa-plus-circle"></i> Add Question
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="container_question">
                                        <?php
                                        $counter_question = 0;
                                        foreach($question as $row){
                                            $counter_question++;
                                            echo '<div class="row border-top border-bottom loop-question" id="loop_question_'.$counter_question.'">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label><strong>Question</strong></label>
                                                            <input type="hidden" name="question[]" value="'.$counter_question.'">
                                                            <input type="hidden" name="survey_question_id_'.$counter_question.'" value="'.$row['id'].'">
                                                            <input type="text" class="form-control" name="question_'.$counter_question.'" id="question_'.$counter_question.'" placeholder="Enter Question" value="'.$row['question'].'">
                                                        </div>
                                                        <div class="form-group">
                                                            <label><strong>Answer Type</strong></label>
                                                            <select name="answer_type_'.$counter_question.'" class="form-control answer-type" data-id="'.$counter_question.'">
                                                                <option value="input" '.($row['answer_type'] == "input" ? "selected" : "").'>INPUT TEXT</option>
                                                                <option value="textarea" '.($row['answer_type'] == "textarea" ? "selected" : "").'>TEXTAREA</option>
                                                                <option value="checkbox" '.($row['answer_type'] == "checkbox" ? "selected" : "").'>CHECKBOX</option>
                                                                <option value="radio" '.($row['answer_type'] == "radio" ? "selected" : "").'>RADIO BUTTON</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-danger btn-sm" onclick="hapus_question('.$counter_question.')">
                                                                <i class="fa fa-trash"></i> Delete Question
                                                            </button>
                                                        </div>
                                                        <div class="form-group" id="container_answer_'.$counter_question.'">';
                                                            
                                                            if($row['answer_type'] == 'radio'){
                                                                echo '<div>
                                                                        <button type="button" class="btn btn-dark btn-sm" onclick="tambah_answer_radio('.$counter_question.')">
                                                                            <i class="fa fa-plus-circle"></i> Add Answer Option
                                                                        </button>
                                                                    </div>
                                                                    <div id="container_answer_radio_'.$counter_question.'">';

                                                                        $answer = explode(',', $row['answer']);
                                                                        
                                                                        echo '<input type="hidden" id="counter_answer_'.$counter_question.'" value="' . $row['id'] . count($answer).'">';
                                                                        
                                                                        
                                                                        $i = 0;
                                                                        foreach($answer as $key => $value):
                                                                            $i++;
                                                                            $value = str_replace(';', ',', $value);
                                                                            $counter_answer = (int) $row['id'] .$i;
                                                                            echo '<div class="mt-2 row loop_radio_'.$counter_question.'" id="loop_radio_'.$counter_answer.'">
                                                                                    <div class="col-1">
                                                                                        <div class="form-check text-center pt-2 pl-5">
                                                                                            <label class="form-check-label">
                                                                                                <input type="radio" class="form-check-input" name="optradio">&nbsp;
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-10">
                                                                                        <input type="hidden" name="answer_'.$counter_question.'[]" value="'.$counter_answer.'">
                                                                                        <input type="text" class="form-control" name="answer_'.$counter_answer.'" placeholder="Enter Option" value="'.$value.'">
                                                                                    </div>
                                                                                    <div class="col-1">
                                                                                        <button type="button" class="btn btn-danger btn-sm btn-hapus-radio" title="Delete Option" onclick="hapus_answer_radio('.$counter_question.', '.$counter_answer.')"><i class="fa fa-trash"></i></button>
                                                                                    </div>
                                                                                </div>';
                                                                            $counter_answer++;
                                                                        endforeach;

                                                                echo '</div>';
                                                            } else if($row['answer_type'] == 'checkbox'){
                                                                echo '<div>
                                                                        <button type="button" class="btn btn-dark btn-sm" onclick="tambah_answer_checkbox('.$counter_question.')">
                                                                            <i class="fa fa-plus-circle"></i> Add Answer Option
                                                                        </button>
                                                                    </div>
                                                                    <div id="container_answer_checkbox_'.$counter_question.'">';
                                                                        
                                                                        $answer = explode(',', $row['answer']);

                                                                        echo '<input type="hidden" id="counter_answer_'.$counter_question.'" value="' . $row['id'] . count($answer).'">';
                                                                        
                                                                        
                                                                        $i = 0;
                                                                        foreach($answer as $key => $value):
                                                                            $i++;
                                                                            $value = str_replace(';', ',', $value);
                                                                            $counter_answer = (int) $row['id'] .$i;
                                                                            echo '<div class="mt-2 row loop_checkbox_'.$counter_question.'" id="loop_checkbox_'.$counter_answer.'">
                                                                                    <div class="col-1">
                                                                                        <div class="form-check text-center pt-2 pl-5">
                                                                                            <label class="form-check-label">
                                                                                                <input type="checkbox" class="form-check-input" name="optcheckbox">&nbsp;
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-10">
                                                                                        <input type="hidden" name="answer_'.$counter_question.'[]" value="'.$counter_answer.'">
                                                                                        <input type="text" class="form-control" name="answer_'.$counter_answer.'" placeholder="Enter Option" value="'.$value.'">
                                                                                    </div>
                                                                                    <div class="col-1">
                                                                                        <button type="button" class="btn btn-danger btn-sm btn-hapus-checkbox" title="Delete Option" onclick="hapus_answer_checkbox('.$counter_question.', '.$counter_answer.')"><i class="fa fa-trash"></i></button>
                                                                                    </div>
                                                                                </div>';
                                                                            $counter_answer++;
                                                                        endforeach;

                                                                echo '</div>';
                                                            }

                                                    echo '</div>
                                                    </div>
                                                </div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/dashboard');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#respondent").select2();
    /*
    | -------------------------------------------------------------------
    | ANSWER TYPE
    | -------------------------------------------------------------------
    */
    $(document).on("change", ".answer-type",function() {
        var answer_type = $(this).val();
        var question_id = $(this).data('id');
        if(answer_type == 'checkbox'){
            generate_checkbox(question_id);
        } else if(answer_type == 'radio'){
            generate_radio(question_id);
        }
    });

    /*
    | -------------------------------------------------------------------
    | ANSWER TYPE RADIO BUTTON
    | -------------------------------------------------------------------
    */
    function generate_radio(question_id){
        var counter_answer = parseInt(question_id + '1');
        var konten = '<div>'+
                        '<button type="button" class="btn btn-dark btn-sm" onclick="tambah_answer_radio('+question_id+')">'+
                            '<i class="fa fa-plus-circle"></i> Add Answer Option'+
                        '</button>'+
                    '</div>'+
                    '<div id="container_answer_radio_'+question_id+'">'+
                        '<div class="mt-2 row loop_radio_'+question_id+'" id="loop_radio_'+counter_answer+'">'+
                            '<div class="col-1">'+
                                '<div class="form-check text-center pt-2 pl-5">'+
                                    '<label class="form-check-label">'+
                                        '<input type="radio" class="form-check-input" name="optradio">&nbsp;'+
                                    '</label>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-10">'+
                                '<input type="hidden" name="answer_'+question_id+'[]" value="'+counter_answer+'">'+
                                '<input type="hidden" id="counter_answer_'+question_id+'" value="'+counter_answer+'">'+
                                '<input type="text" class="form-control" name="answer_'+counter_answer+'" placeholder="Enter Option">'+
                            '</div>'+
                            '<div class="col-1">'+
                                '<button type="button" class="btn btn-danger btn-sm btn-hapus-radio" title="Delete Option" onclick="hapus_answer_radio('+question_id+', '+counter_answer+')"><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        $("#container_answer_" + question_id).html(konten);
    }

    function tambah_answer_radio(question_id){
        var counter_answer = $("#counter_answer_" + question_id).val();
        counter_answer++;
        var konten = '<div class="mt-2 row loop_radio_'+question_id+'" id="loop_radio_'+counter_answer+'">'+
                        '<div class="col-1">'+
                            '<div class="form-check text-center pt-2 pl-5">'+
                                '<label class="form-check-label">'+
                                    '<input type="radio" class="form-check-input">&nbsp;'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-10">'+
                                '<input type="hidden" name="answer_'+question_id+'[]" value="'+counter_answer+'">'+
                                '<input type="text" class="form-control" name="answer_'+counter_answer+'" placeholder="Enter Option">'+
                            '</div>'+
                        '<div class="col-1">'+
                            '<button type="button" class="btn btn-danger btn-sm btn-hapus-radio" title="Delete Option" onclick="hapus_answer_radio('+question_id+', '+counter_answer+')"><i class="fa fa-trash"></i></button>'+
                        '</div>'+
                    '</div>';
        $("#container_answer_radio_" + question_id).append(konten);
        $("#counter_answer_" + question_id).val(counter_answer);
    }

    function hapus_answer_radio(question_id, counter_answer){
        var total_option = $(".loop_radio_" + question_id).length;
        if(total_option == 2){
            swal("Sorry!", "Minimum must two option!", "warning");
        } else {
            $("#loop_radio_" + counter_answer).remove();
        }
    }

    /*
    | -------------------------------------------------------------------
    | ANSWER TYPE CHECKBOX
    | -------------------------------------------------------------------
    */
     function generate_checkbox(question_id){
        var counter_answer = parseInt(question_id + '1');
        var konten = '<div>'+
                        '<button type="button" class="btn btn-dark btn-sm" onclick="tambah_answer_checkbox('+question_id+')">'+
                            '<i class="fa fa-plus-circle"></i> Add Answer Option'+
                        '</button>'+
                    '</div>'+
                    '<div id="container_answer_checkbox_'+question_id+'">'+
                        '<div class="mt-2 row loop_checkbox_'+question_id+'" id="loop_checkbox_'+counter_answer+'">'+
                            '<div class="col-1">'+
                                '<div class="form-check text-center pt-2 pl-5">'+
                                    '<label class="form-check-label">'+
                                        '<input type="checkbox" class="form-check-input" name="optcheckbox">&nbsp;'+
                                    '</label>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-10">'+
                                '<input type="hidden" name="answer_'+question_id+'[]" value="'+counter_answer+'">'+
                                '<input type="hidden" id="counter_answer_'+question_id+'" value="'+counter_answer+'">'+
                                '<input type="text" class="form-control" name="answer_'+counter_answer+'" placeholder="Enter Option">'+
                            '</div>'+
                            '<div class="col-1">'+
                                '<button type="button" class="btn btn-danger btn-sm btn-hapus-checkbox" title="Delete Option" onclick="hapus_answer_checkbox('+question_id+', '+counter_answer+')"><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        $("#container_answer_" + question_id).html(konten);
    }

    function tambah_answer_checkbox(question_id){
        var counter_answer = $("#counter_answer_" + question_id).val();
        counter_answer++;
        var konten = '<div class="mt-2 row loop_checkbox_'+question_id+'" id="loop_checkbox_'+counter_answer+'">'+
                        '<div class="col-1">'+
                            '<div class="form-check text-center pt-2 pl-5">'+
                                '<label class="form-check-label">'+
                                    '<input type="checkbox" class="form-check-input">&nbsp;'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-10">'+
                                '<input type="hidden" name="answer_'+question_id+'[]" value="'+counter_answer+'">'+
                                '<input type="text" class="form-control" name="answer_'+counter_answer+'" placeholder="Enter Option">'+
                            '</div>'+
                        '<div class="col-1">'+
                            '<button type="button" class="btn btn-danger btn-sm btn-hapus-checkbox" title="Delete Option" onclick="hapus_answer_checkbox('+question_id+', '+counter_answer+')"><i class="fa fa-trash"></i></button>'+
                        '</div>'+
                    '</div>';
        $("#container_answer_checkbox_" + question_id).append(konten);
        $("#counter_answer_" + question_id).val(counter_answer);
    }

    function hapus_answer_checkbox(question_id, counter_answer){
        var total_option = $(".loop_checkbox_" + question_id).length;
        if(total_option == 2){
            swal("Sorry!", "Minimum must two option!", "warning");
        } else {
            $("#loop_checkbox_" + counter_answer).remove();
        }
    }

    /*
    | -------------------------------------------------------------------
    | QUESTION
    | -------------------------------------------------------------------
    */
    var counter_question = $(".loop-question").length;
    function tambah_question(){
        counter_question++;
        var konten = '<div class="row border-top border-bottom loop-question" id="loop_question_'+counter_question+'">'+
                        '<div class="col-md-12">'+
                            '<div class="form-group">'+
                                '<label><strong>Question</strong></label>'+
                                '<input type="hidden" name="question[]" value="'+counter_question+'">'+
                                '<input type="hidden" name="survey_question_id_'+counter_question+'" value="0">'+
                                '<input type="text" class="form-control" name="question_'+counter_question+'" id="question_'+counter_question+'" placeholder="Enter Question">'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label><strong>Answer Type</strong></label>'+
                                '<select name="answer_type_'+counter_question+'" class="form-control answer-type" data-id="'+counter_question+'">'+
                                    '<option value="input">INPUT TEXT</option>'+
                                    '<option value="textarea">TEXTAREA</option>'+
                                    '<option value="checkbox">CHECKBOX</option>'+
                                    '<option value="radio">RADIO BUTTON</option>'+
                                '</select>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<button type="button" class="btn btn-danger btn-sm" onclick="hapus_question('+counter_question+')">'+
                                    '<i class="fa fa-trash"></i> Delete Question'+
                                '</button>'+
                            '</div>'+
                            '<div class="form-group" id="container_answer_'+counter_question+'"></div>'+
                        '</div>'+
                    '</div>';
        $("#container_question").append(konten);
        document.getElementById("question_" + counter_question).focus();
    }

    function hapus_question(counter_question){
        var total_question = $(".loop-question").length;
        if(total_question == 1){
            swal("Sorry!", "Minimum must one question!", "warning");
        } else {
            $("#loop_question_" + counter_question).remove();
        }
    }
</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donation_title" name="donation_title" value="<?php echo (set_value('donation_title') ? set_value('donation_title') : $edit['donation_title']);?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('donation_title', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_description">
                                        <strong>Description</strong>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="donation_description" id="donation_description" class="form-control" required="required"><?php echo (set_value('donation_description') ? set_value('donation_description') : $edit['donation_description']);?></textarea>
                                        <?php echo form_error('donation_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_cover">
                                        <strong>Cover</strong>
                                    </label>
                                    <div>
                                        <input type="file" id="donation_cover" name="donation_cover" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" name="donation_cover_old" value="<?php echo $edit['donation_cover'];?>">

                                        <?php
                                        $path = './uploads/donasi/thumbs/';
                                        if($edit['donation_cover'] && file_exists($path . $edit['donation_cover'])){
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/donasi/thumbs/' . $edit['donation_cover']).'" class="img-thumbnail"></div>';
                                        } else {
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('assets/img/default.jpg').'" class="img-thumbnail" style="width:160px;"></div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Publish</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" <?php echo ($edit['publish'] == 'Y' ? 'checked' : '');?>> Yes
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T"  <?php echo ($edit['publish'] == 'T' ? 'checked' : '');?> > No
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/dashboard');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
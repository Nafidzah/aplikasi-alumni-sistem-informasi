<div class="container-fluid">

    <!-- UNTUK KONTEN DAFTAR SURVEI -->
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <div class="row">

                         <div class="col-md-6 col-xs-12">
                            <h6 class="m-0 font-weight-bold text-secondary">
                                List of Survey
                            </h6>
                        </div>
                        
                        <?php if($role_id == 1){?>
                        <div class="col-md-6 col-xs-12">
                            <!-- TAMPILAN DESKTOP -->
                            <a href="<?php echo base_url('backoffice/dashboard/tambah-survei');?>" class="btn btn-dark float-right d-none d-lg-block d-xl-block">
                                <i class="fas fa-plus"></i> Add Survey
                            </a>

                            <!-- TAMPILAN MOBILE -->
                            <a href="<?php echo base_url('backoffice/dashboard/tambah-survei');?>" class="btn btn-dark btn-block d-lg-none d-xl-none mt-2">
                                <i class="fas fa-plus"></i> Add Survey
                            </a>

                        </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="myTableSurvei" class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Start Period</th>
                                            <th>End Period</th>
                                            <th>Respondent</th>
                                            <th>Publish</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- END: UNTUK KONTEN DAFTAR SURVEI -->


    <!-- START: UNTUK KONTEN DAFTAR DONASI -->
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <div class="row">

                         <div class="col-md-6 col-xs-12">
                            <h6 class="m-0 font-weight-bold text-secondary">
                                List of Donation
                            </h6>
                        </div>
                        
                        <?php if($role_id == 1){?>
                        <div class="col-md-6 col-xs-12">
                            <!-- TAMPILAN DESKTOP -->
                            <a href="<?php echo base_url('backoffice/dashboard/tambah-donasi');?>" class="btn btn-dark float-right d-none d-lg-block d-xl-block">
                                <i class="fas fa-plus"></i> Add Donation
                            </a>

                            <!-- TAMPILAN MOBILE -->
                            <a href="<?php echo base_url('backoffice/dashboard/tambah-donasi');?>" class="btn btn-dark btn-block d-lg-none d-xl-none mt-2">
                                <i class="fas fa-plus"></i> Add Donation
                            </a>

                        </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="myTableDonasi" class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Cover</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Publish</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: UNTUK KONTEN DAFTAR DONASI -->

</div>

<script>
    var myTableDonasi;
    var myTableSurvei;
    $(document).ready(function() {

        myTableDonasi = $('#myTableDonasi').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [[2, 'asc']], 
            "oLanguage": {
               "sSearch": "Pencarian",
            },
            "ajax": {
                "url": "<?php echo base_url('backoffice/dashboard/ajax_list_donasi')?>",
                "type": "POST"
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ],
                    "orderable": false, 
                    "width": 50,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 1 ],
                    "width": 160,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 2 ],
                    "width": 200,
                },
                { 
                    "targets": [ 4 ],
                    "width": 80,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 5 ],
                    "orderable": false, 
                    "width": 90,
                    "className": 'text-center'
                },
            ],
            drawCallback: function () {
                $('.pagination').addClass('pagination-sm');
            }
        });

        $('#myTableDonasi').delegate('.btn-hapus', 'click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            swal({
                    title: "Confirm Delete Data",
                    text: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $.get(url, function(data, status){
                            swal('Success!', 'Data has been deleted!', 'success');
                            myTableDonasi.ajax.reload();
                        });
                    } else {
                        swal("Canceled!", "Delete canceled!", "error");
                    }
                });
        });

        myTableSurvei = $('#myTableSurvei').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [[1, 'asc']], 
            "oLanguage": {
               "sSearch": "Pencarian",
            },
            "ajax": {
                "url": "<?php echo base_url('backoffice/dashboard/ajax_list_survei')?>",
                "type": "POST"
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ],
                    "orderable": false, 
                    "width": 50,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 2 ],
                    "width": 120,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 3 ],
                    "width": 120,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 4 ],
                    "width": 180,
                },
                { 
                    "targets": [ 5 ],
                    "width": 80,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 6 ],
                    "orderable": false, 
                    "width": 90,
                    "className": 'text-center'
                },
            ],
            drawCallback: function () {
                $('.pagination').addClass('pagination-sm');
            }
        });

        $('#myTableSurvei').delegate('.btn-hapus', 'click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            swal({
                    title: "Confirm Delete Data",
                    text: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $.get(url, function(data, status){
                            swal('Success!', 'Data has been deleted!', 'success');
                            myTableSurvei.ajax.reload();
                        });
                    } else {
                        swal("Canceled!", "Delete canceled!", "error");
                    }
                });
        });

    });
   
</script>
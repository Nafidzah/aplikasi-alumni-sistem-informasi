<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donation_title" name="donation_title" value="<?php echo set_value('donation_title');?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('donation_title', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_description">
                                        <strong>Description</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="donation_description" id="donation_description" class="form-control" required="required"><?php echo set_value('donation_description');?></textarea>
                                        <?php echo form_error('donation_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_cover">
                                        <strong>Cover</strong>  <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="file" id="donation_cover" name="donation_cover" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Publish</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" checked> Yes
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T"> No
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/dashboard');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
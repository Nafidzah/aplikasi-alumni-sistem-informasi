<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div>
                                        <h4><?php echo $edit['donation_title'];?></h4>
                                        <?php
                                        $path = './uploads/donasi/thumbs/';
                                        if($edit['donation_cover'] && file_exists($path . $edit['donation_cover'])){
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/donasi/thumbs/' . $edit['donation_cover']).'" class="img-thumbnail"></div>';
                                        }
                                        ?>
                                        <p class="mt-2">
                                            <?php echo nl2br($edit['donation_description']);?>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="created_by">
                                        <strong>Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donor_name" name="donor_name" value="<?php echo $this->session->userdata('fullname');?>" class="form-control" placeholder="......" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_category_id">
                                        <strong>Contribution Type</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <select name="donation_category_id" class="form-control">
                                            <option value="">- Choose -</option>
                                            <?php
                                                foreach($donation_category as $row){
                                                    $selected = ($row['id'] == set_value('donation_category_id') ? 'selected' : '');
                                                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['donation_category'].'</option>';
                                                }
                                            ?>
                                        </select>           
                                        <?php echo form_error('donation_category_id', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_description">
                                        <strong>Description</strong>
                                    </label>
                                    <div>
                                        <textarea placeholder=".........." rows="4" name="donor_description" id="donor_description" class="form-control"><?php echo set_value('donor_description');?></textarea>
                                        <?php echo form_error('donor_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_evidence">
                                        <strong>Evidence</strong>
                                    </label>
                                    <div>
                                        <input type="file" id="donor_evidence" name="donor_evidence" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/forum');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
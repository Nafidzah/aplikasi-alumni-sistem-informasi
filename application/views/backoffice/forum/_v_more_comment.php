<div class="card card-forum mb-4">
    <div class="card-header bg-white pb-0" style="border-bottom: none;">
        <div class="media mb-3">
            <img src="http://localhost/alumni/assets/img/default.jpg" alt="John Doe" class="mr-3 mt-0 rounded-circle" style="width:24px;">
            Administrator
        </div> 
    </div>
    <div class="card-body px-4 pt-0">
        Dropdown menus can be placed in the card header in order to extend the functionality of a basic card. 
        In this dropdown card example, the Font Awesome vertical ellipsis icon in the card header can 
        be clicked on in order to toggle a dropdown menu.
        
        <div class="row">
            <div class="col-12 col-sm-6">
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fas fa-heart text-danger"></i>
                </a>
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fas fa-comment"></i>
                </a>
            </div>
            <div class="col-12 col-sm-6">
                <div class="float-sm-right">
                    <small><label class="mr-3">1001 Likes</label> <label>109 Comments</small>
                </div>                            
            </div>
        </div>
    </div>
    <div class="card-footer bg-white">
        <div class="media mb-3">
            <img src="<?php echo $url_gambar;?>" alt="<?php echo $fullname;?>" title="<?php echo $fullname;?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
            <div class="media-body border-bottom pb-4">
                Dropdown menus can be placed in the card header in order to extend the functionality of a basic card. 
                In this dropdown card example, the Font Awesome vertical ellipsis icon in the card header can 
                be clicked on in order to toggle a dropdown menu.
            </div>
        </div> 
        <div class="media mb-3">
            <img src="<?php echo $url_gambar;?>" alt="<?php echo $fullname;?>" title="<?php echo $fullname;?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
            <div class="media-body">
                <textarea name="" class="form-control" placeholder="Write something here......" rows="3"></textarea>
                <button type="button" class="btn btn-dark btn-sm mt-2">
                    Comment Now <i class="fas fa-fw fa-long-arrow-alt-right"></i>
                </button>
            </div>
        </div> 
    </div>
</div>
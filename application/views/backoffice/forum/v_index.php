<?php
// SET AVATAR FOTO
$path = './uploads/profil/';
if(file_exists($path . $foto) && $foto){
    $url_gambar = base_url('uploads/profil/' . $foto);
} else {
    $url_gambar = base_url('assets/img/default.jpg');
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-10 mx-auto mx-sm-auto">
            <form id="form_main_forum" enctype="multipart/form-data" method="post">
                <div class="card card-forum mb-4">
                    <div class="card-body pb-0">
                        <div class="media mb-3">
                            <img src="<?php echo $url_gambar;?>" alt="<?php echo $fullname;?>" title="<?php echo $fullname;?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
                            <div class="media-body">
                                <textarea name="forum_description" id="forum_description" class="form-control" placeholder="Write something here......" rows="3"></textarea>
                            </div>
                        </div> 
                    </div>
                    <div class="card-footer bg-white border-0 py-0">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-sm" id="btn_upload_file">
                                        <input type="file" id="input_upload_file" name="forum_cover">
                                        <i class="fas fa-2x fa-camera"></i>
                                    </button>
                                </div>      
                                <div class="form-group konten_upload_file">
                                    <img src="" id="img_upload_file" style="max-width: 240px;display: none;">
                                </div>
                                <div class="form-group konten_upload_file">
                                    <button type="button" class="btn btn-danger btn-sm" id="delete_upload_file">
                                        <i class="fas fa-fw fa-trash-alt"></i> Delete
                                    </button>
                                </div>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-dark float-right btn-sm btn-submit-status">
                                    Send <i class="fas fa-fw fa-long-arrow-alt-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div id="container_status"></div>

            <div class="text-center mb-5 mt-5">
                <button type="button" class="btn btn-dark btn-load-more">
                    LOAD MORE
                </button>
            </div>

        </div>         
    </div>    
</div>

<style type="text/css">
    .card-forum{
        border: 2px solid #000000;
    }
    .card-forum textarea{
        border: none !important;
    }
    .konten_upload_file{
        display: none;
    }
    button#btn_upload_file {
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }
    input#input_upload_file {
        cursor: pointer;
        position: absolute;
        font-size: 50px;
        opacity: 0;
        right: 0;
        top: 0;
    }    
</style>

<script>
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN FOTO YANG BARU DI BROWSE
    | -------------------------------------------------------------------
    */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(".konten_upload_file").show();
                $("#img_upload_file").show();
                $('#img_upload_file').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input_upload_file").change(function() {
        readURL(this);
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK HAPUS FOTO YANG BARU DI BROWSE
    | -------------------------------------------------------------------
    */
    $("#delete_upload_file").click(function() {
        remove_field_upload();
    });

    function remove_field_upload(){
        $(".konten_upload_file").hide();
        $("#img_upload_file").hide();
        $('#img_upload_file').attr('src', '');
        $('#input_upload_file').val('');
    }

    /*
    | -------------------------------------------------------------------
    | SUBMIT STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-submit-status",function() {
        if($.trim($("#forum_description").val())== ''){
            document.getElementById("forum_description").focus();
            swal("Sorry", "Post Content is required!", "warning");
            return false;
        }

        $.ajax({
            url: "<?php echo site_url('backoffice/forum/ajax_post_status');?>",
            type: "POST",
            data: new FormData($('#form_main_forum')[0]),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    remove_field_upload();
                    $("#forum_description").val("");

                    ajax_latest_post();
                } else {
                    swal("Error!", "Data could not be saved!", "error");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | SUBMIT KOMENTAR
    | -------------------------------------------------------------------
    */

    /*
    | -------------------------------------------------------------------
    | HAPUS STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-status",function() {
        var url = $(this).data('url');
        var id = $(this).data('id');
        swal({
                title: "Confirm Delete Post",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Post has been deleted!', 'success');
                        $("#status_" + id).remove();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | LATEST POST
    | -------------------------------------------------------------------
    */
    function ajax_latest_post(){
        $.ajax({
            url: "<?php echo site_url('backoffice/forum/ajax_latest_post');?>",
            type: "POST",
            data: {
                forum_id: $(".loop-status:first").data('last')
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").prepend(response.content);                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    }


    /*
    | -------------------------------------------------------------------
    | AUTOLOAD POST
    | -------------------------------------------------------------------
    */
    function ajax_automore_post(){
        $.ajax({
            url: "<?php echo site_url('backoffice/forum/ajax_automore_post');?>",
            type: "POST",
            data: {
                offset: $(".loop-status:last").data('id')
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").append(response.content);                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    }

    ajax_automore_post();

    /*
    | -------------------------------------------------------------------
    | LOAD MORE BUTTON
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-load-more",function() {
        $(".btn-load-more").html('<i class="fas fa-spinner fa-spin"></i> LOADING DATA');        
        $.ajax({
            url: "<?php echo site_url('backoffice/forum/ajax_automore_post');?>",
            type: "POST",
            data: {
                offset: $(".loop-status:last").data('id')
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").append(response.content);     
                    $(".btn-load-more").html('LOAD MORE');              
                } else {
                    $(".btn-load-more").hide();
                    $("#container_status").append('<center><small><br><br>No Post</small></center>');  
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | LIKE STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-like",function() {
        var forum_id = $(this).data('id');

        var get_url = "<?php echo base_url('backoffice/forum/ajax_like_status');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            if(data == 1){
                $(".is_like_" + forum_id).addClass('text-danger');
            } else {
                $(".is_like_" + forum_id).removeClass('text-danger');
            }
            ajax_total_like(forum_id);
        });
    });

    function ajax_total_like(forum_id){
        var get_url = "<?php echo base_url('backoffice/forum/ajax_total_like');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            $("#total_like_" + forum_id).html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | COMMENT STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-comment",function() {
        var forum_id = $(this).data('id');
        document.getElementById("forum_description_komentar_" + forum_id).focus();
    });

    $(document).on("click",".btn-submit-comment",function() {
        var forum_id = $(this).data('id');
        if($.trim($("#forum_description_komentar_" + forum_id).val())== ''){
            document.getElementById("forum_description_komentar_" + forum_id).focus();
            swal("Sorry", "Comment is required!", "warning");
            return false;
        }

        var forum_description   = $("#forum_description_komentar_" + forum_id).val();
        var post_url            = "<?php echo base_url('backoffice/forum/ajax_post_comment');?>";
        var post_data           = {forum_id:forum_id, forum_description:forum_description};
        $.post(post_url, post_data, function(data, status){
            $("#forum_description_komentar_" + forum_id).val("");
            $("#total_comment_" + forum_id).html(data);
            ajax_list_comment(forum_id);
        });
    });

    function ajax_list_comment(forum_id){
        var get_url = "<?php echo base_url('backoffice/forum/ajax_list_comment');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            $("#media_comment_" + forum_id).html(data);
        });
    }
    
    /*
    | -------------------------------------------------------------------
    | HAPUS COMMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-comment",function() {
        var url = $(this).data('url');
        var id = $(this).data('id');
        swal({
                title: "Confirm Delete Post",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Comment has been deleted!', 'success');
                        ajax_list_comment(id);
                        $("#total_comment_" + id).html(data);
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

</script>
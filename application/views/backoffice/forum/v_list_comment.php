<?php
$path_foto_profil = './uploads/profil/';
foreach($komentar as $row_komentar):
    // FOTO UNTUK PROFIL YANG TELAH KOMEN
    if(file_exists($path_foto_profil . $row_komentar['foto']) && $row_komentar['foto']){
        $url_profil_telah_komentar = base_url('uploads/profil/' . $row_komentar['foto']);
    } else {
        $url_profil_telah_komentar = base_url('assets/img/default.jpg');
    }
?>
    <div class="media mb-3" id="komentar_<?php echo $row_komentar['id'];?>">
        <img src="<?php echo $url_profil_telah_komentar;?>" alt="<?php echo $row_komentar['created_by'];?>" title="<?php echo $row_komentar['created_by'];?>" class="mr-3 mt-0 rounded-circle" style="width:24px;">
        <div class="media-body border-bottom pb-4">
            <div class="row">
                <div class="col-11">
                    <div>
                        <strong><?php echo $row_komentar['created_by'];?></strong>
                        <div>
                            <small style="font-size: 10px;"><?php echo date('d M Y, H:i:s', strtotime($row_komentar['created_at']));?> WIB</small>
                        </div>
                    </div>
                    <?php echo nl2br($row_komentar['forum_description']);?>
                </div>
                <div class="col-1">
                    <?php if($role_id == 1 OR $users_id == $row_komentar['created_by_id']){?>
                        
                        <a class="float-right btn-hapus-comment mr-3" data-id="<?php echo $row_komentar['forum_id_parent'];?>" data-url="<?php echo base_url('backoffice/forum/ajax_status_hapus/' . $row_komentar['id'] . '/' . $row_komentar['forum_id_parent']);?>" href="javascript:;" title="Delete This Post!">
                            <i class="fas fa-ellipsis-h"></i>
                        </a>
                    
                    <?php } ?>
                </div> 
            </div>
        </div>
    </div> 
<?php endforeach; ?>
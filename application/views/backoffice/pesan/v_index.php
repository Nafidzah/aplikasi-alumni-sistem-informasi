<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-secondary"><?php echo $title;?></h6>
                </div>
                <div class="card-body">
                     <div class="row">
                        <div class="col-xl-4 col-md-6 col-6">
                            <div class="new-cust-card">
                                <div class="card-header bg-white">
                                    <input type="text" id="search" class="form-control" placeholder="Search Name..." autocomplete="off">
                                </div>
                                <div class="cust-scroll ps ps--active-y" style="height:400px;position:relative;">
                                    <div class="card-body p-b-0" id="container-contact"></div>
                                    <input type="hidden" id="active">
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-6 col-6">
                            <div class="chat-card">
                                <div class="card-body">
                                    <div id="container-chat">
                                        <center>
                                            <img class="p-t-50 img-fluid d-none d-sm-block" src="<?php echo base_url('assets/img/hey-chat-illustration.png')?>" alt="" style="width:480px;">
                                            <br>
                                            PILIH DAFTAR KONTAK UNTUK MEMULAI PERCAKAPAN
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">    
    .list-kontak.active{
        background: #EEEEEE;
    }    
    .chat-card .media{
        margin-top:15px;
        margin-bottom:15px
    }
    .chat-card .msg{
        padding:3px 7px;
        margin:5px 0;
        display:inline-block
    }
    .chat-card .chat-saprator{
        position:relative;
        text-align:center
    }
    .chat-card .chat-saprator:before{
        content:"";
        position:absolute;
        top:calc(50% - 1px);
        left:0;
        background-color:#e2e5e8;
        height:2px;
        width:100%;
        z-index:1
    }
    .chat-card .chat-saprator span{
        background-color:#fff;
        color:#4099ff;
        z-index:99;
        position:relative;
        padding:0 5px;
        font-style:italic
    }
    .chat-card .received-chat .msg{
        background-color:#d9ebff
    }
    .chat-card .received-chat .msg {
        background: #d9ebff;
        border-radius: 0 5px 5px 5px;
    }

    .chat-card .send-chat,.chat-card .widget-chat-box .receive-chat,.widget-chat-box .chat-card .receive-chat{
        text-align:right
    }
    .chat-card .send-chat .msg,.chat-card .widget-chat-box .receive-chat .msg,.widget-chat-box .chat-card .receive-chat .msg{
        text-align:left;
        background-color:#fff
    }
    .chat-card .chat-control{
        border-top:1px solid #e2e5e8;
        position:relative
    }
    .chat-card .chat-control .chat-form .form-control{
        border:none;
        margin:15px 0
    }
    .chat-card .chat-control .chat-form .form-control:active,.chat-card .chat-control .chat-form .form-control:focus,.chat-card .chat-control .chat-form .form-control:hover{
        outline:none;
        border:none
    }
    .chat-card .chat-control .chat-form .chat-btn{
        position:absolute;
        right:25px;
        top:22px
    }
    .chat-card .send-chat .msg:after, .chat-card .widget-chat-box .receive-chat .msg:after, .widget-chat-box .chat-card .receive-chat .msg:after {
        content: "";
        position: absolute;
        right: -7px;
        top: -7px;
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        border: 7px solid transparent;
        border-right-color: #4099ff;
    }
    .chat-card .send-chat .msg, .chat-card .widget-chat-box .receive-chat .msg, .widget-chat-box .chat-card .receive-chat .msg {
        background: #4099ff;
        color: #fff;
        border-radius: 5px 0 5px 5px;
    }
    .chat-card .received-chat .msg:after {
        content: "";
        position: absolute;
        left: -7px;
        top: -7px;
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        border: 7px solid transparent;
        border-bottom-color: #d9ebff;
    }
    .cust-scroll, .chat1-scroll{
        overflow-x: hidden;
        overflow-y: auto;
    }
</style>    
<script>

    // UNTUK TOMBOL PESAN SEBELUMNYA
    $(document).on("click",".btn-more",function() {
        $(".btn-more").hide();
        $.ajax({
            url : "<?php echo base_url('backoffice/pesan/ajax_chat');?>",
            type : "POST",
            data : {
                id_user_terima: $("#id_user_terima").val(),
                offset: $(".loop-chat:first").data('id'),
                more: "1"
            },
            success: function(response){
                if(response){
                    $(".chat1-scroll").prepend(response);
                    $(".btn-more").show();
                } else {
                    $(".btn-more").hide();
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });

    // UNTUK PENCARIAN USER
    $(document).on("keyup","#search",function() {
        ajax_user();
    });

    // JIKA AUTO MESSAGE
    function set_aktif(id_user_terima){
        $(".list-kontak").removeClass('active');
        ajax_chat(id_user_terima);
        $(".list-kontak").addClass('active');
    }

    <?php if($this->input->get('chat_to')){?>
    
    set_aktif(<?php echo $this->input->get('chat_to');?>);

    <?php } ?>

    // UNTUK BERALIH KE KONTAK TUJUAN
    $(document).on("click",".list-kontak",function() {
        $(".list-kontak").removeClass('active');
        ajax_chat($(this).data('id'));
        $(this).addClass('active');
    });

    // UNTUK MENAMPILKAN USER KONTAK
    function ajax_user(){
        $.ajax({
            url : "<?php echo base_url('backoffice/pesan/ajax_user');?>",
            type : "POST",
            data : {
                search: $("#search").val(),
                active : $("#active").val()
            },
            success: function(response){
                $("#container-contact").html(response);
                var running_user = setTimeout("ajax_user()", 5000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    ajax_user();

    function ajax_chat(id_user_terima){
        $("#active").val(id_user_terima);
        $.ajax({
            url : "<?php echo base_url('backoffice/pesan/ajax_chat');?>",
            type : "POST",
            data : {
                id_user_terima: id_user_terima,
            },
            success: function(response){
                $("#container-chat").html(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    // KETIKA USER MENEKAN TOMBOL ENTER UNTUK MENGIRIM PESAN
    function process(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13 && !event.shiftKey) { //Enter keycode
            ajax_send_chat();
        }
    }       

    // KIRIM PESAN KE SERVER
    function ajax_send_chat(){
        var id_user_terima = $("#id_user_terima").val();
        $.ajax({
            url : "<?php echo base_url('backoffice/pesan/ajax_send_chat');?>",
            type : "POST",
            data : {
                chat_message: $("#chat_message").val(),
                id_user_terima: $("#id_user_terima").val()
            },
            success: function(response){
                ajax_chat(id_user_terima);
                $("#chat_message").val("");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    } 

    // AMBIL DATA PESAN TERBARU
    function ajax_latest_chat(){
        $.ajax({
            url : "<?php echo base_url('backoffice/pesan/ajax_latest_chat');?>",
            type : "POST",
            data : {
                id_user_terima: $("#id_user_terima").val(),
                waktu_kirim: $(".loop-chat:last").data('info')
            },
            success: function(response){
                $(".chat1-scroll").append(response);
                var running_chat = setTimeout("ajax_latest_chat()", 5000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    ajax_latest_chat();
</script>
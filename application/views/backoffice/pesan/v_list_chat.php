<?php
$default_avatar = base_url('assets/img/default.jpg');
?>
<?php if($total_chat < $total_all){?>
<div class="text-center">
    <button type="button" class="btn btn-outline-secondary m-b-50 btn-more">
        LIHAT CHAT SEBELUMNYA
    </button>
</div>  

<?php } ?>
<div class="chat1-scroll ps ps--active-x ps--active-y" style="height:400px;position:relative;">
    <?php
    if($data_chat){
        $offset = $total_chat;
        
        foreach($data_chat as $row){

            if($row['waktu_baca'] == '0000-00-00 00:00:00' && $id_user_kirim != $row['id_user_kirim']){
                // UPDATE WAKTU BACA
                $sql = "UPDATE tbl_chat
                            SET waktu_baca='".date('Y-m-d H:i:s')."'
                        WHERE id_chat = '".$row['id_chat']."'
                        ";
                $result = $this->db->query($sql);
            }

            // PENGIRIM
            if($id_user_kirim == $row['id_user_kirim']){

                $dibaca = '<i class="fas fa-check m-l-5 read-status" title="Terkirim"></i>';
                if($row['waktu_baca'] != '0000-00-00 00:00:00'){
                    $dibaca = '<i class="fas fa-check-double m-l-5 text-warning read-status" title="Sudah Dibaca"></i>';
                }

                echo '
                    <div class="m-b-20 send-chat loop-chat" data-id="'.$offset.'" data-info="'.$row['waktu_kirim'].'">
                        <div class="col">
                            <div class="msg">
                                <p class="m-b-0">
                                    '.nl2br($row['chat']).'
                                </p>
                                <div class="float-right">
                                    <small>
                                        '.date('d/m/Y H:i:s', strtotime($row['waktu_kirim'])).' '.$dibaca.'
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto p-l-0"></div>
                    </div>
                ';
            } 
            // PENERIMA
            else {
                echo '
                    <div class="m-b-20 received-chat loop-chat" data-id="'.$offset.'" data-info="'.$row['waktu_kirim'].'">
                    <div class="col-auto p-r-0"></div>
                        <div class="col">
                            <div class="msg">
                                <p class="m-b-0">'.nl2br($row['chat']).'</p>
                                <div class="float-right">
                                    <small>
                                        '.date('d/m/Y H:i:s', strtotime($row['waktu_kirim'])).'
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
    } else {
        echo '<div class="text-center m-t-50">MULAI CHAT DENGAN TEMAN ANDA</div>';
    }
    ?>
    <div>
        <input type="hidden" id="id_user_terima" value="<?php echo $id_user_terima;?>">
    </div>
</div>
<div class="right-icon-control m-t-15">
    <textarea id="chat_message" class="form-control" placeholder="Enter Message..." onkeypress="process(event, this)"></textarea>
</div>

<script>
     $(document).ready(function() {

        $('.chat1-scroll').scrollTop($('.chat1-scroll')[0].scrollHeight);

        $("#chat_message").focus();
    });
</script>
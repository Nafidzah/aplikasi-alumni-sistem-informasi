<?php
$default_foto = base_url('assets/img/default.jpg');
$path           = './uploads/profil/';

if($total_user > 0){
    foreach($result_user->result() as $row){

        if(file_exists($path . $row->foto) && $row->foto){
            $foto = base_url() . 'uploads/profil/' . $row->foto;
        } else {
            $foto = $default_foto;
        }

        // AMBIL PESAN TERAKHIR
        $sql = "SELECT t1.*
                FROM tbl_chat as t1
                WHERE 
                    (t1.id_user_kirim = '".$ses_id_user."' AND t1.id_user_terima = '".$row->id."')
                    OR
                    (t1.id_user_kirim = '".$row->id."' AND t1.id_user_terima = '".$ses_id_user."')
                ORDER BY t1.waktu_kirim DESC
                LIMIT 0, 1
                ";
        $result_chat    = $this->db->query($sql);
        $total_chat     = $result_chat->num_rows();
        $row_chat       = $result_chat->row();

        echo '
            <div class="align-middle px-0 py-3 border-bottom list-kontak '.($active == $row->id ? 'active' : '').'" data-id="'.$row->id.'"  style="cursor:pointer;">
                <img src="'.$foto.'" alt="'.$row->fullname.'" class="rounded-circle align-top mr-2" style="width:40px;">
                <div class="d-inline-block">
                    <h6><strong>'.$row->fullname.'</strong></h6>';

                    if($total_chat > 0){
                        echo '<p class="m-b-0">'.nl2br(substr($row_chat->chat, 0, 25)).'</p>';

                        if($row_chat->id_user_kirim != $ses_id_user && $row_chat->waktu_baca == '0000-00-00 00:00:00'){
                            echo '<span class="status active mr-5" title="Belum Dibaca"></span>';
                        }
                    }

            echo '</div>
            </div>
            ';
    }
} else {
    echo '<div class="text-center">TIDAK ADA DATA</div>';
}
?>
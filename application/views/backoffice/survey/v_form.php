<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url(); ?>" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <h4><?php echo $edit['survey_name']; ?></h4>
                                    <hr>
                                    <div>
                                        <?php echo nl2br($edit['survey_description']); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="container_question">
                                        <?php
                                        $nomor = 0;
                                        foreach ($question as $row) {
                                            $nomor++;
                                            echo '<div class="row border-bottom loop-question pt-3">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                <strong>' . $nomor . '. ' . $row['question'] . '</strong>
                                                            </label>
                                                            <input type="hidden" name="survey_question[]" value="' . $row['id'] . '">
                                                            <input type="hidden" name="answer_type_' . $row['id'] . '" value="' . $row['answer_type'] . '">
                                                        </div>
                                                        <div class="form-group">';

                                            if ($row['answer_type'] == 'radio') {
                                                echo '<div>';

                                                $answer = explode(',', $row['answer']);

                                                foreach ($answer as $key => $value) :
                                                    $value = str_replace(';', ',', $value);
                                                    echo '<div class="mt-2 row">
                                                                                    <div class="col-12">
                                                                                        <div class="form-check pt-2">
                                                                                            <label class="form-check-label">
                                                                                                <input type="radio" class="form-check-input" name="answer_result_' . $row['id'] . '" value="' . $value . '"> ' . $value . '
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>';
                                                endforeach;
                                                echo '</div>';
                                            } else if ($row['answer_type'] == 'checkbox') {
                                                echo '<div>';

                                                $answer = explode(',', $row['answer']);

                                                foreach ($answer as $key => $value) :
                                                    $value = str_replace(';', ',', $value);
                                                    echo '<div class="mt-2 row">
                                                                                    <div class="col-12">
                                                                                        <div class="form-check pt-2">
                                                                                            <label class="form-check-label">
                                                                                                <input type="checkbox" class="form-check-input" name="answer_result_' . $row['id'] . '[]" value="' . $value . '">' . $value . '
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>';
                                                endforeach;

                                                echo '</div>';
                                            } else if ($row['answer_type'] == 'input') {
                                                echo '<input type="text" class="form-control" name="answer_result_' . $row['id'] . '" required>';
                                            } else if ($row['answer_type'] == 'textarea') {
                                                echo '<textarea class="form-control" name="answer_result_' . $row['id'] . '" required></textarea>';
                                            }

                                            echo '</div>
                                                    </div>
                                                </div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-check"></i> Submit Survey
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        function load_unseen_notification(view = '') {
            $.ajax({
                url: "fetch.php",
                method: "POST",
                data: {
                    view: view
                },
                dataType: "json",
                success: function(data) {
                    $('.dropdown-menu').html(data.notification);
                    if (data.unseen_notification > 0) {
                        $('.count').html(data.unseen_notification);
                    }
                }
            });
        }

        load_unseen_notification();

        $('#comment_form').on('submit', function(event) {
            event.preventDefault();
            if ($('#subject').val() != '' && $('#comment').val() != '') {
                var form_data = $(this).serialize();
                $.ajax({
                    url: "insert.php",
                    method: "POST",
                    data: form_data,
                    success: function(data) {
                        $('#comment_form')[0].reset();
                        load_unseen_notification();
                    }
                });
            } else {
                alert("Both Fields are Required");
            }
        });

        $(document).on('click', '.dropdown-toggle', function() {
            $('.count').html('');
            load_unseen_notification('yes');
        });

        setInterval(function() {
            load_unseen_notification();;
        }, 5000);

    });
</script>
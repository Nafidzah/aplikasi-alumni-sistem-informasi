<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div>
                                        <small>Posted By <strong><?php echo $edit['created_by'];?></strong> on <?php echo date('d M Y, H:i:s', strtotime($edit['created_at']));?> WIB</small>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="donation_id">
                                        <strong>Donation Name</strong>
                                    </label>
                                    <div>
                                        <?php echo $edit['donation_title'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_category_id">
                                        <strong>Contribution Type</strong>
                                    </label>
                                    <div>
                                        <?php echo $edit['donation_category'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_name">
                                        <strong>Donor Name</strong>
                                    </label>
                                    <div>
                                        <?php echo $edit['donor_name'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_description">
                                        <strong>Description</strong>
                                    </label>
                                    <div>
                                        <?php echo nl2br($edit['donor_description']); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_evidence">
                                        <strong>Evidence</strong>
                                    </label>
                                    <div>
                                        <?php
                                        $path = './uploads/donasi_evidence/';
                                        if($edit['donor_evidence'] && file_exists($path . $edit['donor_evidence'])){
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/donasi_evidence/' . $edit['donor_evidence']).'" class="img-thumbnail"></div>';
                                        } else {
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/thumbs_no_image.jpg').'" class="img-thumbnail" style="width:160px;"></div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <a href="<?php echo base_url('backoffice/donasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-times"></i> Close
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Judul Donasi</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donation_title" name="donation_title" value="<?php echo set_value('donation_title');?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('donation_title', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_description">
                                        <strong>Deskripsi Donasi</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="donation_description" id="donation_description" class="form-control" required="required"><?php echo set_value('donation_description');?></textarea>
                                        <?php echo form_error('donation_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_cover">
                                        <strong>Sampul Donasi</strong>  <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="file" id="donation_cover" name="donation_cover" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Terbitkan</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" checked> Ya
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T"  > Tidak
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Simpan
                                    </button>
                                    <a href="<?php echo base_url('backoffice/donasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Batal
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
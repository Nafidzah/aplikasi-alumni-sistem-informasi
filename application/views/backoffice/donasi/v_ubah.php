<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
<<<<<<< HEAD
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="created_by">
                                        <strong>Created By</strong>
                                    </label>
                                    <div>
                                        <input type="text" id="created_by" name="created_by" value="<?php echo $edit['created_by'];?>" class="form-control" placeholder="......" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_id">
                                        <strong>Donation Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <select name="donation_id" class="form-control">
                                            <option value="">- Choose -</option>
                                            <?php
                                                foreach($donation as $row){
                                                    $selected = ($row['id'] == $edit['donation_id'] ? 'selected' : '');
                                                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['donation_title'].'</option>';
                                                }
                                            ?>
                                        </select>           
                                        <?php echo form_error('donation_id', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_category_id">
                                        <strong>Contribution Type</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <select name="donation_category_id" class="form-control">
                                            <option value="">- Choose -</option>
                                            <?php
                                                foreach($donation_category as $row){
                                                    $selected = ($row['id'] == $edit['donation_category_id'] ? 'selected' : '');
                                                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['donation_category'].'</option>';
                                                }
                                            ?>
                                        </select>           
                                        <?php echo form_error('donation_category_id', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_name">
                                        <strong>Donor Name</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donor_name" name="donor_name" value="<?php echo (set_value('donor_name') ? set_value('donor_name') : $edit['donor_name']);?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('donor_name', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_description">
                                        <strong>Description</strong>
                                    </label>
                                    <div>
                                        <textarea placeholder=".........." rows="4" name="donor_description" id="donor_description" class="form-control"><?php echo (set_value('donor_description') ? set_value('donor_description') : $edit['donor_description']);?></textarea>
                                        <?php echo form_error('donor_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_evidence">
                                        <strong>Evidence</strong>
                                    </label>
                                    <div>
                                        <input type="file" id="donor_evidence" name="donor_evidence" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" name="donor_evidence_old" value="<?php echo $edit['donor_evidence'];?>">

                                        <?php
                                        $path = './uploads/donasi_evidence/thumbs/';
                                        if($edit['donor_evidence'] && file_exists($path . $edit['donor_evidence'])){
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/donasi_evidence/thumbs/' . $edit['donor_evidence']).'" class="img-thumbnail"></div>';
                                        } else {
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/thumbs_no_image.jpg').'" class="img-thumbnail" style="width:160px;"></div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/donasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Cancel
=======
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="donation_title">
                                        <strong>Judul Donasi</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donation_title" name="donation_title" value="<?php echo (set_value('donation_title') ? set_value('donation_title') : $edit['donation_title']);?>" class="form-control" placeholder="......" required="required">
                                        <?php echo form_error('donation_title', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_description">
                                        <strong>Deskripsi Donasi</strong>
                                    </label>
                                    <div>
                                        <textarea rows="4" name="donation_description" id="donation_description" class="form-control" required="required"><?php echo (set_value('donation_description') ? set_value('donation_description') : $edit['donation_description']);?></textarea>
                                        <?php echo form_error('donation_description', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donation_cover">
                                        <strong>Sampul Donasi</strong>
                                    </label>
                                    <div>
                                        <input type="file" id="donation_cover" name="donation_cover" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" name="donation_cover_old" value="<?php echo $edit['donation_cover'];?>">

                                        <?php
                                        $path = './uploads/donasi/thumbs/';
                                        if($edit['donation_cover'] && file_exists($path . $edit['donation_cover'])){
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/donasi/thumbs/' . $edit['donation_cover']).'" class="img-thumbnail"></div>';
                                        } else {
                                            echo '<div class="mt-3 text-center"><img src="'.base_url('assets/img/default.jpg').'" class="img-thumbnail" style="width:160px;"></div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="publish">
                                        <strong>Terbitkan</strong>
                                    </label>
                                    <div>
                                        <div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="Y" <?php echo ($edit['publish'] == 'Y' ? 'checked' : '');?>> Ya
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="publish" value="T"  <?php echo ($edit['publish'] == 'T' ? 'checked' : '');?> > Tidak
                                                </label>
                                            </div>
                                        </div>
                                        <?php echo form_error('publish', '<small class="text-danger">', '</small>');?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Simpan
                                    </button>
                                    <a href="<?php echo base_url('backoffice/donasi');?>" class="btn btn-outline-secondary">
                                         <i class="fas fa-reply"></i> Batal
>>>>>>> ead19b77838041880db2ebebd00defff23acfda7
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
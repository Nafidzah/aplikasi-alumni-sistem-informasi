<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-secondary"><?php echo $edit['survey_name'];?></h6>
                </div>
                <div class="card-body">
                    <div class="float-right">
                        <a href="<?php echo base_url('backoffice/survei');?>" title="Back to List Survey" class="mb-4 btn btn-outline-secondary btn-sm">
                            <i class="fas fa-arrow-left"></i> Back to list survey
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="chart_created_at"></div>
                        </div>
                    </div>                      
                    
                    
                    <?php
                        foreach($question as $row){
                            if($row['answer_type'] == 'radio' OR $row['answer_type'] == 'checkbox'){
                                
                                $answer         = explode(',', $row['answer']);
                                $label_chart    = "";
                                $data_chart     = "";

                                foreach($answer as $key => $value){
                                    $sql = "SELECT FIND_IN_SET('".$value."', answer_result) AS result
                                            FROM tbl_survey_result
                                            WHERE survey_question_id='".$row['id']."'
                                            AND FIND_IN_SET('".$value."', answer_result) > 0
                                            ";
                                    $total = $this->db->query($sql)->num_rows();

                                    $value = str_replace(';', ',', $value);

                                    $data_chart .= $total . ", ";
                                    $label_chart .= "'" . $value . "', ";
                                }

                    ?>

                            <div class="row justify-content-center mt-5 mb-4">
                                <div class="col-md-8">
                                    <canvas id="chart_<?php echo $row['id'];?>"></canvas>
                                </div>
                            </div>

                            <script>
                                var config = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: [<?php echo $data_chart;?>],
                                            backgroundColor: [
                                                '#4dc9f6',
                                                '#f67019',
                                                '#f53794',
                                                '#537bc4',
                                                '#acc236',
                                                '#166a8f',
                                                '#00a950',
                                                '#58595b',
                                                '#8549ba',
                                                '#c62828',

                                                '#00897b',
                                                '#827717',
                                                '#c6ff00',
                                                '#e65100',
                                                '#212121',
                                                '#3e2723',
                                                '#dd2c00',
                                                '#ffd600',
                                                '#00c853',
                                                '#0091ea',

                                                '#33691e',
                                                '#ff6f00',
                                                '#6d4c41',
                                                '#455a64',
                                                '#f57c00',
                                                '#ad1457',
                                                '#8e24aa',
                                                '#f44336',
                                                '#1565c0',
                                                '#64ffda',
                                            ],
                                            label: 'Dataset 1'
                                        }],
                                        labels: [<?php echo $label_chart;?>]
                                    },
                                    options: {
                                        title: {
                                            display: true,
                                            text: '<?php echo $row['question'];?>'
                                        },
                                        responsive: true,
                                        legend: {
                                            display: true,
                                            position: 'bottom',
                                            align: 'start'
                                        }
                                    }
                                };
                                var ctx<?php echo $row['id'];?> = document.getElementById('chart_<?php echo $row['id'];?>').getContext('2d');
                                window.myPie = new Chart(ctx<?php echo $row['id'];?>, config);
                            </script>

                    <?php
                            }
                        }
                    ?>
                    <div class="table-responsive">
                        <a href="<?php echo base_url('backoffice/survei/export/' . $edit['id'] . '/' .url_title(strtolower($edit['survey_name'])));?>" title="Download Excel" class="btn btn-success btn-flat">
                            <i class="fas fa-file-excel"></i> Export Results
                        </a>
                        <table class="table table-bordered table-striped table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width:50px;" class="text-center">No</th>
                                    <th class="text-center">Respondent Name</th>
                                    <th style="width:200px;" class="text-center">Date & Time</th>
                                    <th style="width:200px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($respondent){
                                    $nomor = 0;
                                    foreach($respondent as $row){
                                        $nomor++;
                                        $url_view_result = base_url('backoffice/survei/ajax_result/' . $row['survey_id'] . '/' . $row['created_by']);
                                        echo '<tr>
                                                <td class="text-center">'.$nomor.'</td>
                                                <td>'.$row['fullname'].'</td>
                                                <td class="text-center">'.date('d M Y, H:i:s', strtotime($row['created_at'])).' WIB</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-info btn-sm btn-view-result" data-url="'.$url_view_result.'">
                                                        <i class="fas fa-search"></i> View Result
                                                    </button>
                                                </td>
                                            </tr>';
                                    }
                                } else {
                                    echo '<tr><td colspan="10" class="text-center">No Data</td></tr>';
                                }
                                ?>
                                
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="MyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="MyModalTitle"></h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="MyModalBody"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                    <i class="fa fa-reply"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>        
<!-- END MODAL -->   

<script>
    var progress_bar = '<div class="text-center"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div>LOADING DATA</div>';

    /*
    | -------------------------------------------------------------------
    | UNTUK MODAL DETAIL HASIL SURVEI
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-view-result",function() {
        $("#MyModal").modal('show');
        $("#MyModalBody").html(progress_bar);
        $("#MyModalTitle").html('Result Survey');

        var get_url = $(this).data('url');
        $.get(get_url, function(data, status){
            $("#MyModalBody").html(data);
        });
    });

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN CHART BERDASARKAN SURVEI YANG DIINPUT BY TANGGAL
    | -------------------------------------------------------------------
    */
    var barChartData = {
        labels: [<?php echo $chart_label;?>],
        datasets: [{
            label: 'Period',
            backgroundColor: 'rgb(54, 162, 235)',
            borderColor: 'rgb(54, 162, 235)',
            borderWidth: 1,
            data: [<?php echo $chart_data;?>]
        }]
    };

    $("#chart_created_at").html('<canvas id="canvas_chart_created_at" class="chartjs-render-monitor" style="display: block; height: 100%;"></canvas>');
    var ctx = document.getElementById('canvas_chart_created_at').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Chart Respondent Entry Survey'
                },
                responsive: true,
                legend: {
                    position: 'top',
                }
            }
        });
</script>
<div class="container-fluid">

    <!-- UNTUK KONTEN DAFTAR SURVEI -->
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <div class="row">

                        <div class="col-md-6 col-xs-12">
                            <h6 class="m-0 font-weight-bold text-secondary">
                                List of Survey Result
                            </h6>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="myTableSurvei" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Start Period</th>
                                            <th>End Period</th>
                                            <th>Respondent</th>
                                            <th>Publish</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: UNTUK KONTEN DAFTAR SURVEI -->

</div>

<script>
    var myTableSurvei;
    $(document).ready(function() {

        myTableSurvei = $('#myTableSurvei').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [
                [3, 'desc']
            ],
            "oLanguage": {
                "sSearch": "Pencarian",
            },
            "ajax": {
                "url": "<?php echo base_url('backoffice/survei/ajax_list_survei_result') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                    "width": 50,
                    "className": 'text-center'
                },
                {
                    "targets": [2],
                    "width": 120,
                    "className": 'text-center'
                },
                {
                    "targets": [3],
                    "width": 120,
                    "className": 'text-center'
                },
                {
                    "targets": [4],
                    "width": 180,
                },
                {
                    "targets": [5],
                    "width": 80,
                    "className": 'text-center'
                },
                {
                    "targets": [6],
                    "width": 80,
                    "className": 'text-center'
                },
                {
                    "targets": [7],
                    "orderable": false,
                    "width": 90,
                    "className": 'text-center'
                },
            ],
            drawCallback: function() {
                $('.pagination').addClass('pagination-sm');
            }
        });

        $('#myTableSurvei').delegate('.btn-hapus', 'click', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            swal({
                    title: "Confirm Delete Data",
                    text: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.get(url, function(data, status) {
                            swal('Success!', 'Data has been deleted!', 'success');
                            myTableSurvei.ajax.reload();
                        });
                    } else {
                        swal("Canceled!", "Delete canceled!", "error");
                    }
                });
        });

    });
</script>
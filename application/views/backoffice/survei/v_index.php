<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-secondary"><?php echo $title; ?></h6>
                </div>
                <div class="card-body">
                    <?php
                    if ($survei) {
                        echo '<ul>';
                        foreach ($survei as $row) {
                            echo '<li>
                                    <a href="' . base_url('backoffice/survei/form/' . $row['id'] . '/' . url_title(strtolower($row['survey_name']))) . '/' . sha1($row['survey_name']) . '">' . $row['survey_name'] . '</a>
                            </li>';
                        }
                        echo '</ul>';
                    } else {
                        echo '<center><br><br><br><br>No Data</center>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
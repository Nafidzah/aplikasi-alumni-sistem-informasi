<div id="container_question">
    <?php
    $nomor = 0;
    foreach ($question as $row) {
        $answer_result = $this->M_survey_result->get_answer_result_by($survey_id, $created_by, $row['id']);
        $nomor++;
        echo '<div class="row border-bottom loop-question pt-3">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            <strong>' . $nomor . '. ' . $row['question'] . '</strong>
                        </label>
                        <input type="hidden" name="survey_question[]" value="' . $row['id'] . '">
                        <input type="hidden" name="answer_type_' . $row['id'] . '" value="' . $row['answer_type'] . '">
                    </div>
                    <div class="form-group">';

        if ($row['answer_type'] == 'radio') {
            echo '<div>';
            $array_answer_result = explode(',', $answer_result);
            $answer = explode(',', $row['answer']);

            foreach ($answer as $key => $value) :
                $checked = (in_array($value, $array_answer_result) ? 'checked' : '');

                $value = str_replace(';', ',', $value);

                echo '<div class="mt-2 row">
                                                <div class="col-12">
                                                    <div class="form-check pt-2">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="answer_result_' . $row['id'] . '" value="' . $value . '" ' . $checked . ' disabled> ' . $value . '
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>';
            endforeach;
            echo '</div>';
        } else if ($row['answer_type'] == 'checkbox') {
            echo '<div>';

            $array_answer_result = explode(',', $answer_result);
            $answer = explode(',', $row['answer']);

            foreach ($answer as $key => $value) :

                $checked = (in_array($value, $array_answer_result) ? 'checked' : '');

                $value = str_replace(';', ',', $value);

                echo '<div class="mt-2 row">
                                                <div class="col-12">
                                                    <div class="form-check pt-2">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input" name="answer_result_' . $row['id'] . '[]" value="' . $value . '" ' . $checked . ' disabled>' . $value . '
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>';
            endforeach;

            echo '</div>';
        } else if ($row['answer_type'] == 'input') {
            echo '<input type="text" class="form-control" name="answer_result_' . $row['id'] . '" value="' . $answer_result . '" disabled>';
        } else if ($row['answer_type'] == 'textarea') {
            echo '<textarea class="form-control" name="answer_result_' . $row['id'] . '" disabled>' . $answer_result . '</textarea>';
        }

        echo '</div>
                </div>
            </div>';
    }
    ?>
</div>
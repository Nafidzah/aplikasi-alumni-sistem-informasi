<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=".$edit['survey_name'].".xls");
?>
<!DOCTYPE html>
<html>
<head>
    <title>SURVEY RESULTS</title>
</head>
<body>
    <h3><?php echo $edit['survey_name'];?></h3>
    <table style="width: 100%;" border="2">
        <thead>
            <tr>
                <th style="width:50px;" align="center">No</th>
                <th class="text-center">Respondent Name</th>
                <th style="width:200px;"  align="center">Date & Time</th>
                <?php
                $nomor = 0;
                foreach($question as $row){
                    $nomor++;
                    echo '<th>'.$nomor.'. '.$row['question'].'</th>';
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if($respondent){
                $nomor = 0;
                foreach($respondent as $row){
                    $nomor++;
                    echo '<tr>
                            <td align="center">'.$nomor.'</td>
                            <td>'.$row['fullname'].'</td>
                            <td align="center">'.date('d M Y, H:i:s', strtotime($row['created_at'])).' WIB</td>';

                    foreach($question as $row2){
                        $answer_result = $this->M_survey_result->get_answer_result_by($row['survey_id'], $row['created_by'], $row2['id']);
                        echo '<td>'.$answer_result.'</td>';
                    }
                    echo '</tr>';
                }
            } else {
                echo '<tr><td colspan="10" class="text-center">No Data</td></tr>';
            }
            ?>
            
        </tbody>
    </table>
</body>
</html>
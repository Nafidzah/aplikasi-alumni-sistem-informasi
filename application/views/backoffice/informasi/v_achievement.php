<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Name</th>
                <th style="width:200px;" class="text-center">Year</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($achievement){
                $nomor = 1;
                foreach($achievement as $row){
                    echo '<tr>
                            <td class="text-center">
                                '.$nomor.'
                            </td>
                            <td>'.$row['achievement'].'</td>
                            <td class="text-center">'.$row['achievement_year'].'</td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="4" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
<style type="text/css">
    .card-forum{
        border: 2px solid #000000;
    }
    .card-forum textarea{
        border: none !important;
    }
    .konten_upload_file{
        display: none;
    }
    button#btn_upload_file {
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }
    input#input_upload_file {
        cursor: pointer;
        position: absolute;
        font-size: 50px;
        opacity: 0;
        right: 0;
        top: 0;
    }    
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="border-right pt-3 pb-3 row">
                                <div class="col-md-4">
                                    <?php
                                    $path = './uploads/profil/';
                                    if(file_exists($path . $edit['foto']) && $edit['foto']){
                                        $url_gambar = base_url('uploads/profil/' . $edit['foto']);
                                    } else {
                                        $url_gambar = base_url('assets/img/default.jpg');
                                    }
                                    ?>
                                    <center>
                                        <img src="<?php echo $url_gambar; ?>" alt="John Doe" class="rounded-circle img-thumbnail" style="max-width:96px;">    
                                    </center>                                    
                                    
                                </div>
                                <div class="col-md-8 justify-content-center ">
                                    <h4><small><?php echo ($edit['fullname'] ? $edit['fullname'] : '-');?></small></h4>
                                    <p><?php echo ($edit['tahun_angkatan'] ? $edit['tahun_angkatan'] : '-');?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="border-right p-3">
                                <div class="mb-2">
                                    <i class="fas fa-envelope mr-2"></i> <?php echo ($edit['email'] ? $edit['email'] : '-');?>
                                </div>
                                <div class="mb-2">
                                    <i class="fab fa-whatsapp mr-2"></i> <?php echo ($edit['nomor_hp'] ? $edit['nomor_hp'] : '-');?>
                                </div>
                                <div class="mb-2">
                                    <i class="fab fa-linkedin-in mr-2"></i> <?php echo ($edit['linkedin'] ? $edit['linkedin'] : '-');?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-3">
                                <?php echo ($edit['description'] ? nl2br($edit['description']) : '-');?>
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-tabs nav-justified mt-5">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_post">
                                <i class="fas fa-newspaper mr-2"></i> Post
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_skill">
                                <i class="fas fa-swatchbook mr-2"></i> Skill
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_achievement">
                                <i class="fas fa-medal mr-2"></i> Achievement
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_contribution">
                                <i class="fas fa-people-carry mr-2"></i> Contribution
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_experience">
                                <i class="fas fa-briefcase mr-2"></i> Experience
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane container active" id="tab_post">
                            <?php
                            // SET AVATAR FOTO
                            $path = './uploads/profil/';
                            if(file_exists($path . $foto) && $foto){
                                $url_gambar = base_url('uploads/profil/' . $foto);
                            } else {
                                $url_gambar = base_url('assets/img/default.jpg');
                            }
                            ?>
                            <div class="row mt-4">
                                <div class="col-12 col-sm-12 col-md-10 mx-auto mx-sm-auto">
                                    <div id="container_status"></div>

                                    <div class="text-center mb-5 mt-5">
                                        <button type="button" class="btn btn-dark btn-load-more">
                                            LOAD MORE
                                        </button>
                                    </div>

                                </div>         
                            </div>  
                        </div><!-- .tab_post -->
                        <div class="tab-pane container fade" id="tab_skill">
                            <div class="mt-3 mb-3" id="tampil_skill"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalSkill">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalSkillTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalSkillBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-skill">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->                
                        </div>
                        <div class="tab-pane container fade" id="tab_achievement">
                            <div class="mt-3 mb-3" id="tampil_achievement"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalAchievement">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalAchievementTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalAchievementBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-achievement">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->   
                        </div>
                        <div class="tab-pane container fade" id="tab_contribution">
                            <div class="mt-3 mb-3" id="tampil_contribution"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalContribution">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalContributionTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalContributionBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-contribution">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->   
                        </div>
                        <div class="tab-pane container fade" id="tab_experience">
                            <div class="mt-3 mb-3" id="tampil_experience"></div>    
                            <!-- MODAL -->
                            <div class="modal fade" id="MyModalExperience">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title" id="MyModalExperienceTitle"></h6>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body" id="MyModalExperienceBody"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-dark btn-simpan-experience">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">
                                                <i class="fa fa-reply"></i> Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <!-- END MODAL -->  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var progress_bar = '<div class="text-center"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div>LOADING DATA</div>';
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA SKILL
    | -------------------------------------------------------------------
    */
    function ajax_skill(){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_skill_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_skill").html(data);
        });
    }
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA ACHIEVEMENT
    | -------------------------------------------------------------------
    */
    function ajax_achievement(){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_achievement_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_achievement").html(data);
        });
    }
    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA CONTRIBUTION
    | -------------------------------------------------------------------
    */
    function ajax_contribution(){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_contribution_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_contribution").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | UNTUK MENAMPILKAN DATA EXPERIENCE
    | -------------------------------------------------------------------
    */
    function ajax_experience(){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_experience_list/' . $edit['id']);?>";
        $.get(get_url, function(data, status){
            $("#tampil_experience").html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | AUTOLOAD
    | -------------------------------------------------------------------
    */
    ajax_skill();
    ajax_achievement();
    ajax_contribution();
    ajax_experience();

    /*
    | -------------------------------------------------------------------
    | HAPUS STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-status",function() {
        var url = $(this).data('url');
        var id = $(this).data('id');
        swal({
                title: "Confirm Delete Post",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Post has been deleted!', 'success');
                        $("#status_" + id).remove();
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });

    /*
    | -------------------------------------------------------------------
    | LATEST POST
    | -------------------------------------------------------------------
    */
    function ajax_latest_post(){
        $.ajax({
            url: "<?php echo site_url('backoffice/informasi/ajax_latest_post');?>",
            type: "POST",
            data: {
                forum_id: $(".loop-status:first").data('last'),
                users_id: "<?php echo $edit['id'];?>"
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").prepend(response.content);                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    }


    /*
    | -------------------------------------------------------------------
    | AUTOLOAD POST
    | -------------------------------------------------------------------
    */
    function ajax_automore_post(){
        $.ajax({
            url: "<?php echo site_url('backoffice/informasi/ajax_automore_post');?>",
            type: "POST",
            data: {
                offset: $(".loop-status:last").data('id'),
                users_id: "<?php echo $edit['id'];?>"
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").append(response.content);                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    }

    ajax_automore_post();

    /*
    | -------------------------------------------------------------------
    | LOAD MORE BUTTON
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-load-more",function() {
        $(".btn-load-more").html('<i class="fas fa-spinner fa-spin"></i> LOADING DATA');        
        $.ajax({
            url: "<?php echo site_url('backoffice/informasi/ajax_automore_post');?>",
            type: "POST",
            data: {
                offset: $(".loop-status:last").data('id')
            },
            dataType: "JSON",
            success: function (response) {
                if(response.status){
                    $("#container_status").append(response.content);     
                    $(".btn-load-more").html('LOAD MORE');              
                } else {
                    $(".btn-load-more").hide();
                    $("#container_status").append('<center><small><br><br>No Post</small></center>');  
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    });

    /*
    | -------------------------------------------------------------------
    | LIKE STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-like",function() {
        var forum_id = $(this).data('id');

        var get_url = "<?php echo base_url('backoffice/informasi/ajax_like_status');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            if(data == 1){
                $(".is_like_" + forum_id).addClass('text-danger');
            } else {
                $(".is_like_" + forum_id).removeClass('text-danger');
            }
            ajax_total_like(forum_id);
        });
    });

    function ajax_total_like(forum_id){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_total_like');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            $("#total_like_" + forum_id).html(data);
        });
    }

    /*
    | -------------------------------------------------------------------
    | COMMENT STATUS
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-comment",function() {
        var forum_id = $(this).data('id');
        document.getElementById("forum_description_komentar_" + forum_id).focus();
    });

    $(document).on("click",".btn-submit-comment",function() {
        var forum_id = $(this).data('id');
        if($.trim($("#forum_description_komentar_" + forum_id).val())== ''){
            document.getElementById("forum_description_komentar_" + forum_id).focus();
            swal("Sorry", "Comment is required!", "warning");
            return false;
        }

        var forum_description   = $("#forum_description_komentar_" + forum_id).val();
        var post_url            = "<?php echo base_url('backoffice/informasi/ajax_post_comment');?>";
        var post_data           = {forum_id:forum_id, forum_description:forum_description};
        $.post(post_url, post_data, function(data, status){
            $("#forum_description_komentar_" + forum_id).val("");
            $("#total_comment_" + forum_id).html(data);
            ajax_list_comment(forum_id);
        });
    });

    function ajax_list_comment(forum_id){
        var get_url = "<?php echo base_url('backoffice/informasi/ajax_list_comment');?>";
        var get_data= {forum_id:forum_id};
        $.get(get_url, get_data, function(data, status){
            $("#media_comment_" + forum_id).html(data);
        });
    }
    
    /*
    | -------------------------------------------------------------------
    | HAPUS COMMENT
    | -------------------------------------------------------------------
    */
    $(document).on("click",".btn-hapus-comment",function() {
        var url = $(this).data('url');
        var id = $(this).data('id');
        swal({
                title: "Confirm Delete Post",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.get(url, function(data, status){
                        swal('Success!', 'Comment has been deleted!', 'success');
                        ajax_list_comment(id);
                        $("#total_comment_" + id).html(data);
                    });
                } else {
                    swal("Canceled!", "Delete canceled!", "error");
                }
            });
    });
</script>
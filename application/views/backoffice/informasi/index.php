<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">

                <div class="card-header">
                    <div class="row">

                         <div class="col-md-6 col-xs-12">
                            <h6 class="m-0 font-weight-bold text-secondary">
                                List of Information Systems Alumni
                            </h6>
                        </div>
                        
                        <?php if($role_id == 1){?>
                        <div class="col-md-6 col-xs-12">
                            <!-- TAMPILAN DESKTOP -->
                            <a href="<?php echo base_url('backoffice/informasi/tambah');?>" class="btn btn-dark float-right d-none d-lg-block d-xl-block">
                                <i class="fas fa-plus"></i> Add Alumni
                            </a>

                            <!-- TAMPILAN MOBILE -->
                            <a href="<?php echo base_url('backoffice/informasi/tambah');?>" class="btn btn-dark btn-block d-lg-none d-xl-none mt-2">
                                <i class="fas fa-plus"></i> Add Alumni
                            </a>

                        </div>
                        <?php } ?>

                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Alumni Name</th>
                                    <th>Angkatan</th>
                                    <th>Experience/Company</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>

                                    <?php if($role_id == 1){ ?>
                                    <th></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var myTable
    $(document).ready(function() {

        myTable = $('#myTable').DataTable({ 
            "processing": true,
            "serverSide": true,
            "order": [[1, 'asc']], 
            "oLanguage": {
               "sSearch": "Pencarian",
            },
            "ajax": {
                "url": "<?php echo base_url('backoffice/informasi/ajax_list')?>",
                "type": "POST"
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ],
                    "orderable": false, 
                    "width": 50,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 1 ],
                    "orderable": true, 
                },
                { 
                    "targets": [ 2 ],
                    "orderable": true, 
                    "width": 80,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 3 ],
                    "orderable": true, 
                    "width": 150,
                    "className": 'text-center'
                },
                { 
                    "targets": [ 4 ],
                    "orderable": true, 
                    "width": 120
                },
                { 
                    "targets": [ 5 ],
                    "orderable": true, 
                    "width": 150
                },

                <?php if($role_id == 1){ ?>
                { 
                    "targets": [ 6 ],
                    "orderable": false, 
                    "width": 150,
                    "className": 'text-center'
                },
                <?php } ?>
                
            ],
            drawCallback: function () {
                $('.pagination').addClass('pagination-sm');

                $('[data-toggle="popover"]').popover({
                    html: true
                });
                $('.popover-dismiss').popover({
                    trigger: 'focus',
                });
            }
        });

        $('#myTable').delegate('.btn-hapus', 'click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            swal({
                    title: "Konfirmasi Hapus Data",
                    text: "Apakah anda yakin ingin menghapus data ini?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: "Batal",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $.get(url, function(data, status){
                            swal('Sukses!', 'Data berhasil dihapus!', 'success');
                            myTable.ajax.reload();
                        });
                    } else {
                        swal("Batal", "Hapus data dibatalkan", "error");
                    }
                });
        });
    });
   
</script>
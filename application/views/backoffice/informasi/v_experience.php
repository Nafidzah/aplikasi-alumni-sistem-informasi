<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Name</th>
                <th style="width:100px;" class="text-center">Year</th>
                <th style="width:200px;" class="text-center">Position</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($experience){
                $nomor = 1;
                foreach($experience as $row){                    
                    echo '<tr>
                            <td class="text-center">
                                '.$nomor.'
                            </td>
                            <td>'.$row['experience'].'</td>
                            <td class="text-center">
                                '.$row['experience_year'].'
                            </td>
                            <td>'.$row['experience_position'].'</td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="5" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
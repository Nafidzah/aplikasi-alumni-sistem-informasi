<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover"  cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th style="width:50px;" class="text-center">No</th>
                <th>Name</th>
                <th style="width:100px;" class="text-center">Year</th>
                <th style="width:200px;" class="text-center">Position</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if($contribution){
                $nomor = 1;
                foreach($contribution as $row){                    
                    echo '<tr>
                            <td class="text-center">
                                '.$nomor.'
                            </td>
                            <td>'.$row['contribution'].'</td>
                            <td class="text-center">
                                '.$row['contribution_year'].'
                            </td>
                            <td>'.$row['contribution_position'].'</td>
                        </tr>';
                    $nomor++;
                }
            } else {
                echo '
                    <tr>
                        <td colspan="5" class="text-center">No Data</td>
                    </tr>';
            }
            ?>
            
        </tbody>
    </table>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Donation Category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var myTable
    $(document).ready(function() {

        myTable = $('#myTable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],
            "ajax": {
                "url": "<?php echo base_url('backoffice/master/kategori-donasi/ajax_list') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                    "width": 50,
                    "className": 'text-center'
                },
                {
                    "targets": [2],
                    "orderable": false,
                    "width": 100,
                    "className": 'text-center'
                },
            ],
            drawCallback: function() {
                $('.pagination').addClass('pagination-sm');
            }
        });

        var tambah = '<a href="<?php echo base_url('backoffice/master/kategori-donasi/tambah'); ?>" class="btn btn-dark"><i class="fas fa-plus"></i> Add Donation Category</a>';
        $('.dataTables_length').html(tambah);

        $('#myTable').delegate('.btn-hapus', 'click', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            swal({
                    title: "Confirm Delete Data",
                    text: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.get(url, function(data, status) {
                            swal('Success!', 'Data has been deleted!', 'success');
                            myTable.ajax.reload();
                        });
                    } else {
                        swal("Canceled!", "Delete canceled!", "error");
                    }
                });
        });
    });
</script>
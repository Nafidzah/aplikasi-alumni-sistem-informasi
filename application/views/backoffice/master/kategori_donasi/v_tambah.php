<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <form action="<?php echo current_url(); ?>" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="donation_category">
                                        <strong>Donation Category</strong> <span class="text-danger">*</span>
                                    </label>
                                    <div>
                                        <input type="text" id="donation_category" name="donation_category" value="<?php echo set_value('donation_category'); ?>" class="form-control" placeholder="......" autocomplete="off">
                                        <?php echo form_error('donation_category', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-dark">
                                        <i class="fas fa-save"></i> Save
                                    </button>
                                    <a href="<?php echo base_url('backoffice/master/kategori-donasi'); ?>" class="btn btn-outline-secondary">
                                        <i class="fas fa-reply"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
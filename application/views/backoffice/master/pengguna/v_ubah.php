<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
<<<<<<< HEAD
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-secondary"><?php echo $title;?></h6>
                </div>

=======
>>>>>>> Agung
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="<?php echo current_url();?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">                                    
                                    <div class="form-group">
                                        <label for="username">
                                            <strong>Username</strong> <span class="text-danger">*</span>
                                        </label>
                                        <div>
                                            <input type="text" id="username" name="username" value="<?php echo $edit['username'];?>" class="form-control" placeholder="......" readonly>
                                            <?php echo form_error('username', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">
                                            <strong>Password</strong>
                                            <br>
                                            <small class="text-info" style="font-size:12px;"><strong>ABAIKAN ISIAN PASSWORD, JIKA TIDAK INGIN DI UBAH</strong></small>
                                        </label>
                                        <div>
                                            <input type="password" id="password" name="password" value="<?php echo set_value('password');?>" class="form-control" placeholder="......">
                                            <?php echo form_error('password', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="role_id">
<<<<<<< HEAD
                                            <strong>Role Pengguna</strong> <span class="text-danger">*</span>
                                        </label>
                                        <div>
                                            <select name="role_id" id="role_id" class="form-control">
                                                <option value="">- PILIH ROLE PENGGUNA -</option>
=======
                                            <strong>User Roles</strong> <span class="text-danger">*</span>
                                        </label>
                                        <div>
                                            <select name="role_id" id="role_id" class="form-control">
                                                <option value="">- Choose Role -</option>
>>>>>>> Agung
                                                <?php
                                                foreach($role as $row){
                                                    $selected = ($edit['role_id'] == $row['id'] ? 'selected' : '');
                                                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['role'].'</option>';
                                                }
                                                ?>
                                            </select>
                                            <?php echo form_error('role_id', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">
                                            <strong>Email</strong> <span class="text-danger">*</span>
                                        </label>
                                        <div>
                                            <input type="text" id="email" name="email" value="<?php echo $edit['email'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('email', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="fullname">
<<<<<<< HEAD
                                            <strong>Nama Lengkap</strong> <span class="text-danger">*</span>
=======
                                            <strong>Fullname</strong> <span class="text-danger">*</span>
>>>>>>> Agung
                                        </label>
                                        <div>
                                            <input type="text" id="fullname" name="fullname" value="<?php echo $edit['fullname'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('fullname', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tahun_angkatan">
                                            <strong>Tahun Angkatan</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="tahun_angkatan" name="tahun_angkatan" value="<?php echo $edit['tahun_angkatan'];?>" class="form-control" placeholder="......" maxlength="4">
                                            <?php echo form_error('tahun_angkatan', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tahun_angkatan">
<<<<<<< HEAD
                                            <strong>Jenis Kelamin</strong>
=======
                                            <strong>Gender</strong>
>>>>>>> Agung
                                        </label>
                                        <div>
                                            <div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
<<<<<<< HEAD
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="L" <?php echo ($edit['jenis_kelamin'] == 'L' ? 'checked' : '');?>> Laki-laki
=======
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="L" <?php echo ($edit['jenis_kelamin'] == 'L' ? 'checked' : '');?>> Male
>>>>>>> Agung
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
<<<<<<< HEAD
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="P"  <?php echo ($edit['jenis_kelamin'] == 'P' ? 'checked' : '');?>> Perempuan
=======
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="P"  <?php echo ($edit['jenis_kelamin'] == 'P' ? 'checked' : '');?>> Female
>>>>>>> Agung
                                                    </label>
                                                </div>
                                            </div>
                                            <?php echo form_error('jenis_kelamin', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="website">
                                            <strong>Website</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="website" name="website" value="<?php echo $edit['website'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('website', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="linkedin">
                                            <strong>Linkedin</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="linkedin" name="linkedin" value="<?php echo $edit['linkedin'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('linkedin', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook">
                                            <strong>Facebook</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="facebook" name="facebook" value="<?php echo $edit['facebook'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('facebook', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook">
                                            <strong>Twitter</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="twitter" name="twitter" value="<?php echo $edit['twitter'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('twitter', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook">
                                            <strong>Instagram</strong>
                                        </label>
                                        <div>
                                            <input type="text" id="instagram" name="instagram" value="<?php echo $edit['instagram'];?>" class="form-control" placeholder="......">
                                            <?php echo form_error('instagram', '<small class="text-danger">', '</small>');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">
<<<<<<< HEAD
                                            <strong>Foto Profil</strong>
=======
                                            <strong>Photo</strong>
>>>>>>> Agung
                                        </label>
                                        <div>
                                            <input type="file" id="foto" name="foto" class="form-control" placeholder="......"  accept="image/x-png,image/gif,image/jpeg">
                                            <input type="hidden" name="foto_old" value="<?php echo $edit['foto'];?>">

                                            <?php
                                            $path = './uploads/profil/';
                                            if($edit['foto'] && file_exists($path . $edit['foto'])){
                                                echo '<div class="mt-3 text-center"><img src="'.base_url('uploads/profil/' . $edit['foto']).'" class="img-thumbnail" style="width:160px;"></div>';
                                            } else {
                                                echo '<div class="mt-3 text-center"><img src="'.base_url('assets/img/default.jpg').'" class="img-thumbnail" style="width:160px;"></div>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
<<<<<<< HEAD
                            
=======
>>>>>>> Agung
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-secondary">
<<<<<<< HEAD
                                                <i class="fas fa-save"></i> Simpan
                                            </button>
                                            <a href="<?php echo base_url('backoffice/master/pengguna');?>" class="btn btn-outline-secondary">
                                                 <i class="fas fa-reply"></i> Batal
=======
                                                <i class="fas fa-save"></i> Save
                                            </button>
                                            <a href="<?php echo base_url('backoffice/master/pengguna');?>" class="btn btn-outline-secondary">
                                                 <i class="fas fa-reply"></i> Cancel
>>>>>>> Agung
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
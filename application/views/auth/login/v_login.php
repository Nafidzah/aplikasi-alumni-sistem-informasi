<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>Login Web Administrator</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/fonts.min.css');?>" rel="stylesheet" />
        
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url('assets/css/sb-admin-2.min.css');?>" rel="stylesheet" />             
        <link href="<?php echo base_url('assets/css/custom.min.css');?>" rel="stylesheet" />
    </head>

    <body class="login">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="mt-5 ">
                                <h4 class="text-white d-none d-lg-block d-md-none">Alumni Sistem Informasi</h4>                                                                
                                <h4 class="text-white text-center d-block d-sm-none d-md-none">Alumni Sistem Informasi</h4>                                                                
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="mt-5 text-white text-right d-none d-lg-block">
                                Don't have an account? <a href="<?php echo base_url('auth/register');?>" class="text-warning">Sign Up</a>
                            </div>
                        </div>
                    </div>                    
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="py-5 px-3 h-100 d-flex justify-content-center align-items-center text-center text-secondary font-weight-bold">
                                        Forum yang berisi tentang profil dan informasi dari Alumni Sistem Informasi Telkom University
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Sign To Account!</h1>
                                        </div>
                                        <form class="user" method="post" action="<?php echo current_url();?>">

                                            <?php if($this->session->flashdata('success')):?>
                                                <div class="alert alert-success"><small><?php echo $this->session->flashdata('success'); ?></small></div>
                                            <?php endif; ?>

                                            <?php if($this->session->flashdata('error')):?>
                                                <div class="alert alert-danger"><small><?php echo $this->session->flashdata('error'); ?></small></div>
                                            <?php endif; ?>

                                            <?php echo validation_errors('<div class="alert alert-danger"><small>', '</small></div>');?>
                                            
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="param1" name="param1" placeholder="Username" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control form-control-user" id="param2" name="param2" placeholder="Password" />
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <a class="small" href="<?php echo base_url('auth/forgot-password');?>">Forgot Password?</a>
                                                </div>
                                                <div class="col-6">
                                                    <button type="submit" class="btn btn-secondary btn-user btn-block">
                                                        <i class="fas fa-sign-in-alt fa-fw"></i> Sign In
                                                    </button>
                                                </div>                                                 
                                            </div>                                            
                                            <hr />
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <a href="<?php echo base_url('auth/google');?>" class="btn btn-google btn-user btn-block">
                                                        <i class="fab fa-google fa-fw"></i> Google
                                                    </a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="<?php echo base_url('auth/linkedin');?>" class="btn btn-facebook btn-user btn-block">
                                                        <i class="fab fa-linkedin fa-fw"></i> Linkedin
                                                    </a>
                                                </div>                                                 
                                            </div> 
                                            <div class=" d-block d-sm-none d-md-none">
                                                <hr />
                                                <div class="text-center">
                                                    <a href="" class="small">Don't have an account? Sign Up</a>
                                                  </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?php echo base_url('assets/js/sb-admin-2.min.js');?>"></script>
    </body>
</html>

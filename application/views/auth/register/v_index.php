<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>Register - Alumni Sistem Informasi</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/fonts.min.css');?>" rel="stylesheet" />
        <link href="<?php echo base_url('assets/vendor/sweetalert/sweetalert.css'); ?>" rel="stylesheet" type="text/css">
        
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url('assets/css/sb-admin-2.min.css');?>" rel="stylesheet" />             
        <link href="<?php echo base_url('assets/css/custom.min.css');?>" rel="stylesheet" />
    </head>

    <body class="login">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="mt-5 ">
                                <h4 class="text-white d-none d-lg-block d-md-none">Alumni Sistem Informasi</h4>                                                                
                                <h4 class="text-white text-center d-block d-sm-none d-md-none">Alumni Sistem Informasi</h4>                                                                
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="mt-5 text-white text-right d-none d-lg-block">
                                Already have an account? <a href="<?php echo base_url('auth/login');?>" class="text-warning">Sign In</a>
                            </div>
                        </div>
                    </div>                    
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="py-5 px-3 h-100 d-flex justify-content-center align-items-center text-center text-secondary font-weight-bold">
                                        Forum yang berisi tentang profil dan informasi dari Alumni Sistem Informasi Telkom University
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Register New Member</h1>
                                        </div>
                                        <form class="user" method="post" action="<?php echo current_url();?>" id="form_register">

                                            <?php if($this->session->flashdata('success')):?>
                                                <div class="alert alert-success"><small><?php echo $this->session->flashdata('success'); ?></small></div>
                                            <?php endif; ?>

                                            <?php if($this->session->flashdata('error')):?>
                                                <div class="alert alert-danger"><small><?php echo $this->session->flashdata('error'); ?></small></div>
                                            <?php endif; ?>

                                            <?php echo validation_errors('<div class="alert alert-danger"><small>', '</small></div>');?>
                                            
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="fullname" name="fullname" value="<?php echo set_value('fullname');?>" placeholder="Nama Lengkap" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="username" name="username" value="<?php echo set_value('username');?>" placeholder="Username" />
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="email" class="form-control form-control-user" id="email" name="email" value="<?php echo set_value('email');?>" placeholder="Email" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user angka" id="tahun_angkatan" name="tahun_angkatan" value="<?php echo set_value('tahun_angkatan');?>" placeholder="Tahun Angkatan" maxlength="4" />
                                            </div>
                                            <div class="form-group text-center">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="L" <?php echo (set_value('jenis_kelamin') == "L" ? 'checked' : '' );?>> Laki-laki
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="P"  <?php echo (set_value('jenis_kelamin') == "P" ? 'checked' : '' );?>> Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user angka" id="nomor_hp" name="nomor_hp" value="<?php echo set_value('nomor_hp');?>" placeholder="Nomor Telepon" maxlength="15" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control form-control-user" id="konfirmasi_password" name="konfirmasi_password" placeholder="Konfirmasi Password" />
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6"></div>
                                                <div class="col-6">
                                                    <button type="button" class="btn btn-secondary btn-user btn-block btn-register">
                                                        <i class="fas fa-sign-in-alt fa-fw"></i> Register
                                                    </button>
                                                </div>                                          
                                            </div>     
                                            <div class=" d-block d-sm-none d-md-none">
                                                <hr />
                                                <div class="text-center">
                                                    <a href="<?php echo base_url('auth/login');?>" class="small">Already have an account? Sign In</a>
                                                  </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>
        <script src="<?php echo base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?php echo base_url('assets/js/sb-admin-2.min.js');?>"></script>

        <script>
            $(document).on("keyup", ".angka", function() {
                this.value = this.value.replace(/\D/g,'');
            });

            $(document).on("click",".btn-register",function() {
                var fullname = $.trim($("#fullname").val());
                if(fullname == ''){
                    document.getElementById("fullname").focus();
                    swal("Mohon Maaf", "Nama lengkap wajib diisi", "warning");
                    return false;
                } else if(fullname.length < 3){
                    document.getElementById("fullname").focus();
                    swal("Mohon Maaf", "Nama lengkap minimal 3 karakter", "warning");
                    return false;
                }

                var username = $.trim($("#username").val());
                if(username == ''){
                    document.getElementById("username").focus();
                    swal("Mohon Maaf", "Username wajib diisi", "warning");                    
                    return false;
                } else if(username.length < 3){
                    document.getElementById("username").focus();
                    swal("Mohon Maaf", "Username minimal 3 karakter", "warning");
                    return false;
                }

                if($.trim($("#email").val()) == ''){
                    document.getElementById("email").focus();
                    swal("Mohon Maaf", "Email wajib diisi", "warning");                    
                    return false;
                }

                if($.trim($("#tahun_angkatan").val()) == ''){
                    document.getElementById("tahun_angkatan").focus();
                    swal("Mohon Maaf", "Tahun angkatan wajib diisi", "warning");                    
                    return false;
                }

                if ($('input[name="jenis_kelamin"]:checked').length == 0) {
                    swal("Mohon Maaf", "Jenis kelamin wajib dipilih", "warning");                    
                    return false;
                }

                if($.trim($("#nomor_hp").val()) == ''){
                    document.getElementById("nomor_hp").focus();
                    swal("Mohon Maaf", "Nomor telepon wajib diisi", "warning");                    
                    return false;
                }

                var password = $.trim($("#password").val());
                if(password == ''){
                    document.getElementById("password").focus();
                    swal("Mohon Maaf", "Password wajib diisi", "warning");                    
                    return false;
                } else if(password.length < 3){
                    document.getElementById("password").focus();
                    swal("Mohon Maaf", "Password minimal 3 karakter", "warning");
                    return false;
                }

                var konfirmasi_password = $.trim($("#konfirmasi_password").val());
                if(konfirmasi_password == ''){
                    document.getElementById("konfirmasi_password").focus();
                    swal("Mohon Maaf", "Konfirmasi password wajib diisi", "warning");                    
                    return false;
                } else if(konfirmasi_password.length < 3){
                    document.getElementById("konfirmasi_password").focus();
                    swal("Mohon Maaf", "Konfirmasi password minimal 3 karakter", "warning");
                    return false;
                }

                swal({
                    title: "Konfirmasi Pendaftaran",
                    text: "Apakah anda yakin dengan data yang telah diinput?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary',
                    confirmButtonText: 'Ya',
                    cancelButtonText: "Batal",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $("#form_register").submit();
                    } else {
                        swal("Batal", "Pendaftaran dibatalkan", "error");
                    }
                });
            });
        </script>
    </body>
</html>
